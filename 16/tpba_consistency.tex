Before proceeding, it is important to note that a PTS $\apts=\tuple{\tbaparent, \tbachildren, \forkreln,\joinreln}$ is not itself interpreted
as an automaton. In particular, we do not ever define a language accepted by $\apts$. Indeed,
it is not entirely clear what such a language would be, as we never specify the input to any of the children
in $\atfa$. Rather, the sole intent in specifying such a system $\apts$ is to specify the
\emph{timing behavior} of the overall system, rather than any particular language that would be accepted by it.

\subsubsection{Consistency}
With this said, we note that in Example~\ref{ex:pts}, $\atfa$ is in some sense 
``consistent" with its usage in $\tbaparent$. Specifically, since the maximum duration of any string accepted by 
$\atfa$ is 10, we are guaranteed that any instance of $\atfa$ forked on the 
$\trans{\state[1]}{a}{\state[2]}$ transition will have completed in time for the `join' along the 
$\trans{\state[2]}{c}{\state[1]}$ transition and hence, the timer $\timer{T<50}$ on this transition would be respected in all 
cases. In this sense, all $\pair{\fork{\atfa}}{\join{\atfa}}$ pairs are consistent
with timer $T$. However, such consistency is not always the case. Consider, for instance,
the parallel timing system $\apts[2]$ shown in Figure~\ref{fig:notcons}.
%
\input{tpbaex2}
%
In this case, there are two child processes: $\atfa$ and $\btfa$.
The maximum duration of a timed word accepted by $\atfa$ is 10, and that of 
$\btfa$ is 20. Supposing that an `a' occurs (and $\atfa$ forked) at time 0, it is thus
possible that the $\atfa$ will not complete until time $10-\epsilon_1$, at which time the `b' and fork of 
$\btfa$ can proceed. It is therefore possible that
$\btfa$ will not complete until time $30-\epsilon_1-\epsilon_2$ (for small $\epsilon_1,\epsilon_2$).
This would then violate the $\timer{\atimer<25}$ constraint,
corresponding to a case in which
a child process could take longer to complete than is allowable, given the timing constraints of the
parent process. It is precisely this type of interference
which we must disallow in order for a timing system to be considered consistent
with itself.  

To this end, we propose a method of defining \emph{consistency} within a timing system. 
Informally, we take the approach of 
deriving a new set of conditions from the timing constraints of the child processes,
so that checking \emph{consistency} reduces to the process of verifying that these
conditions respect the timing constraints of the master process.

First, we replace $\atfas, \forkreln,$ and $\joinreln$ from the  parallel timing system 
with a new set of \emph{derived} timers,
one for each $\atfa\in\atfas$, defining the possible ``worst case'' behavior of the child processes. 
Each such timer $\atimer[\atfa]$ is initialized on the transition along which
the corresponding
$\atfa$ is forked, and is used along (constrains) any transitions along which $\atfa$ is joined.
Each such use ensures that the timer is less than $\delayTFA[\atfa]$, representing the fact that
the elapsed time between the forking and joining of a child process is bounded in the worst case
by $\delayTFA[\atfa]$--- the longest possible duration for the child process. 
As an example, ``flattening" the 
timing system $\apts[1]$ of Example~\ref{ex:pts} results in a single new timer $\atimer[\atfa]$,
initialized along the $\trans{\state[1]}{a}{\state[2]}$ transition, and used along the
$\trans{\state[2]}{c}{\state[1]}$ transition with the constraint $\timer{\atimer[\atfa]<10}$.
We then check that none of these new derived timers invalidate the timing constraints of the 
parent process.

\begin{comment}
	The idea is that these derived timers represent the maximum delays potentially imposed on the system
	by each forked child process; we then proceed to reason about how these worst possible delays might 
	be able to break a run which would otherwise satisfy the timing properties of the real-time system. 
	Continuing with the $\apts[1]$ example, consider the timed word 
	\[
		\pair{\infwd{(ac)}}{(0,1,2,3,4,5,...)}
	\]
	There are two things to notice here. First, the timed word is in the language of the parent automaton $\tbaparent$--- 
	the delay between each `a' and the following
	`c' is less than $50$. Secondly, there is no way that the timing properties of the child automaton $\atfa$ 
	can ``break" those of the parent automaton. In other words, temporally speaking, no matter what happens 
	with the child automaton in this example, the parent can always recover to the point where its timing constraints can
	be satisfied. 
	To give an example, the instance of $\atfa$ forked at time $0$ might complete at  
	time $9$. We reflect this worst case scenario by replacing the timestamp of the symbol 
	$\pair{c}{1}$ with this value, and doing so will not invalidate the constraint on $\atimer$, since $9<50$. 
	In the original timed word, however, the next symbols occur at time $2,3,4,...$, so we must offset these
	values accordingly, adding the difference--- in this case, 8---  to each of these subsequent timestamps.
	The result is a timed word  $\pair{\infwd{(ac)}}{(0,1,8,9,10,11,...)}$ which is still accepted by $\tbaparent$.
	We can apply the same argument to any subsequently forked instance of $\atfa$.

	On the other hand, this would not be the case for the system $\apts[2]$ from Figure~\ref{fig:notcons}. In this case,
	the timed word 
	\[
		\pair{\infwd{(abc)}}{(0,5,10,15,20,25,...)}
	\]
	is an example of a word accepted by the parent automaton, and one which can potentially be ``broken" by the child
	automata $\atfa$ and $\btfa$. In this case, $\atfa$ could potentially take 9 (or more) time units,
	and $\btfa$ 19 (or more) to complete, for an elapsed time of 28 time units between an $a$ and the subsequent
	$c$. In the case of
	the instance forked at time 0, this would delay the timestamp of the subsequent `c' from 5 to 28,
	which we reflect by replacing $\pair{c}{5}$  in the timed word with
	$\pair{c}{28}$.  At this point, the $\timer{\atimer<25}$ constraint would already have been violated, something
	that, contrary to the prior example, cannot be fixed by substituting new value for subsequent timestamp values.

	The first
	step in formalizing this concept is to define how a run can potentially be invalidated by a set of
	edge pairs. In each of these pairs,
	the first edge represents a state transition which initializes a timer, and the 
	second represents one which uses the same timer as a constraint check. Such
	a pair takes the form 
	$\epair=\pair{\tuple{\state[i],\state[j],\aalpha[i],\set{\atimer}}
	}{
		\tuple{\state[m],\state[n],\aalpha[m],\uconstraint[\atimer]}
	}$, 
	and we refer to such a pair as an \emph{edge pair}.

	\begin{definition}[Invalidation]
	Let $\atba= \tba{\Aalpha}{\astates}{\state[0]}{\accept}{\aclks}{\tbareln}{\ireln}{\creln}$ be a TBA, and let 
	$\pair{\astring}{\timeseq}$ be a timed word in $\langof{\atba}$.
	Let $\epairs=\set{\epair[1],\epair[2],...}$ be a set of edge pairs, where each edge is of the form
	\[\edgepair
			{\bstate[i]}
			{\bstate[i+1]}
			{\aalpha[i]}
			{\atimer}
			{\bstate[j]}
			{\bstate[j+1]}
			{\aalpha[j]}
			{\uconstraint_j(\atimer)}
		\]
	%%%%%%%%%%%%%%%%%%%5
	%%%%%%%%%%%%%%%%%%%5
	for some fresh timer $\atimer\not\in\aclks$,
	where $\tbareln\ni\tuple{
				{\bstate[i]},
				{\bstate[i+1]},
				{\aalpha[i]}}$,
	and $\tbareln\ni\tuple{ {\bstate[j]},
				{\bstate[j+1]},
				{\aalpha[j]}}$.
	Finally, let 
	\[
	\arun: 
	\pair{\bstate[0]}{\clkint[0]} 
	  \runto{{\timing[0]}}{\aalpha[0]}
	\pair{\bstate[1]}{\clkint[1]} 
	  \runto{{\timing[1]}}{\aalpha[1]}
	\pair{\bstate[2]}{\clkint[2]} 
	  \runto{{\timing[2]}}{\aalpha[2]}
	...
	\]
	be a run of $\pair{\astring}{\timeseq}$ on $\atba$, with  $\clkint[0],\clkint[1],...$ being 
	the timer interpretations over the set of clocks $\aclks$.
	Then $\epairs$ \emph{invalidates} $\arun$
	if 
	\begin{equation}
		\some{
			(\timing[i]', ..., 
			\timing[j]')
		}{
			\all{ (\timing[j+1]',\timing[j+2]',...)}
				\phi_1\land\phi_2\land\phi_3
		}
	\label{eq:invalid}
	\end{equation}
	where 
	\begin{align*}
	\phi_1\equiv&\  \arun\with{\subst{\timing}{\timing'}} \text {generates timer interpretation } \clkints' 
	\text{over the clock set } \aclks \cup \set{\atimer}.  \\
	\phi_2\equiv&\  \all{m\le j}{\ \uconstraint_m(\atimer) \text{ evaluates to true under } \clkints'_{m}}  \\
	\phi_3\equiv&\ {\arun\with{\subst{\timing}{\timing'}} \text{ is not a valid run over } \atba }  
	\end{align*}
	\end{definition}
	%
	We give some examples to help to clarify.
	Consider the infinite timed word  
	\[
		\aword=\pair{a}{0}, \pair{b}{5}, \pair{c}{9}, \pair{a}{10},\pair{b}{11},\pair{c}{12},...
	\]
	or, more formally:
	\[
		\aword=\pair{\infwd{(abc)}}{\timing}, \text{ where } 
		\timing[1..4] = (0,5,9,10) \text{ and } \all{i>4}{\timing[i] = \timing[i-1] + 1}
	\]
	Then, the run $\arun$ of $\aword$ over $\atba_1$ (from Example~\ref{ex:tba1}) is given by:
	\[
	\arun: 
	\pair{\state[1]}{0} 
	  \runto{{0}}{a}
	\pair{\state[2]}{0} 
	  \runto{{5}}{b}
	\pair{\state[2]}{5} 
	  \runto{{9}}{c}
	\pair{\state[1]}{9} 
	  \runto{{10}}{a}
	\pair{\state[2]}{0} 
	  \runto{{11}}{b}
	\pair{\state[2]}{1} 
	  \runto{{12}}{c}
	...
	\]
	Let $\epair[1]$ be the edge pair 
	$\epair[1]=\edgepair 
		{\state[1]}
		{a}
		{\state[2]}
		{\btimer}
		{\state[2]}
		{c}
		{\state[1]}
		{\btimer < 51}$, where $\btimer$ is a fresh timer:
	\begin{figure}[ht]
	\begin{center}
	\begin{pspicture}(5,2)
	\cnodeput[doubleline=true](2,0){pq1}{$\state[1]$}
	\cnodeput(5,0){pq2}{$\state[2]$}
	%\cnodeput(8,2){pq3}{$\state[3]$}

	\ncline{->}{pq1}{pq2}
	\goto[\btimer=0]{\clock{a}{}{\atimer=0}}


	\ncarc[arcangleA=125,arcangleB=125,ncurv=3]{->}{pq2}{pq2}
	\aput{:U}{\clock{b}{}{}}

	%\ncline{->}{pq2}{pq3}
	\ncloop[angleB=180,loopsize=1.8]{->}{pq2}{pq1}
	\dgoto[$\timer{\btimer < 51}$]{\clock{c}{T<50}{}}
	\end{pspicture}
	\end{center}
	\caption{$\atba[1]$ shown with an invalidating edge pair $\epair[1]$ added. $\epair[1]$ adds timer
	$\btimer$.}
	\end{figure}
	%
	\input{lem_invalid}
	Now, let $\epair[2]$ be the edge pair 
	$\epair[2]=\edgepair 
		{\state[1]}
		{a}
		{\state[2]}
		{\btimer}
		{\state[2]}
		{c}
		{\state[1]}
		{\btimer < 20}$, where $\btimer$ is a fresh timer (See Figure~\ref{fig:noninvalid}):
	\begin{figure}[ht]
	\begin{center}
	\begin{pspicture}(5,2)
	\cnodeput[doubleline=true](2,0){pq1}{$\state[1]$}
	\cnodeput(5,0){pq2}{$\state[2]$}
	%\cnodeput(8,2){pq3}{$\state[3]$}

	\ncline{->}{pq1}{pq2}
	\goto[\btimer=0]{\clock{a}{}{\atimer=0}}


	\ncarc[arcangleA=125,arcangleB=125,ncurv=3]{->}{pq2}{pq2}
	\aput{:U}{\clock{b}{}{}}

	%\ncline{->}{pq2}{pq3}
	\ncloop[angleB=180,loopsize=1.8]{->}{pq2}{pq1}
	\dgoto[$\timer{\btimer < 20}$]{\clock{c}{T<50}{}}
	\end{pspicture}
	\end{center}
	\caption{$\atba[1]$ shown with a noninvalidating edge pair $\epair[2]$ added. $\epair[2]$ adds timer $\btimer$.}
	\label{fig:noninvalid}
	\end{figure}
	\input{lem_valid}
	%
	From a practical perspective, Lemma~\ref{lem:invalid} models a system which spawns
	a child process $\atfa$ on the $\state[1]\to\state[2]$ transition. Furthermore, 
	$\atfa$ could potentially take 51 time units to complete (i.e., $\delayTFA[\atfa]=51$),
	and $\atfa$ must finish executing before the $\state[2]\to\state[1]$ transition 
	can be taken.
	The implication is that if $\atfa$ indeed takes the full 51 time units to complete,
	then the timing constraints of the parent process (i.e., the $\atimer<50$ constraint)
	will fail, rendering the real-time specifications of the parent process invalid.

	On the other hand, Lemma~\ref{lem:valid} models a system which instead spawns
	a child process $\btfa$ along the same transition, but this time, $\btfa$ requires
	at most 10 time units to complete ($\delayTFA[\btfa]=10$). The implication in this case
	is that even in the worst case, $\btfa$ will complete in time for the $\state[2]\to\state[1]$
	transition, so the timing constraints of the parent process will always
	hold subject to the timing behavior of all child processes.

	In game-theoretic terms, one could view \emph{invalidation} as a game in which Player 1 is 
	the edge pair $\epair$, and Player 2 is the run $\arun$ of $\pair{\astring}{\timing}$ over $\atba$. In invalidation,
	Player 1's goal is to select a set of timestamps $\timing[i]',...,\timing[j]'$ to ``break''
	$\arun$ severely enough to render it ``unfixable''---
	i.e., such that there are no timestamps $\timing[j+1]', \timing[j+2]',...$ that Player 2 can choose which 
	would turn the broken run back into a valid one when all timestamps $\timing$ are substituted with $\timing'$.
	Pragmatically, Player 1 can select from all possible timing behaviors of a child process;
	if it is possible for the timing of a child process to interfere with that of a parent process to the point
	where the parent cannot recover its timing properties, then the child process has effectively \emph{invalidated} the parent process.  
	When viewed in this manner, we can say that $\epair$ invalidates $\arun$ if Player 1 has a winning 
	strategy for this game.

	\emph{Invalidation} gives a localized example of how a child process can potentially
	interfere with the the timing properties of its parent. Specifically, it is defined
	in terms of a specific timed string, a run of that string on the parent, and a somewhat 
	arbitrary edge pair. Our ultimate goal is to guarantee timing properties over a parallel timing
	system, and to do this, we need to generalize the concept of invalidation to a higher level.
	%
	The first step in doing so is to take a parallel timing system and derive from it
	the set of edge pairs which will be used to test for potential invalidation against the 
	parent TBA.
	To do this,
\end{comment}

	Formally, we define two relations. The first of these is  \emph{flattening}, which takes 
	a parallel timing system $\pts{\tbaparent}{\tbachildren}{\forkreln}{\joinreln}$ 
	and yields a new pair of relations $\pair{\ireln}{\creln}$.
	Intuitively, $\ireln$ defines the edges along which each of the derived timers
	are initialized, and $\creln$ defines the edges along which each of the derived timers
	are used: 
\begin{definition}
Let  $\apts=\pts{\tbaparent}{\tbachildren}{\forkreln}{\joinreln}$ be a parallel timing system.
Then $\flatten{\apts} = \pair{\ireln}{\creln}$, where
\begin{align*}
\ireln&=  \setst{
		\tuple{\state[i],\state[j],\aalpha,\set{\atimer[\atfa]}}
	}{
		\tuple{\state[i],\state[j],\aalpha,\atfa} \in\forkreln
	} \\
\creln&= \setst{
		\tuple{\state[i],\state[j],\aalpha,\Constraint}
	}{
		\tuple{\state[i],\state[j],\aalpha,\atfa} \in\joinreln
	} 
\end{align*}
and
\begin{align*}
\Constraint &= 
	\bigwedge_{
					\tuple{\state[i],\state[j],\aalpha,\atfa} \in \joinreln
				}{
					(\atimer[\atfa] < \delayTFA[\atfa])
				}
\end{align*}
\end{definition}
The second relation takes $\forkreln$ and $\joinreln$ as inputs and extracts a set of edge pairs, defined
such that each such pair
$\pair{\aedge[1]}{\aedge[2]}$ specifies when a derived timer is initialized $(\aedge[1])$
and used $(\aedge[2])$. 
\begin{definition}
Let  $\apts=\tuple{\tbaparent,\tbachildren,\forkreln,\joinreln}$ be a parallel timing system,
with $\atfa\in\tbachildren$.
Then the set of all use pairs of $\atfa$ in $\apts$ is defined as $\uses[\atfa]{\apts} = \setst{
	\pair{
		\pair{\state[x]}{\state[y]}
	}{
		\pair{\state[m]}{\state[n]}
	}
}{
	(\tuple{\state[x],\state[y],\aalpha[1],\atfa} \in \forkreln) \land
	(\tuple{\state[m],\state[n],\aalpha[2],\atfa} \in \joinreln) 
}$ for some $\aalpha[1],\aalpha[2]$. Furthermore,
\[
	\uses{\apts} = \bigcup_{
		\atfa\in\tbachildren
	}{
		\uses[\atfa]{\apts}
	}
\]
\end{definition}

\begin{figure*}[tlh]
\begin{center}
\begin{pspicture}(10.5,7)
\rput(0,4){\rnode{P}{\tbaparent:}}

%%%%%%%%%% P:
\cnodeput[linestyle=dashed](1,4){pq1}{$\state[1]$}
\cnodeput[doubleline=true](5,4){pq2}{$\state[2]$}
\cnodeput(5,7){pq3}{$\state[3]$}
\ncline{->}{pq1}{pq2}
\goto[$\fork{\atfa}$]{\clock{a}{}{\atimer=0}}
\ncline{->}{pq2}{pq3}
\bgoto[$\fork{\btfa}$]{$b$}
\ncline{->}{pq3}{pq1}
\dgoto[$\join{\atfa,\btfa}$]{\clock{c}{\atimer<24}{}}

%%%%%%%%%% A:
\rput(0,0.5){\rnode{A}{$\atfa$:}}
\cnodeput[linestyle=dashed](1,1){aq1}{$\bstate[1]$}
\cnodeput(3,1){aq2}{$\bstate[2]$}
\cnodeput(4,0){aq3}{$\bstate[3]$}
\cnodeput(6,0){aq4}{$\bstate[4]$}
\cnodeput(4,2){aq5}{$\bstate[5]$}
\cnodeput(8,2){aq6}{$\bstate[6]$}
\cnodeput[doubleline=true](10.5,1){aq7}{$\bstate[7]$}

\ncline{->}{aq1}{aq2}
\goto{\clock{0}{}{U=0}}
\ncline{->}{aq2}{aq3}
\goto[0]{}
\ncline{->}{aq3}{aq4}
\goto[0]{}
\ncline{->}{aq4}{aq7}
\goto[\clock{0}{\btimer<10}{}]{}
\ncline{->}{aq2}{aq5}
\goto{1}
\ncline{->}{aq5}{aq6}
\goto{\clock{1}{\btimer<20}{\ctimer=0}}
\ncline{->}{aq6}{aq7}
\goto{\clock{1}{\ctimer<5}{}}
\ncline{->}{aq5}{aq4}
\goto{\clock{0}{}{}}

%%%%%%%% B:
%\rput(0,0.5){\rnode{B}{$\btfa$:}}
\rput(6,4){\rnode{B}{$\btfa$:}}
\cnodeput[linestyle=dashed](7,5){bq1}{$\cstate[1]$}
\cnodeput(9,5){bq2}{$\cstate[2]$}
\cnodeput[doubleline=true](11,5){bq3}{$\cstate[3]$}

\ncline{->}{bq1}{bq2}
\goto{\clock{0}{}{\ctimer=0}}
\ncline{->}{bq2}{bq3}
\goto{\clock{0}{\ctimer<11}{}}
\end{pspicture}
\end{center}
\caption{Parallel timing system $\apts[3]$. $\delayTFA[\atfa]=25, \delayTFA[\btfa]=11$.}
\label{fig:s3}
\end{figure*}
\begin{example}
Consider parallel timing system $\apts[3]$ shown in Figure~\ref{fig:s3}. 
Observe that $\delayTFA[\atfa]=25$ and $\delayTFA[\btfa]=11$. Then:
$\flatten{\apts[3]}=\pair{\ireln}{\creln}$, where
\begin{align*}
\ireln&=\set{\tuple{\state[1],\state[2],a,\set{{\atimer[\atfa]}}},
	\tuple{\state[2],\state[3],b,\set{{\atimer[\btfa]}}}}\\
\creln&=\set{
	\tuple{
		\state[3],\state[1],c,\Constraint}
}\text{ , where } \Constraint=(\atimer[\atfa] < 25)\land(\atimer[\btfa] < 11) 
\end{align*}
shown graphically in Figure~\ref{fig:s3-flat}, and 
\begin{align*}
\uses{\apts} &= \uses[\atfa]{\apts} \cup \uses[\btfa]{\apts} \\
&= \set{
	\pair{
		\pair{\state[1]}{\state[2]}
	}{
		\pair{\state[3]}{\state[1]}
	}
} \cup \set{
	\pair{
			\pair{\state[2]}{\state[3]}
		}{
			\pair{\state[3]}{\state[1]}
		}
}\\
&= \set{ \pair{
		\pair{\state[1]}{\state[2]}
	}{
		\pair{\state[3]}{\state[1]}
	}, 
	\pair{
			\pair{\state[2]}{\state[3]}
		}{
			\pair{\state[3]}{\state[1]}
		}
}
\end{align*}

\end{example}

\begin{figure}[tlh]
\begin{center}
\begin{pspicture}(0,0)(5,3.5)
\cnodeput[linestyle=dashed](0,0.5){pq1}{$\state[1]$}
\cnodeput[doubleline=true](5,0.5){pq2}{$\state[2]$}
\cnodeput(5,3.5){pq3}{$\state[3]$}
\ncline{->}{pq1}{pq2}
\goto[{\atimer=0\timersep\atimer[\atfa]=0}]{\clock{a}{}{}}
\ncline{->}{pq2}{pq3}
\bgoto[{\atimer[\btfa]=0}]{b}
\ncline{->}{pq3}{pq1}
\dgoto[${\timer{(\atimer[\atfa]<25) \land (\atimer[\btfa]<11)}}$]{\clock{c}{\atimer<24}{}}
\end{pspicture}
\end{center}
\caption{The result of flattening $\apts[3]$: forks and joins of $\atfa$ and $\btfa$ are shown along with
derived timers $\atimer[\atfa]$ and $\atimer[\btfa]$. Compare with Figure~\ref{fig:s3}.}
\label{fig:s3-flat}
\end{figure}

We can now proceed with a formal definition of consistency for a parallel timing system. Recall that
intuitively, such a system is consistent if the worst case timing scenarios over all child processes
will not invalidate the timing constraints of the parent process--- in other words, if the maximum delay 
between two states allowed
by the child processes never exceeds the corresponding maximum delay allowed by the timers in the 
parent process.

\begin{definition}[Consistency]
Let $\apts=\tuple{\atba,\forkreln,\joinreln,\tbachildren}$ be a PTS, where
\begin{itemize}
\item $\atba= \tba{\Aalpha}{\astates}{\state[0]}{\accepts}{\aclks}{\tbareln}{\ireln}{\creln} $ is a TBA
\item $\flatten{\apts} = \pair{\ireln'}{\creln'}$
\item $\atba'= \tba{\Aalpha}{\astates}{\state[0]}{\accepts}{\aclks}{\tbareln}{\ireln'}{\creln'}$
%\item $\epairs=\setst{
%	\pair{\aedge[1] }
%	{\aedge[2] }
%}{
%	(\aedge[1]\in\ireln')\land
%	(\atimer\in\timerof{\aedge[1]})\land
%	(\aedge[2]=\tuple{\state[i],\state[j],\aalpha,
%	\bigwedge_{\uconstraint\in\tlookup{\constraints}{\atimer}}\uconstraint})\land
%	(\tuple{\state[i],\state[j],\aalpha,\constraints}\in\creln')
%}$
\end{itemize}
%
%Then $\apts$ is \emph{inconsistent} if there exists a timed string $\pair{\astring}{\timing}$ and an edge pair
%$\epair\in\epairs$ such that $\epair$ invalidates the run of $\pair{\astring}{\timing}$ over $\atba$.
%
%Furthermore, we say that $\apts$  is \emph{consistent} if it is not \emph{inconsistent}--- that is, if no such timed string
%and edge pair exists.
Then $\apts$ is \emph{consistent} if 
for all edge pairs $\pair{
	\pair{\state[x]}{\state[y]}
}{
	\pair{\state[m]}{\state[n]}
}\in\uses{\apts}$ and all paths $\apath=\state[x]\state[y]...\state[m]\state[n]$ through $\atba$,
\begin{equation}
	\delayTBA[\atba']{\apath} \le \delayTBA[\atba]{\apath}
\label{eq:cons}	
\end{equation}
\label{def:cons}
\end{definition}

We conclude this section with a few simple examples, which should help to clarify 
Definition~\ref{def:cons}; the following section gives a more realistic example.
%\input{thm_consistency}
\input{thm_cons}
\input{thm_incons}

\endinput
