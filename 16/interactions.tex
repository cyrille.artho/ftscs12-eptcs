This problem can thus be formulated
in the following manner: given a timed finite automaton $\atfa$ and an 
integer $n$, is there a timed word of duration $d\ge n$ that is accepted by $\atfa$?
While simple cases, such as those presented in this paper, can be computed by observation and enumeration,
the complexity of the general problem
remains an open question, although we highly suspect it to be intractable--- Courcoubetis and Yannakakis give 
exponential-time algorithms for this and 
related problems, and have shown a strictly more difficult variant of the problem to be \PSPACE-complete 
\cite{minmax}. 
%(c.f. the simpler, single-path variant of the problem stated previously).
Furthermore, expanding the timer constraint syntax to a more 
expressive variant (c.f. \cite{tba}) can only complicate matters in terms of complexity.
We must be cautious, then, to ensure that we do not impose an inordinately large number of timers on a child process.
%We conjecture that this will not be an unreasonable restriction, as in most practical cases, we expect a rather small number 
%of timers--- an example of a \emph{fixed parameter tractable} problem in parameterized terms 
%(\cite{fpt, fpt2}).
%However, as this is not the main focus or objective of the current paper, we move on to the topic of 
%model checking real-time  constraints in the larger system. 


\begin{comment}
		However, we give a dynamic programming algorithm in the next section which
		gives a reasonable heuristic.

		\subsubsection{Heuristic Approximation}
		The idea behind our dynamic programming heuristic is that 
		the delay along a path between two states $\state[x]$ and $\state[y]$ can be
		upper bounded, albeit not necessarily tightly,
		by first picking an intermediate state $\state[z]$ along the path, 
		and taking the sum of the worst case 
		delay between $\state[x]$ and $\state[z]$, and that between $\state[z]$ and $\state[y]$.

		Before we formally give our dynamic programming algorithm, we demonstrate using a simple
		example:
		\begin{center}
		\begin{pspicture}(7.5,2)
		\cnodeput(0,1){n1}{$\state[1]$}
		\cnodeput(1.5,1){n2}{$\state[2]$}
		\cnodeput(3,1){n3}{$\state[3]$}
		\cnodeput(4.5,1){n4}{$\state[4]$}
		\cnodeput(6,1){n5}{$\state[5]$}
		\cnodeput(7.5,1){n6}{$\state[6]$}

		\ncline{->}{n1}{n2}
		\bput{:U}{$\atimer=0$}
		\ncline{->}{n2}{n3}
		\bput{:U}{$\ctimer=0$}
		\ncline{->}{n3}{n4}
		\aput{:U}{$\timer{\atimer<20}$}
		\bput{:U}{ \begin{tabular}{l}
				$\btimer=0$\\
				$\dtimer=0$
			   \end{tabular} }
		\ncline{->}{n3}{n4}
		\ncline{->}{n4}{n5}
		\aput{:U}{$\timer{\ctimer<10}$}
		\ncline{->}{n5}{n6}
		\aput{:U}{
			\begin{tabular}{l}
			$\timer{\btimer<30}$\\
			$\timer{\dtimer<2}$\\
			\end{tabular} }
		\end{pspicture}
		\end{center}

		In this case, we are interested in computing an upper bound on the delay
		between states $\state[1]$ and $\state[6]$. As is the case with most dynamic programming approaches, 
		we begin by giving bottom-up solutions to 
		the base cases, which in this case are individual transitions along the path.
		For instance, we can assign an initial bound on the delay of 20 from $\state[2]$ to $\state[3]$.
		To see why, in this case, $\atimer$ is the only timer spanning this transition, 
		and the delay along this transition can approach $20-\epsilon$ units while
		still
		respecting the $\atimer<20$ bound specified along the $\state[3]\to\state[4]$
		transition.  This scenario is illustrated by the timed substring 
		\[
			(\aalpha[1], \timing[1]), (\aalpha[2], \timing[1]+20-\epsilon_1-\epsilon_2), 
			(\aalpha[3], \timing[1]+20-\epsilon_1)
		\]
		Any longer delay along the $\state[2]\to\state[3]$ transition--- i.e., a delay of 20 or more, corresponding to $\aalpha[2]$ occurring at time $\timing[1]+20$ or later--- 
		would necessarily invalidate the $\timer{\atimer<20}$ bound.

		On the other hand, the $\state[3]\to\state[4]$ transition is spanned by two timers:
		$\atimer$ and $\ctimer$. As before, $\atimer$ restricts the delay to a maximum of 
		20 time units, but $\ctimer$ is even more restrictive, giving a tighter upper bound of 10. 
		Taking the minimum of these bounds gives us an upper bound of 10 along the $\state[3]\to\state[4]$ transition.

		Continuing in this fashion, we are able to derive a set of values corresponding to our
		base cases as follows:
		\begin{center}
		\begin{tabular}{c|c|l}
		Transition $t$&  $\pathdelay{\apath}{t}{}$& Comment\\
		\hline
		$\state[1]\to\state[2]$&$0$&No spanning timers \\
		$\state[2]\to\state[3]$& $min(20)=20$& Spanned by $\atimer$\\
		$\state[3]\to\state[4]$& $min(10,20)=10$& Spanned by $\atimer,\ctimer$ \\
		$\state[4]\to\state[5]$&$min(2,10,30)=2$ &Spanned by $\btimer,\ctimer,\dtimer$\\
		$\state[5]\to\state[6]$&$min(2,10,30)=2$ &Spanned by $\btimer,\ctimer,\dtimer$\\
		\end{tabular}
		\end{center}
		From these upper bounds on single transitions, we can construct upper bounds 
		on pairs of transitions. Namely, we can obtain an upper delay bound 
		on $\state[2]\goesto\state[4]$ as the sum of the upper delay bound from 
		$\state[2]\to\state[3]$, and that from $\state[3]\to\state[4]$, or 
		$20+10=30$. \emph{TODO: Should probably stress again that this bound is not tight.}
		\begin{center}
		\begin{tabular}{c|c|l}
		Transition $t$&  $\pathdelay{\apath}{t}{}$& Comment\\
		\hline
		$\state[1]\to\state[3]$&$0+20$&Spanned by $\atimer$\\
		$\state[2]\to\state[4]$& $20+10=30$& Spanned by $\atimer,\ctimer$\\
		$\state[3]\to\state[5]$& $10+2=12$& Spanned by $\ctimer,\dtimer$ \\
		$\state[4]\to\state[6]$&$2$ &Spanned by $\dtimer$\\
		\end{tabular}
		\end{center}

		Next, we can similarly use these computed results to obtain upper bounds
		on three-step transitions. This time, however, we combine intermediate results:
		\begin{center}
		\begin{tabular}{c|c|l}
		Transition $t$&  $\pathdelay{\apath}{t}{}$& Comment\\
		\hline
		$\state[1]\to\state[4]$&
		$
		\begin{array}[t]{r@{}l}
			min(&
			\pathdelay{\apath}{\state[1]}{\state[2]}+
			\pathdelay{\apath}{\state[2]}{\state[4]},\\
			&\pathdelay{\apath}{\state[1]}{\state[3]}+
			\pathdelay{\apath}{\state[3]}{\state[4]})= \\
			 min(&0+30,20+10)=30
		\end{array}
		$
		&Spanned by $\atimer,\ctimer$\\
		%
		$\state[2]\to\state[5]$&
		$
		\begin{array}[t]{r@{}l}
			min(&
			\pathdelay{\apath}{\state[2]}{\state[3]}+
			\pathdelay{\apath}{\state[3]}{\state[5]},\\
			&\pathdelay{\apath}{\state[2]}{\state[4]}+
			\pathdelay{\apath}{\state[4]}{\state[5]})= \\
			 min(&20+12, 30+2)=32
		\end{array}
		$
		&Spanned by $\atimer,\ctimer,\dtimer$\\
		%
		$\state[3]\to\state[6]$&
		$
		\begin{array}[t]{r@{}l}
			min(&
			\pathdelay{\apath}{\state[3]}{\state[4]}+
			\pathdelay{\apath}{\state[4]}{\state[6]},\\
			&\pathdelay{\apath}{\state[3]}{\state[5]}+
			\pathdelay{\apath}{\state[5]}{\state[6]})= \\
			 min(&10+2, 12+2)=12
		\end{array}
		$&Spanned by $\ctimer,\dtimer$\\
		\end{tabular}
		\end{center}

		Four step transitions: 

		\begin{center}
		\begin{tabular}{c|c|l}
		Transition $t$&  $\pathdelay{\apath}{t}{}$& Comment\\
		\hline
		$\state[1]\to\state[5]$&
		$
		\begin{array}[t]{r@{}l}
			min(&
			\pathdelay{\apath}{\state[1]}{\state[2]}+
			\pathdelay{\apath}{\state[2]}{\state[5]},\\
			&\pathdelay{\apath}{\state[1]}{\state[3]}+
			\pathdelay{\apath}{\state[3]}{\state[5]}), \\
			&\pathdelay{\apath}{\state[1]}{\state[4]}+
			\pathdelay{\apath}{\state[4]}{\state[5]})= \\
			 min(&0+32,20+12, 30+2)=32
		\end{array}
		$
		&Spanned by $\atimer,\ctimer,\dtimer$\\
		%
		$\state[2]\to\state[6]$&
		$
		\begin{array}[t]{r@{}l}
			min(&
			\pathdelay{\apath}{\state[2]}{\state[3]}+
			\pathdelay{\apath}{\state[3]}{\state[6]},\\
			&\pathdelay{\apath}{\state[2]}{\state[4]}+
			\pathdelay{\apath}{\state[4]}{\state[6]}), \\
			&\pathdelay{\apath}{\state[2]}{\state[5]}+
			\pathdelay{\apath}{\state[5]}{\state[6]})= \\
			 min(&20+12,30+2,32+2)=32
		\end{array}
		$
		&Spanned by $\atimer,\ctimer,\dtimer$\\
		\end{tabular}
		\end{center}

		Lastly,
		\begin{center}
		\begin{tabular}{c|c|l}
		Transition $t$&  $\pathdelay{\apath}{t}{}$& Comment\\
		\hline
		$\state[1]\to\state[6]$&
		$
		\begin{array}[t]{r@{}l}
			min(&
			\pathdelay{\apath}{\state[1]}{\state[2]}+
			\pathdelay{\apath}{\state[2]}{\state[6]},\\
			&\pathdelay{\apath}{\state[1]}{\state[3]}+
			\pathdelay{\apath}{\state[3]}{\state[6]}), \\
			&\pathdelay{\apath}{\state[1]}{\state[3]}+
			\pathdelay{\apath}{\state[3]}{\state[5]}), \\
			&\pathdelay{\apath}{\state[1]}{\state[4]}+
			\pathdelay{\apath}{\state[4]}{\state[6]})= \\
			 min(&0+32,20+12, 30+2, 22)=32
		\end{array}
		$
		&Spanned by $\atimer,\ctimer,\dtimer$\\
		\end{tabular}
		\end{center}

		..., giving rise to two matrices, one of which records the values of 
		$\pathdelay{\apath}{\state[x]}{\state[y]}$, and the other of which
		records the set of timers spanning the interval $\state[x]\to\state[y]$.

		For a given path $\apath=\path{\state[1],\state[2],...,\state[n]}$ in a timed automaton $\atfa$, we 
		define $\pathdelay{\apath}{\state[x]}{\state[y]}$, 
		our upper delay bound between $\state[x]$ and $\state[y]$, as follows:

		\[
		\pathdelay{\apath}{\state[x]}{\state[y]} =
		\begin{cases}
			\min_{\atimer\in\timerspan{\state[x]}{\state[y]}{\aalpha}}\timerbound{\atimer}{\apath} &
			\text{ if } y=x+1\\
			\min_{x<i<y}
			\sum_{
				\atimer\in
					\timerspan{\apath}{\state[x]}{\state[i]}\bigcup
					\timerspan{\apath}{\state[i]}{\state[y]}\bigcup
			}{
			}
		(\pathdelay{\apath}{\state[x]}{\state[i]} + \pathdelay{\apath}{\state[i]}{\state[y]}) 
				&
				\text{ if } y\neq x+1 
		\end{cases}
		\]

		\begin{example}
		\end{example}
\end{comment}

\endinput 




%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{comment}
	%% 8-28-2012: [PH]: Commented out for space
	To begin, consider an arbitrary path between two states $\state[x]$ and $\state[y]$.
	We observe that, in certain cases, it is possible to obtain an upper bound on the delay 
	between $\state[x]$ and $\state[y]$. For example, in the case where a timer $\atimer$
	is initialized on the first transition  along the path, and constrains the last transition
	along the path  as in Figure~\ref{fig:simpledelay}a,
	we know that $\aconst$ is an upper bound on the delay between $\state[x]$ and 
	$\state[y]$. However, in the case where $\atimer$ is not initialized
	until some intermediate transition, we cannot infer such an upper bound in the absence
	of any other timers (Figure~\ref{fig:simpledelay}b).
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\begin{figure}[hlt]
	\begin{center}
	\begin{pspicture}(6.5,2.5)
	%%%%%%%%%
	\rput(0,2){(a)}
	\cnodeput(1,2){n1}{$\state[x]$}
	\cnodeput(3,2){n2}{}
	\ncline{->}{n1}{n2}
	\goto[$\atimer=0$]{$\aalpha_x$}
	%\bput{:U}{$\atimer=0$}

	\cnodeput(4,2){n3}{}
	\ncline[linestyle=dotted]{->}{n2}{n3}
	%\bput{:U}{label}

	\cnodeput(6,2){n4}{$\state[y]$}
	\ncline{->}{n3}{n4}
	\goto[$\timer{\atimer<\aconst}$]{$\aalpha_y$}

	%%%%%%%%%%%55
	\rput(0,0.5){(b)}
	\cnodeput(1,0.5){m1}{$\state[x]$}
	\cnodeput(2.5,0.5){m10}{$\state[z]$}
	\cnodeput(4,0.5){m2}{}
	\ncline{->}{m10}{m2}
	\goto[$\atimer=0$]{$\aalpha_x$}
	\ncline[linestyle=dotted]{->}{m1}{m10}
	%\bput{:U}{$\atimer=0$}

	\cnodeput(5,0.5){m3}{}
	\ncline[linestyle=dotted]{->}{m2}{m3}
	%\bput{:U}{label}

	\cnodeput(6.5,0.5){m4}{$\state[y]$}
	\ncline{->}{m3}{m4}
	\goto[$\timer{\atimer<\aconst}$]{$\aalpha_y$}
	\end{pspicture}
	\caption{(a) Case in which timer $\atimer$ is initialized along first transition, and used
	along the last transition. Maximum delay between $\state[x]$ and $\state[y]$ is $\aconst$, corresponding
	to the case where $\aalpha[x]$ occurs at some time $\timing[x]$, and $\aalpha[y]$
	at time $\timing[x] + \aconst - \epsilon$ for some small $\epsilon$.
	(b) Case where timer is initialized at some state $\state[z]$ after first transition. In the absence of
	any other timers, no maximum delay from $\state[x]$ to $\state[y]$ can be inferred,
	since the elapsed time between $\state[x]$ and $\state[z]$ is potentially unbounded.
	}
	\label{fig:simpledelay}
	\end{center}
	\end{figure}
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	Similarly, in certain cases with two timers, we can 
	derive an upper delay bound--- Figure~\ref{fig:twotimers} illustrates the possible
	ways in which two timers $\atimer$ and $\btimer$ can interact,  
	in the absence of any other timers.

	In the first possibility, shown in 
	Figure~\ref{fig:twotimers}a, $\atimer$ is both initialized and used
	prior to $\btimer$'s initialization and use. Specifically, there is at least
	one transition between the use of $\atimer$ and the initialization of $\btimer$ 
	(between states $\state[m]$ and $\state[n]$ in  Figure~\ref{fig:twotimers}a), across which
	there are no timer constraints, and hence a potentially unbounded delay. Hence in this case,
	no guaranteed upper delay bound can be established.

	In the second case, shown in Figure~\ref{fig:twotimers}b, $\btimer$ is initialized
	along the same transition that $\atimer$ is used. In this case, we are able to derive
	an upper delay bound between $\state[x]$ and $\state[y]$, namely
	$\aconst[1]+\aconst[2]$,
	corresponding to the case in which $\aalpha[x]$ occurs at some time $\timing[x]$,
	$\aalpha[m]$ at time $\timing[x]+\aconst[1]-\epsilon_1$, and 
	$\aalpha[y]$ at time 
	\begin{align*}
	\timing[y]=&(\timing[x]+\aconst[1]-\epsilon_1) + \aconst[2]-\epsilon_2  \\
		  =&\timing[x]+\under{\pathdelay{\apath}{}{}}{\aconst[1] + \aconst[2]-\epsilon}
	\end{align*}
	for some small $\epsilon$.

	In the third possibility, shown in Figure~\ref{fig:twotimers}c, $\btimer$ is both
	initialized and used between $\atimer$'s initialization and use.
	In this case, 
	the delay $d$ between $\state[x]$ and $\state[y]$ must be less than $\aconst[1]$.
	%the maximum delay between $\state[x]$ and $\state[y]$ in this case is $\aconst[1]$.

	In the fourth possibility, shown in Figure~\ref{fig:twotimers}d, $\atimer$ is initialized along
	the first transition of the path, and $\btimer$ along a later transition. $\atimer$ and
	$\btimer$ are then used along the last transition of the path. The maximum delay in this case
	is also $\aconst[1]$, by the same reasoning as the previous case.

	In the last remaining possibility, shown in Figure~\ref{fig:twotimers}e,
	$\atimer$ is first initialized, followed by $\btimer$. $\atimer$ is then used,
	followed by a use of $\btimer$. In this case, 
	the maximum delay between $\state[x]$
	and $\state[y]$ is given by $\aconst_1+\aconst_2$. To see why, consider a run of 
	the string 
	\[
		\pair{\aalpha[x]}{\timing[x]},
		...
		\pair{\aalpha[m]}{\timing[m]},
		...
		\pair{\aalpha[n]}{\timing[n]},
		...
		\pair{\aalpha[y]}{\timing[y]}
	\]
	which starts at state $\state[x]$ and ends at state $\state[y]$. The worst case
	scenario--- that which gives the  
	longest delay between $\state[x]$ and $\state[y]$---
	corresponds to the case where 
	\begin{itemize}
	\item $\timing[n]=\timing[x]+\aconst_1-\epsilon_1$ (the longest delay accepted by $\atimer$), 
	\item $\timing[m]=\timing[n]-\epsilon_2=\timing[x]+\aconst_1-\epsilon_1-\epsilon_2$
	for some small values $\epsilon_1,\epsilon_2$, giving a worst case value of $\timing[y]$ of:
	\item $\timing[y]=\timing[m]+\aconst_2-\epsilon_3=\timing[x]+\under{\pathdelay{\apath}{}{}}{\aconst_1+\aconst_2-\epsilon_1-\epsilon_2-\epsilon_3}$
	\end{itemize}
	giving an upper delay bound of $\aconst_1+\aconst_2-\epsilon$ for some small $\epsilon$.

	\begin{figure*}[hlt]
	\begin{center}
	\begin{pspicture}(10,6.5)

	\rput(0,6){(a)}
	\cnodeput(1,6){n1}{$\state[x]$}
	\cnodeput(3,6){n2}{}
	\ncline{->}{n1}{n2}
	\bput{:U}{$\atimer=0$}

	\cnodeput(4,6){n3}{}
	\ncline[linestyle=dotted]{->}{n2}{n3}
	%\bput{:U}{label}

	\cnodeput(5.9,6){n4}{$\state[m]$}
	\ncline{->}{n3}{n4}
	\bput{:U}{  $\timer{\atimer<\aconst_1}$} 

	\cnodeput(7.1,6){n5}{$\state[n]$}
	\ncline[linestyle=dotted]{->}{n4}{n5}
	%\bput{:U}{label}

	\cnodeput(9,6){n6}{}
	\ncline{->}{n5}{n6}
	\bput{:U}{ $\btimer=0$}

	\cnodeput(10,6){n7}{}
	\ncline[linestyle=dotted]{->}{n6}{n7}

	\cnodeput(12,6){n8}{$\state[y]$}
	\ncline{->}{n7}{n8}
	\bput{:U}{$\timer{\btimer<\aconst_2}$}

	%%%%%%%%%%%%%%%%%%5

	\rput(0,4.5){(b)}
	\cnodeput(1,4.5){n1}{$\state[x]$}
	\cnodeput(3,4.5){n2}{}
	\ncline{->}{n1}{n2}
	\aput{:U}{$\aalpha[x]$}
	\bput{:U}{$\atimer=0$}

	\cnodeput(4.5,4.5){n5}{$\state[m]$}
	\ncline[linestyle=dotted]{->}{n2}{n5}
	%\bput{:U}{label}

	\cnodeput(8,4.5){n6}{$\state[n]$}
	\ncline{->}{n5}{n6}
	\aput{:U}{$\aalpha[m]$}
	\bput{:U}{ $\timer{\atimer<\aconst_1},\btimer=0$} 

	\cnodeput(10,4.5){n7}{}
	\ncline[linestyle=dotted]{->}{n6}{n7}

	\cnodeput(12,4.5){n8}{$\state[y]$}
	\ncline{->}{n7}{n8}
	\aput{:U}{$\aalpha[y]$}
	\bput{:U}{$\timer{\btimer<\aconst_2}$}

	%%%%%%%%%%%%%%%%%%5

	\rput(0,3){(c)}
	\cnodeput(1,3){n1}{$\state[x]$}
	\cnodeput(3,3){n2}{}
	\ncline{->}{n1}{n2}
	\bput{:U}{$\atimer=0$}

	\cnodeput(4,3){n3}{$\state[m]$}
	\ncline[linestyle=dotted]{->}{n2}{n3}
	%\bput{:U}{label}

	\cnodeput(6,3){n4}{}
	\ncline{->}{n3}{n4}
	\bput{:U}{$\btimer=0$}

	\cnodeput(7,3){n5}{}
	\ncline[linestyle=dotted]{->}{n4}{n5}
	%\bput{:U}{label}

	\cnodeput(9,3){n6}{$\state[n]$}
	\ncline{->}{n5}{n6}
	\bput{:U}{$\timer{\btimer<\aconst_2}$}

	\cnodeput(10,3){n7}{}
	\ncline[linestyle=dotted]{->}{n6}{n7}

	\cnodeput(12,3){n8}{$\state[y]$}
	\ncline{->}{n7}{n8}
	\bput{:U}{$\timer{\atimer<\aconst_1}$} 


	%%%%%%%%%%%%%%%%%%%%%%5


	\rput(0,0){(e)}
	\cnodeput(1,0){n1}{$\state[x]$}
	\cnodeput(3,0){n2}{}
	\ncline{->}{n1}{n2}
	\aput{:U}{$\aalpha_x$}
	\bput{:U}{$\atimer=0$}

	\cnodeput(4,0){n3}{}
	\ncline[linestyle=dotted]{->}{n2}{n3}
	%\bput{:U}{label}

	\cnodeput(6,0){n4}{}
	\ncline{->}{n3}{n4}
	\aput{:U}{$\aalpha_m$}
	\bput{:U}{$\btimer=0$}

	\cnodeput(7,0){n5}{}
	\ncline[linestyle=dotted]{->}{n4}{n5}
	%\bput{:U}{label}

	\cnodeput(9,0){n6}{}
	\ncline{->}{n5}{n6}
	\aput{:U}{$\aalpha_n$}
	\bput{:U}{$\timer{\atimer<\aconst_1}$}

	\cnodeput(10,0){n7}{}
	\ncline[linestyle=dotted]{->}{n6}{n7}

	\cnodeput(12,0){n8}{$\state[y]$}
	\ncline{->}{n7}{n8}
	\aput{:U}{$\aalpha_y$}
	\bput{:U}{$\timer{\btimer<\aconst_2}$}

	%%%%%%%%%%%%%%%%%%5

	\rput(0,1.5){(d)}
	\cnodeput(1,1.5){n1}{$\state[x]$}
	\cnodeput(3,1.5){n2}{}
	\ncline{->}{n1}{n2}
	\aput{:U}{$\aalpha[x]$}
	\bput{:U}{$\atimer=0$}

	\cnodeput(4.5,1.5){n5}{$\state[m]$}
	\ncline[linestyle=dotted]{->}{n2}{n5}
	%\bput{:U}{label}

	\cnodeput(6.6,1.5){n6}{$\state[n]$}
	\ncline{->}{n5}{n6}
	\aput{:U}{$\aalpha[m]$}
	\bput{:U}{ $\btimer=0$} 

	\cnodeput(8,1.5){n7}{}
	\ncline[linestyle=dotted]{->}{n6}{n7}

	\cnodeput(12,1.5){n8}{$\state[y]$}
	\ncline{->}{n7}{n8}
	\aput{:U}{$\aalpha[y]$}
	\bput{:U}{$\timer{\atimer<\aconst[1]\land\btimer<\aconst[2]}$}

	\end{pspicture}
	\end{center}
	\caption{Possible interactions between two timers $\atimer$ and $\btimer$ along a 
	path from $\state[x]$ to $\state[y]$. In the absence of any other timers:
	(a) no upper bound can be inferred, since there is a potentially unbounded delay
	between $\state[m]$ and $\state[n]$. 
	(b) max delay is $\aconst[1]+\aconst[2]$.
	(c) max delay is $\aconst[1]$. 
	(d) max delay is $\aconst[1]$. 
	(e) max delay is $\aconst[1]+\aconst[2]$.
	}
	\label{fig:twotimers}
	\end{figure*}

	Beyond two timers, however, it is not immediately clear how to compute a tight
	upper bound along a given path, but we conjecture that it is possible to do so efficiently
	(in polynomial time).  
	%\input{interactions2}
\end{comment}
