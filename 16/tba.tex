\subsection{Timed B\"{u}chi Automata}
\label{sec:tba}
Whereas we model the timing properties of the child processes of a cluster system using
the timed finite automata of the previous section, 
we model these properties of the parent using a timed variant of 
$\omega$-automata, specifically
Timed B\"uchi Automata. We assume a basic familiarity with these; due to space constraints, we give 
only brief overview here.  
To review briefly, 
$\omega$-automata, like standard finite automata, also consist of a finite number of states, 
but instead operate over words of infinite length. Classes of $\omega$-automata are distinguished
by their acceptance criteria. \buchi automata, which we consider in this paper, are defined
to accept their input if and only if a run over the input string visits an accepting state 
infinitely often. Other classes of $\omega$-automata exist as well. For example, Muller automata
are more stringent, specifying their acceptance criteria as a \emph{set} of acceptance sets; a
Muller automaton accepts its input if and only if the set of states visited infinitely often
is specified as an acceptance set.  More detailed specifics can be found elsewhere---
for example, \cite{tba}.

A \emph{Timed B\"uchi Automaton} (TBA) is a tuple 
$\tba{\Aalpha}{\astates}{\state[0]}{\accept}{\aclks}{\tbareln}{\ireln}{\creln}$, where
\begin{itemize}
\item $\Aalpha$ is a finite alphabet, 
\item $\astates$ is a finite set of states,
\item $\state[0]\in\astates$ is the start state,
\item $\accepts\subseteq\astates$ is a set of accepting states,
\item $\clocks$ is a set of clocks,
\item $\reln\subseteq \astates\times\astates\times \Aalpha$ is the state transition relation,
\item $\ireln \subseteq \reln\times\powset{\clocks}$ is the clock initialization relation, and
\item $\creln \subseteq \reln\times\constraints[\clocks]$ is the constraint relation.
\end{itemize}

A tuple $\tuple{\state[i],\state[j],\aalpha}\in\reln$ indicates that a symbol
$\aalpha$ yields a transition from state $\state[i]$ to state $\state[j]$, subject to the
restrictions specified by the clock constraints in $\creln$.
A tuple $\tuple{\state[i],\state[j],\aalpha, \clocks}\in\ireln$ indicates that on the
transition on symbol $\aalpha$ from $\state[i]$ to $\state[j]$, all clocks in 
$\clocks$ are to be initialized to $0$.
Finally, a tuple $\tuple{\state[i],\state[j],\aalpha, \constraints[\clocks]}\in\creln$ indicates that 
the transition on $\aalpha$ from $\state[i]$ to $\state[j]$ can only be
taken if the constraint $\constraints[\clocks]$ evaluates to true under the values of the current timer interpretation.

We define \emph{paths}, \emph{runs}, and \emph{subruns} over a TBA analagously to those over a TFA:
\begin{definition}[Path (TBA)]
Let $\atba$ be a TBA with state set $\astates$ and transition relation $\reln$.
$\path{\state[1],...,\state[n]}$ is a \emph{path} over $\atfa$ if, for all 
$1 \le i < n$, $\some{\aalpha}{\tuple{\state_i, \state_{i+1},\aalpha} \in \reln}$.
\end{definition}
\begin{definition}[Run, Subrun (TBA)]
A run (subrun) $\arun$, denoted by $\pair{\states}{\clkints}$, 
of a Timed B\"uchi Automaton
$\tba{\Aalpha}{\astates}{\state[0]}{\accept}{\aclks}{\tbareln}{\ireln}{\creln}$
over a timed word $\pair{\astring}{\timeseq}$,
is an infinite (finite) sequence of the form
\[
\arun: 
\pair{\state[0]}{\clkint[0]} 
  \runto{{\timing[1]}}{\aalpha[1]}
\pair{\state[1]}{\clkint[1]} 
  \runto{{\timing[2]}}{\aalpha[2]}
\pair{\state[2]}{\clkint[2]} 
  \runto{{\timing[3]}}{\aalpha[3]}
...
\]
satisfying the same requirements as given in Definition~\ref{def:runtfa}.
\begin{comment}
	satisfying the following requirements:
	\begin{itemize}
	\item Initialization: $\lookup{\clkint[0]}{\aclk}=0, \forall \aclk\in\clocks$
	\item Consecution: For all $i\ge 0$: 
		\begin{itemize}
		\item $\tbareln\ni\tuple{\state[i],\state[i+1],\aalpha[i]}$, 
		\item $\uconstraint_i$ evaluates to true under 
	$\clkint[i-1] + \timing[i] - \timing[i-1]$, where $\creln\ni \tuple{\state[i],\state[i+1],\aalpha[i],\uconstraint_i}$, and 
		\item $\uconstraint_1 = (\clkint[i-1] + \timing[i] - \timing[i-1])\with{\map{\atimer}{0}}$, 
			$\forall \atimer \in \atimers$, where $\ireln\ni\tuple{\state[i],\state[j],\aalpha[i],\atimers}$
		\end{itemize}
	\end{itemize}
\end{comment}
\label{def:runtba}
\end{definition}

For a run $\arun$, the set $\inf{\arun}$ denotes the set of states which are
visited infinitely many times. A TBA $\atba$ with final states $\accepts$
accepts a timed word $\aword=\pair{\astring}{\timeseq}$ if $\inf{\arun} \bigcap \accepts \neq \emptyset$, 
where $\arun$ is the run of $\aword$ on $\atba$. That is, a TBA accepts its input
if any of the states  from $\accepts$ repeat an infinite number of times in  $\arun$.

%%%%%%%%%%%%%%%
	\begin{comment}
	Similarly, we define the concept of a finite prefix of a run of a timed word
	over a TBA:
	\begin{definition}[Prefix]
	A prefix $\aprefix$, denoted by $\pair{\states}{\clkints}$ 
	of a TBA $\tba{\Aalpha}{\astates}{\state[0]}{\accept}{\aclks}{\tbareln}{\ireln}{\creln}$
	over a timed word $\pair{\aalpha}{\timing}$,
	is a finite sequence of the form
	\[
	\arun: 
	\pair{\state[0]}{\clkint[0]} 
	  \runto{{\timing[1]}}{\aalpha[1]}
	\pair{\state[1]}{\clkint[1]} 
	  \runto{{\timing[2]}}{\aalpha[2]}
	  ...
	  \runto{{\timing[k]}}{\aalpha[k]}
	\pair{\state[k]}{\clkint[k]} 
	\]
	satisfying the same requirements as in Definition~\ref{def:run}.
	\end{definition}
	\end{comment}
%%%%%%%%%%%%%%%

\input{tbaex1}
%

Lastly, we take the concept of maximum delay, introduced in the previous section with respect to
Timed Finite Automata, and extend it to apply to Timed B\"{u}chi Automata. Doing so first requires the following
definition, which allows us to restrict the timing analysis for TBAs to finite subwords: 
\begin{definition}[Subword over $\states$]
Let $\atba$ be a TBA, and let $\states=\path{\state[m]...\state[n]}$ be a finite path over $\atba$.
A finite timed word $\aword=\pair{(\aalpha[m]...\aalpha[n])}{(\timing[m]...\timing[n])}$ is a 
\emph{subword over} $\states$ iff 
$\exists{
	\state[0],...,\state[m-1],\aalpha[0],...,\aalpha[m-1], \timing[0],...,\timing[m-1]
}$
such that $\pair{
	\state[0]...\state[m-1]\state[m]...\state[n]
}{
	\clkints		
}$
%$\pair{\states}{\clkints}$ 
is a subrun of $\pair{
	(\aalpha[0]...\aalpha[m-1]\aalpha[m]...\aalpha[n])
}{
	(\timing[0]...\timing[m-1]\timing[m]...\timing[n])
}$ over $\atba$ for some $\clkints$.
\label{def:subwordOver}
\end{definition}
Definition~\ref{def:subwordOver} is a technicality which is necessary to support the following 
definition of the maximum delay between states of a TBA:
\begin{definition}
Let $\atba$ be a TBA, and let $\states$ be a finite path over $\atba$.
Then $\delayTBA[\atba]{\states}$ is the maximum duration of any subword over 
$\states$.
\end{definition}
\begin{example}
Consider $\atba[1]$ from Example~\ref{ex:tba1}. Then
$\delayTBA[{\atba[1]}]{\state[1]\state[2]\state[2]\state[1]}=50$.
\end{example}

Algorithmically computing $\delayTBA[\atba]{\states}$ for a TBA $\atba$ is analogous to the case 
for TFAs;
in small cases (i.e., relatively few timers with small time constraints),
the analysis is relatively simple, while we conjecture the problem for more complex cases to be
intractable; we leave more detailed analysis for future work.

\endinput
