\subsection{Timed Finite Automata}
\label{sec:tafsa}
In this section, we define a simple timed extension 
of traditional finite state automata and the words they accept. We will use these 
in later sections to model the timing properties of child processes in a real-time 
cluster system.  

\emph{Timed strings} take the form $(\astring,\timeseq)$, where $\astring$ is a string of symbols, 
and $\timeseq$ is a monotonically increasing sequence of reals (timestamps).  
$\timing[x]$ denotes the timestamp at which symbol $\aalpha[x]$ occurs.  We 
also use the notation  $\pair{\aalpha[x]}{\timing[x]}$ to denote a particular symbol/timestamp pair.  For instance, the timed string $\pair{(abc)}{(1,10,11)}$ is equivalent to the sequence
$(a,1) (b,10) (c,11)$, and both represent the case where `a' occurs at time 1, `b' at time 10, 
and `c' at time 11.

Correspondingly, we extend traditional finite automata to include a set of 
\emph{timers}, which impose temporal restrictions along state transitions.
A timer can
be \emph{initialized} along a transition, setting its value to 0 when the transition is taken,
and it can be \emph{used} along a transition, indicating that the transition can only be taken
if the value of the timer satisfies the specified constraint. 
Formally, we associate with each automaton a set of timer variables $\atimers$, and 
following the nomenclature of \cite{tba}, an \emph{interpretation} $\clkint$ for this set
of timers is an assignment of a real value to each of the timers in $\clocks$. 
We write $\clkint\with{\map{\atimer}{0}}$ to denote the interpretation $\clkint$
with the value of timer $\atimer$ reset to 0.
Clock constraints consist of conjunctions of upper bounds:
\begin{definition}
For a set $\atimers$ of clock variables, the set $\constraints[\clocks]$ of clock constraints $\uconstraint$
is defined inductively as
\[
	\uconstraint := \LT{\atimer}{\aconst}\  |\  \uconstraint_1 \land \uconstraint_2
\]
where $\atimer$ is a clock in $\atimers$ and $\aconst$ is a constant in $\reals[+]$.
\end{definition}
While this definition may seem overly restrictive compared to
some other treatments (e.g. \cite{tba}), 
we believe it to be acceptable in this early work for
a couple of reasons. First, while simple, this sole syntactic form 
remains expressive enough to capture an interesting, non-trivial set of use cases (e.g.
Section~\ref{sec:cases}).
Secondly, the timing analysis %in Section~\ref{sec:tfadelay} 
in subsequent sections of the paper
becomes rather complex, even 
when timers are limited
to this single form. Restricting the syntax in this manner simplifies this analysis 
to a more manageable level. We leave more complex formulations and the corresponding 
analysis for future work.


\begin{definition}[Timed Finite Automaton (TFA)]
A Timed Finite Automaton (TFA) is a tuple 
\[\tuple{\Aalpha,\astates,\start,\accept,\clocks,\reln,\ireln,\creln}\], where 
\begin{itemize}
\item $\Aalpha$ is a finite alphabet, 
\item $\astates$ is a finite set of states,
\item $\start\in\astates$ is the start state,
\item $\accept\in\astates$ is the accepting state,
\item $\clocks$ is a set of clocks,
\item $\reln\subseteq \astates\times\astates\times \Aalpha$ is the state transition relation,
\item $\ireln \subseteq \reln\times\powset{\clocks}$ is the clock initialization relation, and
\item $\creln \subseteq \reln\times\constraints[\clocks]$ is the constraint relation.
\end{itemize}
A tuple $\tuple{\state[i],\state[j],\aalpha}\in\reln$ indicates that a symbol
$\aalpha$ yields a transition from state $\state[i]$ to state $\state[j]$, subject to the
restrictions specified by the timer constraints in $\creln$.
A tuple $\tuple{\state[i],\state[j],\aalpha, \set{\atimer[1],...,\atimer[n]}}\in\ireln$ indicates that on the
transition on symbol $\aalpha$ from $\state[i]$ to $\state[j]$, all of the specified timers
are to be initialized to $0$.
Finally, a tuple $\tuple{\state[i],\state[j],\aalpha, \constraints[\clocks]}\in\creln$ indicates that 
the transition on $\aalpha$ from $\state[i]$ to $\state[j]$ can only be
taken if the constraint $\constraints[\clocks]$ evaluates to true under the current timer interpretation.
\end{definition}

\begin{example}
The following TFA accepts the timed language 
$\setst{\pair{ab^*c}{\timing[1]...\timing[n]}}{\timing[n]-\timing[1] < 10}$
(i.e., the set of all strings consisting of an `a', followed by an arbitrary number of `b's,
followed by a `c', such that the elapsed time between the first and last symbols is no greater
than 10 time units).
The start state is denoted with a dashed circle, and the accepting state with a double line. 

\begin{center}
\begin{pspicture}(4,1.5)
\cnodeput[linestyle=dashed](0,0.4){n1}{$\state[1]$}
\cnodeput(2,0.4){n2}{$\state[2]$}
\cnodeput[doubleline=true](4,0.4){n3}{$\state[3]$}
\ncline{->}{n1}{n2}
\aput{:U}{a}
\bput{:U}{$\atimer=0$}

\ncline{->}{n2}{n3}
\aput{:U}{c}
\bput{:U}{$\timer{\atimer<10}$}

\ncarc[arcangleA=135,arcangleB=135,ncurv=5]{->}{n2}{n2}
\aput{:U}{b}
\end{pspicture}
\end{center}
%The $\trans{\state[1]}{a}{\state[2]}$ transition initiates a timer $\atimer$, setting its
%value to 0. Consequently, the $\trans{\state[2]}{c}{\state[3]}$ transition 
%can only proceed if the value of timer $\atimer$ is less than 10--- in other words, 
%if the elapsed time
%between the initial $a$ and the final $c$ is 10 or fewer time units.
\end{example}

Paths and runs are defined in the standard way:
\begin{definition}[Path]
Let $\atfa$ be a TFA with state set $\astates$ and transition relation $\reln$.
Then $\path{\state[1],...,\state[n]}$ is a \emph{path} over $\atfa$ if, for all 
$1 \le i < n$, $\some{\aalpha}{\tuple{\state_i, \state_{i+1},\aalpha} \in \reln}$.
\end{definition}

\begin{definition}[Run]
A run $\arun$ of a TFA
$\tfa{\Aalpha}{\astates}{\state[0]}{\accept}{\aclks}{\tbareln}{\ireln}{\creln}$
over a timed word $\pair{\astring}{\timeseq}$,
is a sequence of the form
\[
\arun: 
\pair{\state[0]}{\clkint[0]} 
  \runto{{\timing[1]}}{\aalpha[1]}
\pair{\state[1]}{\clkint[1]} 
  \runto{{\timing[2]}}{\aalpha[2]}
\pair{\state[2]}{\clkint[2]} 
  \runto{{\timing[3]}}{\aalpha[3]}
...
  \runto{{\timing[n]}}{\aalpha[n]}
\pair{\state[n]}{\clkint[n]} 
\]
satisfying the following requirements:
\begin{itemize}
\item Initialization: $\lookup{\clkint[0]}{\aclk}=0, \forall \aclk\in\clocks$
\item Consecution: For all $i\ge 0$: 
	\begin{itemize}
	\item $\tbareln\ni\tuple{\state[i],\state[i+1],\aalpha[i]}$, 
	\item $(\clkint[i-1] + \timing[i] - \timing[i-1])$ satisfies $\uconstraint_i$,
where $\creln\ni \tuple{\state[i-1],\state[i],\aalpha[i],\uconstraint_i}$, and 
	\item $\clkint[i] = (\clkint[i-1] + \timing[i] - \timing[i-1])\with{\map{\atimer}{0}}$, 
		$\forall \atimer \in \atimers$, where $\ireln\ni\tuple{\state[i],\state[j],\aalpha[i],\atimers}$
	\end{itemize}
\end{itemize}
$\arun$ is an \emph{accepting} run if $\state[n]=\accept$.
\label{def:runtfa}
\end{definition}

A TFA $\atfa$ \emph{accepts} a timed string 
$s=\pair{\astring}{\timing[1]...\timing[n]}$ if there is an accepting 
run of $s$ over $\atfa$, and $\timing_n-\timing_1$ is called the \emph{duration} of the string.

\begin{note}[Well-Formedness]
We introduce a restriction on how timers can be used in a TFA, thus defining 
what it means for a TFA to be \emph{well-formed}.
Namely, we restrict timers to be used only once along a path; this is to simplify somewhat
the timing analysis in subsequent sections.
In particular, we say that a TFA $\atfa$ is well-formed if, for all pairs of states 
$\pair{\state[x]}{\state[y]}$, all timers $\atimer$, and all paths from 
$\state[x]$ to $\state[y]$, $\atimer$ is used no more than once.
For example, the TFAs shown in Figure~\ref{fig:malformed} are not well-formed, since in both cases, timers can
potentially be used more than once--- in the first case ($\atfa[1]$), along the self-loop 
on $\state[2]$, and in the second case ($\atfa[2]$), along two separate transitions along the path.
At first, this may appear to be overly restrictive, but as it turns out, many of these
cases can easily be rewritten equivalently to conform to the single-use restriction, as
shown in Figure~\ref{fig:wellformed}.
\input{malformedTFA}
\input{wellformedTFA}
\end{note}


%%%
\begin{comment}
	\begin{definition}
	For a transition $\apair=\tuple{\state[x],\state[y],\aalpha}\in\tbareln$, we define the set 
	of timers \emph{spanning} $\apair$ as the set of timers which are active over $\apair$,
	Or more formally, 
	$\timerspan{\state[x]}{\state[y]}{\aalpha}=\setst{\atimer}{\phi_1 \lor \phi_2}$,
	where
	\begin{align*}
	\phi_1 &= \creln \ni \tuple{\state[x],\state[y],\aalpha,\constraints[\atimer]} \\
	\phi_2 &= \exists{\state[m],\state[n]}. 
		\begin{array}[t]{ll}
		(\state[m]\goesto\state[x]) &\land\\
				(\state[y]\goesto\state[n]) &\land\\
				(\ireln\ni\tuple{\state[m],\state[x],\atimer})&\land\\
				(\creln\ni\tuple{\state[y],\state[n],\constraints[\atimer]})&
		\end{array}
	\end{align*}
	With respect to the transition $\tuple{\state[x],\state[y],\aalpha}\in\tbareln$,
	$\phi_1$ states that $\atimer$ can be used along the transition, and
	$\phi_2$ states that $\atimer$ is initialized along some transition on the path prior to 
	the transition, and used along some transition after it.
	\end{definition}
\end{comment}

\begin{comment}
	\begin{definition}
	Let $\atfa=\tfa{\Aalpha}{\astates}{\state[0]}{\accept}{\aclks}{\tbareln}{\ireln}{\creln}$ 
	be a timed finite automaton.
	For a path $\apath=\path{\state[1],...,\state[n]}$ through $\atfa$,
	and a timer $\atimer$, 
	we say that the \emph{maximum bound} of $\atimer$ along $\apath$,
	written as $\timerbound{\atimer}{\apath}$, is 
	\begin{itemize}
	\item $z$, if
	$\creln\ni\tuple{\state[i],\state[j],\aalpha,\atimer<z}$, for some $\aalpha$ 
	and some $1\le i\le j\le n$
	\item $\infty$ otherwise.
	\end{itemize}
	\end{definition}
\end{comment}
%%%
\input{delaybounds}

\endinput
