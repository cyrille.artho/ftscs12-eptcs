\section{Case Study: Matrix Multiplication}
\label{sec:cases}
We now turn our attention to a practical application of the concepts discussed so far. Namely,
we demonstrate the use of the formal validation concepts on a simple parallel, 
MPI-style \cite{mvapich,mpi} matrix multiplication kernel,
extracted from the larger power-grid analysis application described in \cite{rtss-wip,hui-sc}.
Our kernel implements a variant of Fox's algorithm for matrix multiplication \cite{FoxMM}. For simplicity,
we assume square matrices, and that the number of columns, rows, and processors
are all perfect squares. The algorithm distributes the
task of multiplying two matrices amongst all processors in the system. 

We give a simple distributed algorithm for matrix multiplication, and
a consistent parallel timing system for that algorithm. We conclude the section with empirical
results--- timing measurements taken on a small, four-node real-time cluster, 
each node consisting of dual quad-core 2.66Ghz Xeon X5660 processors running 
the Xenomai RTOS with 48GB RAM.
The timing measurements of the PTS,
along with the usual restrictions associated with real-time computation
(e.g. no virtual memory or paging, process scheduling, ensuring minimal variance in execution timings,
etc.),
are bounded by virtue of Xenomai's real-time process scheduler.
The result is a matrix multiplication kernel which provably runs in under 9 ms per cycle
for $128\times 128$ double-precision matrices.
We emphasize
that we are not claiming the speed of the operation to be a groundbreaking result--- obviously, 
this is a relatively small matrix size, but was so chosen as this is the order of the size 
required by our targeted application kernel. 
Rather, we give these numbers, as well as the PTS,
to illustrate the \emph{process} by which we analyze the temporal interactions between processes,
thus showing this delay to be a provable upper bound. 


\subsection{Algorithm}
\begin{algorithm}
{%header
\begin{tabular}{lll}
$p$&:&Number of processors\\
$N$&:&Rank of matrices
\end{tabular}
}
\caption{MatrixMultiply: Compute $\cmat = \amat \times \bmat$}
\label{alg:mm}
\begin{algorithmic}[1]
\STATE $q \gets \sqrt{p}$ 
\WHILE{\TRUE} \label{algline:mm:while}
\STATE $dest\gets 1$ \label{algline:mm:dest}
%\STATE $\stfork{1..q-1}$ \label{algline:mm:fork}
\IF[Master process]{self $== 0$}  \label{algline:mm:master}
	\FOR{$i = 0$ to $q-1$}  \label{algline:mm:masterstart}
		\FOR{$j=0$ to $q-1$}
		\STATE $w \gets i\frac{N}{q}, x\gets (i+1)\frac{N}{q}$ \label{algline:mm:partition1}
		\STATE $y \gets j\frac{N}{q}, z\gets (j+1)\frac{N}{q}$ 
		\STATE $\xs \gets \amat[w:x][0:N]$
		\STATE $\ys \gets \bmat[0:N][y:z]$ \label{algline:mm:partition2}
		\IF[Master already has these chunks]{$i\neq 0$ \AND $j \neq 0$} \label{algline:mm:if}
			\STATE $\send{\xs}{dest}$ \label{algline:mm:send1}
			\STATE $\send{\ys}{dest}$ \label{algline:mm:send2}
			\STATE $dest\gets dest + 1$ \label{algline:mm:incdest}
		\ENDIF
		\ENDFOR
	\ENDFOR \label{algline:mm:masterend}
\ELSE[Child processes] \label{algline:mm:child}
	\STATE $\xs \gets \recv{0}$\label{algline:mm:recv1}
	\STATE $\ys \gets \recv{0}$\label{algline:mm:recv2}
\ENDIF
\STATE $\zs \gets \locmm{\xs}{\ys}$\label{algline:mm:locMM}
\STATE $\wb{\zs}{\cmat}$\label{algline:mm:wb}
%\STATE $\stjoin{1..q-1}$\label{algline:mm:join}
\ENDWHILE \label{algline:mm:endwhile}
\end{algorithmic}
\label{algo:mm}
\end{algorithm}

The pseudocode for the algorithm is given in Algorithm~\ref{algo:mm}.
Conceptually, to multiply two $N\times N$ matrices $\amat$ and $\bmat$  using a $p$ processor cluster, each matrix is 
divided into 
%$\sqrt{p}$ segments: matrix $\amat$ is divided into $\sqrt{p}$ sets of $\frac{N}{\sqrt{p}}$ rows each,
%and matrix $\bmat$ into $\sqrt{p}$ sets of $\frac{N}{\sqrt{p}}$ columns each. The row and column sets of 
%$\amat$ and $\bmat$ are then distributed in a round-robin fashion amongst the processors of the cluster:
%processors $0$ through $\sqrt{p}-1$ each receive the first row set of matrix $\amat$, as well as
%column sets $1, 2, ..., \sqrt{p}$, respectively, of matrix $\bmat$. Processors $\sqrt{p}$ through $2\sqrt{p}-1$
%receive the second row set of $\amat$, as well as column sets $1, 2, ..., \sqrt{p}$, respectively, of $\bmat$,
%and so on, until processors $p-\sqrt{p}$ through $p-1$ receive the $\sqrt{p}$'th row set of $\amat$, as well as 
%column sets $1, 2, ..., \sqrt{p}$, respectively, of $\bmat$.
segments, which are then distributed in round-robin fashion amongst the processors of the cluster.
Each processor then performs a
local matrix multiplication on its own local submatrices, and the results of these local operations are 
aggregated (reduced) to form the matrix product $\amat \times \bmat$.

Due to space constraints, we will not describe the partitioning in detail;
Figure~\ref{fig:mm-ex} shows the partitioning and distribution of work by Algorithm~\ref{algo:mm} for a four-processor cluster.
In this figure, $\amat,\bmat,$ and $\cmat$ are all $N \times N$ matrices. $\amat$ is partitioned into
2  sets of $\frac{N}{2}$ rows each, and  $\bmat$ is partitioned into 2 sets of $\frac{N}{2}$ columns each.
The master process, $p_0$, computes the local product $\amat_1\times\bmat_1$, and writes the result to $\cmat_1$.
$p_0$ then sends submatrices $\amat_1$ and $\bmat_2$ to $p_1$, who 
then computes their product, writing the result to $\cmat_2$.
Similarly, 
$p_2$ receives and computes $\cmat_3=\amat_2\times\bmat_1$,  and
$p_3$ receives and computes $\cmat_4=\amat_2\times\bmat_2$. 

%As is commonly the case in MPI-type programs, the algorithm distinguishes
%between the parent and child processes based on the process id, with 
%process 0 denoting the master process (line \ref{algline:mm:master}), and all
%others denoting a child process (line \ref{algline:mm:child}). 

The algorithm proceeds as follows: the master process 
executes lines \ref{algline:mm:masterstart}  through \ref{algline:mm:masterend},
which partition $\amat$ and $\bmat$ into submatrices 
(lines \ref{algline:mm:partition1}--\ref{algline:mm:partition2}), 
and send these parts out to the respective child processes (lines
\ref{algline:mm:send1}--\ref{algline:mm:send2}).
Conversely, the child processes execute lines~\ref{algline:mm:recv1}--\ref{algline:mm:recv2}, which 
receive the
submatrices assigned by the master process.
Lines~\ref{algline:mm:locMM}--\ref{algline:mm:wb} are run
by all processes, including the master process (which, in this case, participates
in the task of matrix multiplication as well). Line \ref{algline:mm:locMM}
performs the local operation, line~\ref{algline:mm:wb} writes the local
result to the appropriate location in $\cmat$.%, and line~\ref{algline:mm:join}
%waits for all child processes to complete before proceeding.
The entire process then repeats indefinitely, as given by the
\textbf{while} loop (lines~\ref{algline:mm:while} and \ref{algline:mm:endwhile}).

\begin{figure}[hlt]
\begin{center}
\begin{pspicture}(0,0.5)(8,3)
\psframe(0,1)(2,3) % matrix A
\psframe(3,1)(5,3) % matrix B
\psframe(6,1)(8,3) % matrix C

% A's horizontal lines
\psline(0,2)(2,2)

% B's vertical lines
\psline(4,1)(4,3)

% C's lines
\psline(7,1)(7,3)
\psline(6,2)(8,2)

% Labels
\rput(1,0.5){$\amat$}
\rput(4,0.5){$\bmat$}
\rput(7,0.5){$\cmat$}
\rput(2.5,2){$\times$}
\rput(5.5,2){$=$}

% A's section labels
\rput(1,2.5){$\amat_1$}
\rput(1,1.5){$\amat_2$}

% B's section labels
\rput(3.5,2){$\bmat_1$}
\rput(4.5,2){$\bmat_2$}

% C's section labels
\rput(6.5,2.5){$\cmat_1$}
\rput(7.5,2.5){$\cmat_2$}

\rput(6.5,1.5){$\cmat_3$}
\rput(7.5,1.5){$\cmat_4$}
\end{pspicture}
\end{center}
\caption{Partitioning and distribution of matrix multiplication by Algorithm~\ref{algo:mm} across a four
processor cluster.  
}
\label{fig:mm-ex}
\end{figure}

%\subsection{Empirical Results}
%Timings were taken between event points carefully chosen
%to reflect key points in the algorithm which are most likely to dominate the 
%running time of the respective processes--- for instance, the time required to send data across 
%the network to a 
%remote process. New states and timers could have been created for every
%statement in the process, but, as the running time for many of these intermediate states
%is comparatively negligible, we only model events at a slightly coarser level
%of granularity. The resulting events and points have been translated into 
%states and transitions in the corresponding parallel timing system, detailed in the following section.
%
\subsection{Parallel Timing System}
Figure \ref{pts:mm} shows a parallel timing system for Algorithm~\ref{algo:mm} across
a four processor cluster, consisting
of the TBA $\tbaparent[MM]$, which models the master process, and a child TFA $\tbachild[MM]$,
modeling instances of the child processes. Specific events have been elided from the diagram
in this case, since events in this case always represent transitions between statements.

\subsubsection{Parent}
States in the parent automaton $\tbaparent$ 
are prefixed with a `P', followed by
the line number as given in Algorithm~\ref{algo:mm}.
For example, $P\ref{algline:mm:dest}$ corresponds to
the state of the parent process as it is executing line \ref{algline:mm:dest}.

Additionally, lines \ref{algline:mm:send1} and \ref{algline:mm:send2} each beget
three separate states--- parameterized on the values of the loop induction
variables $i$ and $j$--- and are labeled accordingly. As is commonly the case
in WCET analysis, unrolling the loop nest in this
fashion is necessary in order to obtain a strict upper bound on the number of
iterations and, consequently, the total execution time, of the loop nest.

$\tbaparent$ forms, in this case, a simple cycle. The cycle starts at state
$\tbaparent\ref{algline:mm:dest}$, and steps sequentially through the steps
(states) of the algorithm.  Namely, the parent process starts at line $\ref{algline:mm:dest}$
(i.e., state $\tbaparent\ref{algline:mm:dest}$),
and proceeds sequentially through lines $\ref{algline:mm:master}$ 
(state $\tbaparent\ref{algline:mm:master}$), and eventually
to line $\ref{algline:mm:send1}$ ($\stij{\tbaparent\ref{algline:mm:send1}}{0}{1}$). The delay between 
the initialization 
(state $\tbaparent\ref{algline:mm:dest}$) and the first send 
($\stij{\tbaparent\ref{algline:mm:send1}}{0}{1}$) is
bounded by a timer, $\atimer[setup1]$ (the idea being that this is the delay incurred
by the time to ``set up" the first send). 
Execution then proceeds to line~$\ref{algline:mm:send2}$ 
($\stij{\tbaparent\ref{algline:mm:send2}}{0}{1}$); the delay along this transition
represents the time to send the first chunk to the respective child process, and is
bounded by timer $\atimer[send1]$. At this point, execution proceeds to 
line~$\ref{algline:mm:incdest}$ ($\stij{\tbaparent\ref{algline:mm:incdest}}{0}{1}$). Along this transition, there are two items to note:
first, the time to process the second send is bounded by  the timer $\atimer[send2]$,
and second, the child process has now been sent the data it needs,
and consequently, $\tbachild[1]$ is forked. Execution proceeds similarly through the
next six states, representing the unwound iterations of the loop nest. 
Child process $\tbachild[2]$ is similarly forked on the transition from $\stij{\tbaparent\ref{algline:mm:send2}}{1}{0}$
to $\stij{\tbaparent\ref{algline:mm:incdest}}{1}{0}$, and
$\tbachild[3]$ on the transition from $\stij{\tbaparent\ref{algline:mm:send2}}{1}{1}$
to $\stij{\tbaparent\ref{algline:mm:incdest}}{1}{1}$.
Execution then proceeds through lines
$\ref{algline:mm:locMM}$ (state $\tbaparent\ref{algline:mm:locMM}$) and $\ref{algline:mm:wb}$
($\tbaparent\ref{algline:mm:wb}$).
The duration of the local 
matrix multiplication operation (line~$\ref{algline:mm:locMM}$) is bounded by the timer
$\atimer[MM]$, and that through the reduce operation (line~$\ref{algline:mm:wb}$)
by the timer $\atimer[reduce]$. Additionally, the transition from   
$\tbaparent\ref{algline:mm:wb}$ back to   $\tbaparent\ref{algline:mm:dest}$ waits
for (joins with) all child processes to complete before proceeding.

\begin{comment}
	The parent automaton makes use of several timers which constrain delays between
	the various states. These are summarized in the following table:

	\begin{tabular}{l|c|l}
	\emph{Timer} & \emph{Constrains delay} & \emph{Description}\\
	      & \emph{between} &\\
	\hline
	$\atimer[setup1]$ & 
		$\tbaparent\ref{algline:mm:dest} \to \stij{\tbaparent\ref{algline:mm:send1}}{0}{1}$
		& Max delay to set up the first \textbf{send}.\\
	\hline
	$\atimer[send1]$ & 
		$ \stij{\tbaparent\ref{algline:mm:send1}}{0}{1} 
		\to
		\stij{\tbaparent\ref{algline:mm:send2}}{0}{1} $
		& Max delay to execute first \textbf{send}.\\
		&& (line \ref{algline:mm:send1}, $i=0,j=1$)\\
	\hline
	$\atimer[send2]$ & 
		$\stij{\tbaparent\ref{algline:mm:send2}}{0}{1} 
		\to	
		\stij{\tbaparent\ref{algline:mm:incdest}}{0}{1} $
		& Max delay to execute second \textbf{send}.\\
		&& (line \ref{algline:mm:send2}, $i=0,j=1$)\\
	\hline
	$\atimer[setup2]$ & 
		$ \stij{\tbaparent\ref{algline:mm:incdest}}{0}{1} \to \stij{\tbaparent\ref{algline:mm:send1}}{1}{0}$
		& Max delay to set up the third \textbf{send}.\\
	\hline
	$\atimer[send3]$ & 
		$ \stij{\tbaparent\ref{algline:mm:send1}}{1}{0} 
		\to
		\stij{\tbaparent\ref{algline:mm:send2}}{1}{0} $
		& Max delay to execute third \textbf{send}.\\
		&& (line \ref{algline:mm:send1}, $i=1,j=0$)\\
	\hline
	$\atimer[send4]$ & 
		$ \stij{\tbaparent\ref{algline:mm:send2}}{1}{0} 
		\to
		\stij{\tbaparent\ref{algline:mm:incdest}}{1}{0} $
		& Max delay to execute fourth \textbf{send}.\\
		&& (line \ref{algline:mm:send2}, $i=1,j=0$)\\
	%
	\hline
	$\atimer[setup3]$ & 
		$ \stij{\tbaparent\ref{algline:mm:incdest}}{1}{0} \to \stij{\tbaparent\ref{algline:mm:send1}}{1}{1}$
		& Max delay to set up the fifth \textbf{send}.\\
	\hline
	$\atimer[send5]$ & 
		$ \stij{\tbaparent\ref{algline:mm:send1}}{1}{1} 
		\to
		\stij{\tbaparent\ref{algline:mm:send2}}{1}{1} $
		& Max delay to execute fifth \textbf{send}.\\
	\hline
	$\atimer[send6]$ & 
		$ \stij{\tbaparent\ref{algline:mm:send2}}{1}{1} 
		\to
		\stij{\tbaparent\ref{algline:mm:incdest}}{1}{1} $
		& Max delay to execute sixth \textbf{send}.\\
	\hline
	$\atimer[setupMM]$ & 
		$\stij{\tbaparent\ref{algline:mm:incdest}}{1}{1} 
		\to
		\tbaparent\ref{algline:mm:locMM}$
	& Max delay between sixth send and local matrix multiply.
	\\
	\hline
	$\atimer[MM]$ & 
		$ \tbaparent\ref{algline:mm:locMM}
		\to
		\tbaparent\ref{algline:mm:wb}$
	& Max delay to execute local matrix multiply.
	\\
	\hline
	$\atimer[reduce]$ & 
		$ \tbaparent\ref{algline:mm:wb}
		\to
		\tbaparent\ref{algline:mm:dest} $
	& Max delay to aggregate local results.
	\end{tabular}


	For example, the timer $\atimer[setup1]$ constrains the maximum allowable
	delay between states $\tbaparent\ref{algline:mm:dest}$ and
	$\stij{\tbaparent\ref{algline:mm:send1}}{0}{1}$, and this models the longest
	time (delay) required to execute lines 
	\ref{algline:mm:dest} through \ref{algline:mm:if} on the first pass through the loop nest---
	in other words, the time to set up the sending of $\amat_1$ to the first remote process.
	$\atimer[send1]$ models the longest delay required to execute this send, and so forth.
\end{comment}
\subsubsection{Child}
In this case, the child processes are modeled by the TFA $\tbachild$.
Nomenclature is analogous to that of $\tbaparent$: states in $\tbachild$ are
prefixed with an $\tbachild$, followed by the corresponding line number  from 
Algorithm~\ref{algo:mm}.

The child process starts at line $\ref{algline:mm:child}$ (state $\tbachild\ref{algline:mm:child}$).
The process then proceeds to receive the first block of data (line $\ref{algline:mm:recv1}$,
state $\tbachild\ref{algline:mm:recv1}$). The time to process the receive is bounded
by timer $\atimer[recv1]$. Execution proceeds to receive the second block of data
 (line $\ref{algline:mm:recv2}$, state $\tbachild\ref{algline:mm:recv2}$). The time
 to process this second receive is bounded by $\atimer[recv2]$.  Execution proceeds  next
 to the local matrix multiplication 
 (line $\ref{algline:mm:locMM}$, state $\tbachild\ref{algline:mm:locMM}$); the time
 spent on this operation is bounded by timer $\atimer[locMM]$.
 Finally, execution proceeds to the data writeback 
 (line $\ref{algline:mm:wb}$, state $\tbachild\ref{algline:mm:wb}$); the time
 spent on this operation is bounded by timer $\atimer[reduce]$.




\begin{figure*}
\begin{center}
\input{parentDFA}
\end{center}
\caption{Parallel timing system $\apts[MM]$ for Algorithm~\ref{algo:mm} across a four processor cluster.
Events have been elided for the sake of clarity. Upper bounds on timer constraints correspond to delay measurements
taken over our implementation; times are given in milliseconds.
Minimal variance from these bounds is ensured to the extent
provided by the underlying RTOS.}
\label{pts:mm}
\end{figure*}

\begin{theorem}
$\apts[MM]$ is consistent.
\begin{proof}
Let $\flatten{\apts[MM]}=\pair{\ireln'}{\creln'}$,
with $\tbaparent[MM]'=\tba{\Aalpha}{\astates}{\state[0]}{\accepts}{\aclks}{\tbareln}{\ireln'}{\creln'}$. By definition, 
$\uses{\apts[MM]} = \set{
		\pair{\aedge[1]}{\aedge[4]},
		\pair{\aedge[2]}{\aedge[4]},
		\pair{\aedge[3]}{\aedge[4]}}$, where
\begin{align*}
	\aedge[1] &= \pair{\stij{\tbaparent{\ref{algline:mm:send2}}}{0}{1}}{\stij{\tbaparent{\ref{algline:mm:incdest}}}{0}{1}} &
	\aedge[2] &= \pair{\stij{\tbaparent{\ref{algline:mm:send2}}}{1}{0}}{\stij{\tbaparent{\ref{algline:mm:incdest}}}{1}{0}}\\
	\aedge[3] &= \pair{\stij{\tbaparent{\ref{algline:mm:send2}}}{1}{1}}{\stij{\tbaparent{\ref{algline:mm:incdest}}}{1}{1}} &
	\aedge[4] &= \pair{\tbaparent{\ref{algline:mm:wb}}}{\tbaparent{\ref{algline:mm:dest}}}
\end{align*}
By observation, there are three paths which we must consider:
\begin{align*}
\apath[1] &= \path{
		   \stij{\tbaparent{\ref{algline:mm:send2}}}{0}{1}
		   \stij{\tbaparent{\ref{algline:mm:incdest}}}{0}{1}...
		   \tbaparent\ref{algline:mm:wb}
		   \tbaparent\ref{algline:mm:dest}
}\\
\apath[2] &= \path{
		   \stij{\tbaparent{\ref{algline:mm:send2}}}{1}{0}
		   \stij{\tbaparent{\ref{algline:mm:incdest}}}{1}{0}...
		   \tbaparent\ref{algline:mm:wb}
		   \tbaparent\ref{algline:mm:dest}
}\\
\apath[3] &= \path{
		   \stij{\tbaparent{\ref{algline:mm:send2}}}{1}{1}
		   \stij{\tbaparent{\ref{algline:mm:incdest}}}{1}{1}...
		   \tbaparent\ref{algline:mm:wb}
		   \tbaparent\ref{algline:mm:dest}
}
\end{align*}

The rest of the proof follows by enumeration:
\begin{align*}
	(\delayTBA[{\tbaparent[MM]'}]{\apath[1]} &= 6.1)  <  (\delayTBA[{\tbaparent[MM]}]{\apath[1]} = 8.1)     \\
	(\delayTBA[{\tbaparent[MM]'}]{\apath[2]} &= 6.1)  <  (\delayTBA[{\tbaparent[MM]}]{\apath[2]} = 7.3)     \\
	(\delayTBA[{\tbaparent[MM]'}]{\apath[3]} &= 6.1)  <  (\delayTBA[{\tbaparent[MM]}]{\apath[3]} = 6.5) 
\end{align*}

\end{proof}
\label{thm:mmcons}
\end{theorem}

Finally, we note that the worst case delay along one iteration of the algorithm
is 8.9 ms. This follows from the observation that the parent automaton $\tbaparent$
takes the form of a simple cycle with no unbound segments
(i.e., subpaths which are not constrained by any timer). Specifically,
$\tbaparent$ consists of consecutive pairs of segments, each constrained by pairs of timers.
%of the form shown in Figure~\ref{fig:twotimers}(b). 
Consequently, we 
can derive an upper bound for a single iteration of the algorithm by summing
the bounds of all of the timers, yielding the specified upper bound.
Combined with Theorem~\ref{thm:mmcons}, which ensures that the timing of the child processes does 
not invalidate this bound, we are left with a cyclic, parallel, time-bounded matrix multiplication 
kernel.


\endinput
