


\section{Formal Analysis of the Airplane Turning  Control System }
\label{sec:analysis}

This section explains  
how we have formally analyzed the above Real-Time
Maude model of the multirate synchronous design  of the airplane
turning control system, 
and how the turning control system  has been improved as a result of our analysis.
Multirate PALS  ensures that the verified properties
also hold in a distributed real-time realization of the design.
%
There are two important requirements that 
the airplane turning control system  should satisfy:
%
\begin{itemize}
	\item \emph{Liveness}: 
	the airplane should reach the goal direction within a reasonable time with a stable status 
	in which both the roll angle and the yaw angle are close to $0$.��
	\item \emph{Safety}: during a turn, the yaw angle should always be close to $0$.
\end{itemize}


We first analyze deterministic behaviors when the airplane turns $+ 60^\circ$ to the right.%
\footnote{In our model, a turn of positive degrees is a right turn, and one of negative degrees a left turn.}
In this case, there are no outer environment inputs to the pilot console,
so that the pilot console sends the goal angles in the scenario to the main controller in turn.
We consider the following three scenarios:

\begin{enumerate}
	\item The pilot gradually increases the goal direction in 6 seconds, $+10^\circ$ for each second.
	\item The pilot sets the goal direction to $+ 60^\circ$ at the first step.
	\item\label{item:scenario-3}
	 The goal direction is at first $- 30^\circ$, and then it is suddenly set to $+ 60^\circ$ in one second.
\end{enumerate}

\begin{figure}[htb]
\centering
\includegraphics[trim=0.9cm 0.7cm 0.7cm 0.9cm, width=\textwidth] {simul.png}    
\caption{Simulation results for turning scenarios: the directions of
  aircraft (left), the roll angles (top right), and the yaw angles
  (bottom right)}  \label{fig:simul} 
\end{figure}

\noindent
Figure~\ref{fig:simul} shows the simulation results for these three scenarios up to $6$ seconds,
obtained by using the Real-Time Maude simulation command 
\quad\texttt{(trew
  \char123 model(\(\mathit{Scenario}\)){\char125} in time <= 6000
  .)}, 
where  \texttt{model(\(\mathit{Scenario}\))} gives the initial state of the system
with the scenario $\mathit{Scenario}$ for the pilot console.
For example, the scenario \ref{item:scenario-3} is represented as the term ``@d(-30.0) d(90.0)@'' for a list of directions.
As we can see in the graph, the airplane reaches the goal direction $60^\circ$ in a fairly short time,
and the roll angle also goes to a stable status.
However, the yaw angle seems to be quite unstable.
% and sometimes becomes greater than $1.5^\circ$
%when the pilot gives a sharp turn command to the main controller.

There are basically two reasons why the yaw angle is not sufficiently close to $0$ during a turn.
First, since all control functions are linear, the new angles for the ailerons and the rudder are not small enough 
when the yaw angle is near $0$. Second, the roll angle is sometimes changing too fast, so that the rudder
cannot effectively counter the adverse yaw.
Therefore, we modify the control functions as follows:

\small
\begin{alltt}
ceq horizWingAngle(CR, GR) 
  = sign(FR) * (if abs(FR)\;>\;1.0 then min(abs(FR)\;*\;0.3, 45.0) else FR\;\emph{^\;2.0}\;*\;0.3 fi) 
 if FR := angle(GR - CR) . \vspace{1ex}
ceq tailWingAngle(CY) 
  = sign(FY) * (if abs(FY)\;>\;1.0 then min(abs(FY)\;*\;0.8, 30.0) else FY\;\emph{^\;2.0}\;*\;0.8 fi)
 if FY := angle(-\,CY) . \vspace{1ex}
ceq goalRollAngle(CR, CD, GD)
  = if abs(FD\;*\;0.32\;-\;CR) > 1.5 then CR + sign(FD\;*\;0.32\;-\;CR) * 1.5 else FD\;*\;0.32 fi 
 if FD := angle(GD - CD) .
\end{alltt}
\normalsize

\noindent
When the difference between the goal and the current angles (i.e., @FR@ or @FY@) is less than
or equal to~$1$,
the functions @horizWingAngle@ and @tailWingAngle@ are now \emph{not} proportional
to the difference, but proportional to the \emph{square} of it.
Furthermore, the goal roll angle can be changed at most $1.5^\circ$ at a time,
so that there is no more abrupt rolling.


In order to check if the new control functions are safe, we define some auxiliary functions.
The function \texttt{$\mathit{PortId}$ ?= $\mathit{Component}$}
returns the content of the corresponding port in the $\mathit{Component}$:

\small
\begin{alltt}
eq P ?= < C : Component | ports : < P : Port | content : DL > PORTS >  =  DL .
\end{alltt}
\normalsize

\noindent
and the function \texttt{safeYawAll($\mathit{OutputDataList}$)} checks whether every output data 
in the given list has a~safe yaw angle, namely, an angle less than $1.0^\circ$.

\small
\begin{alltt}
eq safeYawAll(DO LI) = abs(yaw(DO)) < 1.0 and safeYawAll(LI) .
eq safeYawAll(nil)   = true .
\end{alltt}
\normalsize

\noindent
Then, we can verify, using the Real-Time Maude search command,
that there is no dangerous yaw angle within sufficient time bound;
e.g., for the scenario \ref{item:scenario-3}:

\small
\begin{alltt}
Maude> \emph{(tsearch [1] \char123model(d(-30.0) d(90.0)){\char125} =>* \char123SYSTEM\char125} 
        \emph{such that not safeYawAll(output ?= SYSTEM) in time <= 27000 .)} \vspace{1ex}
No solution
\end{alltt}
\normalsize

\noindent
Although  each state of the transition system captures only the slow steps for the top ensemble 
(i.e., every $600 \mathrm{ms}$),
 @safeYawAll@ also checks all fast steps for the main controller (every $60 \mathrm{ms}$),
since it accesses the \emph{history} of the main controller's status %during each $600 \mathrm{ms}$ 
in the output port of the top ensemble,
which the main controller sends to the top ensemble for each fast step of it.


Furthermore, we can use Real-Time Maude's LTL model checking to verify
both liveness and safety  at the same time.
The desired property is that the airplane reaches the desired direction with a stable status while keeping the yaw
angle close to $0$, which can formalized as  the  LTL formula
\[
\square (\neg \texttt{stable} \to (\texttt{safeYaw} \;\mathbf{U}\; (\texttt{reach} \wedge \texttt{stable})))
\]
where the atomic propositions \texttt{safeYaw}, \texttt{stable}, and \texttt{reach}
are defined as follows:

\small
\begin{alltt}
 eq \char123SYSTEM\char125 |= safeYaw = safeYawAll(output ?= SYSTEM) .
 eq \char123SYSTEM\char125 |= stable  = stableAll(output ?= SYSTEM) .
ceq \char123SYSTEM\char125 |= reach   = abs(angle(goal(DO) - dir(DO))) < 0.5 
 if DO := last(output ?= SYSTEM) .
\end{alltt}
\normalsize

\noindent
and the function \texttt{stableAll($\mathit{OutputDataList}$)} returns @true@
only if both the yaw angle and the roll angle are less than $0.5^\circ$ for every output data
in the $\mathit{OutputDataList}$.
We have verified 
that all three scenarios  satisfy the above LTL property with the \emph{new} control functions, 
using the time-bounded LTL model checking command of Real-Time Maude:

\small
\begin{alltt}
Maude> \emph{(mc \char123model(d(-30.0) d(90.0))\char125 |=t [] (~\,stable -> (safeYaw U reach\;/\char92\;stable))} 
           \emph{in time <= 7200 .)}  \vspace{1ex}   
Result Bool : true
\end{alltt}
\normalsize

%\noindent
%Each model checking command generates $13$ system states.


Finally, we have verified nondeterministic behaviors in which the pilot sends
one of the turning angles $-60.0^\circ$, $-10.0^\circ$, $0^\circ$, $10^\circ$, and $60.0^\circ$
to the main controller for $6$ seconds.
Such nondeterministic behaviors can be defined by adding the following five rewrite rules,
which nondeterministically assign one of these values to the input port of the pilot console:

\small
\begin{alltt}
rl possibleEnvOutput => input = d(0.0) .
rl possibleEnvOutput => input = d(10.0) .   rl possibleEnvOutput => input = d(-10.0) .
rl possibleEnvOutput => input = d(60.0) .   rl possibleEnvOutput => input = d(-60.0) .
\end{alltt}
\normalsize

\noindent
The following model checking command then shows that 
our redesigned system, with the new control functions, satisfies the above LTL property
within $18$ seconds, where one of the above five angles is nondeterministically
chosen and added to the angle $0^\circ$ in the scenario for each step of the pilot console:

\small
\begin{alltt}
Maude> \emph{(mc \char123model(d(0.0) d(0.0) d(0.0) d(0.0) d(0.0) d(0.0))\char125} 
             \emph{|=t} 
           \emph{[] (~ stable -> (safeYaw U (reach\;/\char92\;stable))) in time <= 18000 .)} \vspace{1ex}
Result Bool : true
\end{alltt}
%32163434ms cpu (32242017ms real) (137329 rewrites/second)
\normalsize

\noindent
The number of states explored in this model checking analysis is $246,785$,%
\footnote{This analysis took almost $9$ hours on Intel Core i5 2.4 GHz with $4$ GB memory.}
which is a huge state space reduction compared to the distributed asynchronous
model since:
\begin{inparaenum}[(i)]
	\item asynchronous behaviors are eliminated thanks to PALS, and
	\item any intermediate fast steps 
		for the sub-components are merged into a single-step of the system's top-level ensemble.
\end{inparaenum}

