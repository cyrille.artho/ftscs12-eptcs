\begin{thebibliography}{10}
\providecommand{\bibitemdeclare}[2]{}
\providecommand{\surnamestart}{}
\providecommand{\surnameend}{}
\providecommand{\urlprefix}{Available at }
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\href}[2]{\texttt{#2}}
\providecommand{\urlalt}[2]{\href{#1}{#2}}
\providecommand{\doi}[1]{doi:\urlalt{http://dx.doi.org/#1}{#1}}
\providecommand{\bibinfo}[2]{#2}

\bibitemdeclare{inproceedings}{abdullah-multirate}
\bibitem{abdullah-multirate}
\bibinfo{author}{A.~\surnamestart Al-Nayeem\surnameend},
  \bibinfo{author}{L.~\surnamestart Sha\surnameend}, \bibinfo{author}{D.~D.
  \surnamestart Cofer\surnameend} \& \bibinfo{author}{S.~M. \surnamestart
  Miller\surnameend} (\bibinfo{year}{2012}):
  \emph{\bibinfo{title}{Pattern-Based Composition and Analysis of Virtually
  Synchronized Real-Time Distributed Systems}}.
\newblock In: {\sl \bibinfo{booktitle}{Proc. ICCPS'12}},
  \bibinfo{publisher}{IEEE}, pp. \bibinfo{pages}{65--74},
  \doi{10.1109/ICCPS.2012.15}.

\bibitemdeclare{book}{anderson2005introduction}
\bibitem{anderson2005introduction}
\bibinfo{author}{J.D. \surnamestart Anderson\surnameend}
  (\bibinfo{year}{2005}): \emph{\bibinfo{title}{Introduction to flight}}.
\newblock \bibinfo{publisher}{McGraw-Hill}.

\bibitemdeclare{unpublished}{ftscs12-techrep}
\bibitem{ftscs12-techrep}
\bibinfo{author}{K.~\surnamestart Bae\surnameend},
  \bibinfo{author}{J.~\surnamestart Krisiloff\surnameend},
  \bibinfo{author}{J.~\surnamestart Meseguer\surnameend} \&
  \bibinfo{author}{P.~C. \surnamestart {\"O}lveczky\surnameend}
  (\bibinfo{year}{2012}): \emph{\bibinfo{title}{PALS-Based Analysis of an
  Airplane Multirate Control System in {R}eal-{T}ime {M}aude (Extended
  Version)}}.
\newblock \bibinfo{note}{Manuscript available at
  \url{http://formal.cs.illinois.edu/kbae/airplane/}}.

\bibitemdeclare{inproceedings}{bae-facs12}
\bibitem{bae-facs12}
\bibinfo{author}{K.~\surnamestart Bae\surnameend},
  \bibinfo{author}{J.~\surnamestart Meseguer\surnameend} \&
  \bibinfo{author}{P.~C. \surnamestart {\"O}lveczky\surnameend}
  (\bibinfo{year}{2012}): \emph{\bibinfo{title}{Formal Patterns for Multi-Rate
  Distributed Real-Time Systems}}.
\newblock In: {\sl \bibinfo{booktitle}{Proc.\ FACS'12}},
  \bibinfo{series}{LNCS}, \bibinfo{publisher}{Springer}.
\newblock \bibinfo{note}{To appear}.

\bibitemdeclare{inproceedings}{icfem11}
\bibitem{icfem11}
\bibinfo{author}{K.~\surnamestart Bae\surnameend}, \bibinfo{author}{P.~C.
  \surnamestart {\"O}lveczky\surnameend}, \bibinfo{author}{A.~\surnamestart
  Al-Nayeem\surnameend} \& \bibinfo{author}{J.~\surnamestart
  Meseguer\surnameend} (\bibinfo{year}{2011}):
  \emph{\bibinfo{title}{Synchronous {AADL} and its Formal Analysis\; in
  {R}eal-{T}ime {M}aude}}.
\newblock In: {\sl \bibinfo{booktitle}{Proc.\ ICFEM'11}}, {\sl
  \bibinfo{series}{LNCS}} \bibinfo{volume}{6991},
  \bibinfo{publisher}{Springer}, pp. \bibinfo{pages}{651--667},
  \doi{10.1007/978-3-642-24559-6\_43}.

\bibitemdeclare{book}{maude-book}
\bibitem{maude-book}
\bibinfo{author}{M.~\surnamestart Clavel\surnameend},
  \bibinfo{author}{F.~\surnamestart Dur\'an\surnameend},
  \bibinfo{author}{S.~\surnamestart Eker\surnameend},
  \bibinfo{author}{P.~\surnamestart Lincoln\surnameend},
  \bibinfo{author}{N.~\surnamestart Mart\'\i-Oliet\surnameend},
  \bibinfo{author}{J.~\surnamestart Meseguer\surnameend} \&
  \bibinfo{author}{C.~\surnamestart Talcott\surnameend} (\bibinfo{year}{2007}):
  \emph{\bibinfo{title}{All About {Maude} -- A High-Performance Logical
  Framework}}.
\newblock {\sl \bibinfo{series}{LNCS}} \bibinfo{volume}{4350},
  \bibinfo{publisher}{Springer}, \doi{10.1007/978-3-540-71999-1}.

\bibitemdeclare{book}{collinson1996introduction}
\bibitem{collinson1996introduction}
\bibinfo{author}{R.~P.~G. \surnamestart Collinson\surnameend}
  (\bibinfo{year}{1996}): \emph{\bibinfo{title}{Introduction to avionics}}.
\newblock \bibinfo{publisher}{Chapman \& Hall}.

\bibitemdeclare{inproceedings}{lepri-wrla12}
\bibitem{lepri-wrla12}
\bibinfo{author}{D.~\surnamestart Lepri\surnameend},
  \bibinfo{author}{E.~\surnamestart {\'A}brah{\'a}m\surnameend} \&
  \bibinfo{author}{P.C. \surnamestart Olveczky\surnameend}
  (\bibinfo{year}{2012}): \emph{\bibinfo{title}{Timed CTL Model Checking in
  Real-Time Maude}}.
\newblock In: {\sl \bibinfo{booktitle}{Proc. WRLA'12}}, {\sl
  \bibinfo{series}{LNCS}} \bibinfo{volume}{7571},
  \bibinfo{publisher}{Springer}, pp. \bibinfo{pages}{182--200},
  \doi{10.1007/978-3-642-34005-5\_10}.

\bibitemdeclare{inproceedings}{icfem10}
\bibitem{icfem10}
\bibinfo{author}{J.~\surnamestart Meseguer\surnameend} \&
  \bibinfo{author}{P.~C. \surnamestart {\"O}lveczky\surnameend}
  (\bibinfo{year}{2010}): \emph{\bibinfo{title}{Formalization and Correctness
  of the {PALS} Architectural Pattern for Distributed Real-Time Systems}}.
\newblock In: {\sl \bibinfo{booktitle}{Proc.\ ICFEM'10}}, {\sl
  \bibinfo{series}{LNCS}} \bibinfo{volume}{6447},
  \bibinfo{publisher}{Springer}, pp. \bibinfo{pages}{303--320},
  \doi{10.1007/978-3-642-16901-4\_21}.

\bibitemdeclare{article}{pals-tcs}
\bibitem{pals-tcs}
\bibinfo{author}{J.~\surnamestart Meseguer\surnameend} \&
  \bibinfo{author}{P.~C. \surnamestart {\"O}lveczky\surnameend}
  (\bibinfo{year}{2012}): \emph{\bibinfo{title}{Formalization and correctness
  of the {PALS} architectural pattern for distributed real-time systems}}.
\newblock {\sl \bibinfo{journal}{Theor.\ Comp.\ Sci.}} \bibinfo{volume}{451},
  pp. \bibinfo{pages}{1--37}, \doi{10.1016/j.tcs.2012.05.040}.

\bibitemdeclare{inproceedings}{dasc09}
\bibitem{dasc09}
\bibinfo{author}{S.~P. \surnamestart Miller\surnameend}, \bibinfo{author}{D.~D.
  \surnamestart Cofer\surnameend}, \bibinfo{author}{L.~\surnamestart
  Sha\surnameend}, \bibinfo{author}{J.~\surnamestart Meseguer\surnameend} \&
  \bibinfo{author}{A.~\surnamestart Al-Nayeem\surnameend}
  (\bibinfo{year}{2009}): \emph{\bibinfo{title}{Implementing Logical Synchrony
  in Integrated Modular Avionics}}.
\newblock In: {\sl \bibinfo{booktitle}{Proc.\ DASC'09}},
  \bibinfo{publisher}{IEEE}, \doi{10.1109/DASC.2009.5347579}.

\bibitemdeclare{article}{journ-rtm}
\bibitem{journ-rtm}
\bibinfo{author}{P.~C. \surnamestart {\"O}lveczky\surnameend} \&
  \bibinfo{author}{J.~\surnamestart Meseguer\surnameend}
  (\bibinfo{year}{2007}): \emph{\bibinfo{title}{Semantics and Pragmatics of
  {Real-Time Maude}}}.
\newblock {\sl \bibinfo{journal}{Higher-Order and Symbolic Computation}}
  \bibinfo{volume}{20}(\bibinfo{number}{1-2}), pp. \bibinfo{pages}{161--196},
  \doi{10.1007/s10990-007-9001-5}.

\bibitemdeclare{inproceedings}{steiner2011tta}
\bibitem{steiner2011tta}
\bibinfo{author}{W.~\surnamestart Steiner\surnameend} \&
  \bibinfo{author}{J.~\surnamestart Rushby\surnameend} (\bibinfo{year}{2011}):
  \emph{\bibinfo{title}{TTA and PALS: Formally verified design patterns for
  distributed cyber-physical systems}}.
\newblock In: {\sl \bibinfo{booktitle}{Proc. DASC'11}},
  \bibinfo{organization}{IEEE}, \doi{10.1109/DASC.2011.6096120}.

\end{thebibliography}
