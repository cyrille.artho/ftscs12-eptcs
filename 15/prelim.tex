
\section{Preliminaries on  Multirate PALS and Real-Time Maude}
\label{sec:prelim}

\subsection{Multirate PALS}
In many distributed real-time systems, such as automotive and avionics systems, the
system design is essentially  
a \emph{synchronous design} that must be realized in a distributed setting. 
Both design and verification of such \emph{virtually synchronous}
distributed real-time systems is very hard 
because of asynchronous communication, network delays, clock skews,
and because the state space explosion caused by the system's
concurrency can make it unfeasible to apply model checking to verify
required properties. 
%
The (single-rate) PALS (``physically asynchronous, logically
synchronous'') formal design pattern~\cite{icfem10,dasc09}  reduces
the design and verification of  
  a  virtually synchronous distributed real-time system to the much
  simpler task of designing and verifying its  
synchronous version,  provided that the network infrastructure can
guarantee bounds on the messaging delays and the skews of the local
clocks. 


%However, it is a fact of  life that different
%sensors and effectors  need to operate at different rates, and
%that this  necessitates  using slower rates in
%the distributed control hierarchies that orchestrate and synchronize
%their actions in, say, a  car or an airplane.
%
We have %therefore 
recently developed Multirate PALS \cite{bae-facs12},
which extends PALS
to  hierarchical \emph{multirate} systems
in which  controllers with the same rate  communicate with
each other and with a number of faster components. 
As is common for hierarchical control
systems~\cite{abdullah-multirate}, 
we assume that the period of the higher-level controllers is a multiple of the period of each fast component.
%
%The point is that, 
Given  a multirate synchronous design $\mathfrak{E}$,  bounds
$\Gamma$ on the network transmission times and clock skews, and
function 
$T$ assigning to each distributed component its period, Multirate
PALS
defines a transformation $(\mathfrak{E}, T, \Gamma) \mapsto
\MA(\mathfrak{E}, T, \Gamma)$
mapping each synchronous design $\mathfrak{E}$ % with  parameters $T$ and $\Gamma$ 
to a specification $\MA(\mathfrak{E}, T, \Gamma)$ of the
corresponding distributed multirate real-time system. 
In~\cite{bae-facs12} we formalize Multirate PALS
and show that the  synchronous design $\mathfrak{E}$ and the
asynchronous distributed model $\MA(\mathfrak{E}, T, \Gamma)$ satisfy
the same temporal logic properties.

\paragraph{Multirate Synchronous Models.}

In Multirate PALS, the synchronous design is formalized as the synchronous composition of a collection of \emph{typed machines},  
 an  \emph{environment}, and a \emph{wiring diagram} that  connects the machines. 
A \emph{typed machine}  $M$ is a tuple 
$M = (\mathcal{D}_i, S, \mathcal{D}_o,\delta_{M})$,
 where $\mathcal{D}_i=D_{i_1}\times \cdots \times D_{i_n}$ is an \emph{input set},  
 $S$ is  a  set of \emph{states},
$\mathcal{D}_o=D_{o_1}\times \cdots \times D_{o_m}$ is an \emph{output set},  
and $\delta_{M}\subseteq (D_i\times S)  \times (S\times D_o)$ is 
a  \emph{transition relation}.  %That is,  
Such a machine $M$ has $n$ input ports and $m$ output ports.



To compose a collection of machines with different rates into a
synchronous system in which all components perform one transition in
lock-step in each iteration of the system, we  ``slow down'' the
faster components so that all components run at the slow rate in the
synchronous composition.  A fast machine that is slowed, or
\emph{decelerated}, by a factor $k$ \emph{performs $k$ internal
transitions} in  one synchronous step. Since the fast machine consumes 
an input and produces an output in each of these internal
steps, the decelerated machine consumes (resp.\ produces) $k$-tuples
of inputs (resp.\ outputs) in each synchronous  step.
A $k$-tuple output
from the fast machine must therefore be \emph{adapted} so that it can be read by
the slow component. That is, the $k$-tuple  must be transformed to a
single value (e.g., the average of the $k$ values, the last 
 value, or any other function of the $k$ values); this
transformation is formalized as an \emph{input adaptor}. Likewise,  the single output from a slow component
must be transformed to a $k$-tuple of inputs to the fast machine;
this  is also done by input adaptors which 
may, for example,   transform an input   $d$ to a $k$-tuple $(d, \bot,
\ldots, \bot)$ for some ``don't care'' value $\bot$. 
Formally, an input adaptor $\alpha_M$ for a typed machine $M = (\mathcal{D}_i, S, \mathcal{D}_o,\delta_{M})$
 is a family of functions 
 $\alpha_M = \{ \alpha_k : D_k' \to D_{i_k}\}_{k \in \{1,\ldots,n\}}$,
each of which determines a desired value from an output $D_k'$ of another typed machine.



Typed machines (with rate assignments and input
adaptors)  can be ``wired together'' into \emph{multirate machine ensembles} as shown in 
 Figure~\ref{fig:pals2},
 where ``local'' fast environments are  integrated with their corresponding fast machines.
 A multirate machine ensemble is a tuple 
\[
\mathfrak{E} = (J_S \cup J_F \cup \{e\}, \{M_j\}_{j\in J_s \cup J_F}, E, \mathit{src}, \mathit{rate}, \mathit{adaptor})
\]
 where:
\begin{inparaenum}[(i)]
	\item $J_S$ (resp., $J_F$) is a  finite set of \emph{indices} for controller components (resp., fast components),
	and $e\not\in J_S \cup J_F$ is the \emph{environment index},
 	\item $\{M_j\}_{j\in J_S \cup J_F}$ is  a family of typed machines,
	\item the \emph{environment} is a  pair  $E=(\mathcal{D}^e_i, \mathcal{D}^e_o)$, 
	with $\mathcal{D}^{e}_i$   the environment's \emph{input set} and $\mathcal{D}^{e}_o$  its \emph{output set},
	\item $src$ is  a function that  assigns to each input port $(j, n)$ (input port $n$ of machine $j$) its ``source,''
	such that  there are no connections between fast machines,
	\item $\mathit{rate}$ is a function assigning to each fast machine a value denoting 
	how many times faster the machine runs compared to the controller machines, and
	\item $\mathit{adaptor}$ is a function that assigns an input adaptor to each $l \in J_F \cup J_S$.
\end{inparaenum}
%
 
 \begin{figure}[htb]
\centering
\includegraphics[trim=0.4cm 0.4cm 0.35cm 0.4cm, clip=true,width=0.6\textwidth]{mrEnsem.pdf}    
\caption{A multirate machine ensemble.
$M_1$ and $M_2$ are controller machines,
and $\mathit{env}_2$ and $\mathit{env}_3$ are local environments 
with faster rates, hidden from high-level controllers.
}  \label{fig:pals2}
\end{figure}




The transitions of all  machines in an ensemble are performed simultaneously,
where each fast machine with rate $k$ performs $k$ ``internal
transitions''  in one  synchronous transition step.
If  a machine has a feedback wire to itself and/or to another
machine, then the  output becomes an input at the \emph{next} instant.  
The \emph{synchronous composition} of  a multirate ensemble  $\mathfrak{E}$ is therefore
equivalent to a \emph{single machine} $M_{\mathfrak{E}}$,  where
each state of $M_{\mathfrak{E}}$ consists of the states of its
subcomponents and of the contents in the feedback outputs,
as
explained in~\cite{bae-facs12,pals-tcs}.
The synchronous composition of the ensemble in Figure~\ref{fig:pals2}
is the  machine given by the outer box. 
Since  $M_{\mathfrak{E}}$ is itself a typed machine
which can appear as a component in another multirate ensemble, we can 
easily define hierarchical multirate systems.




\subsection{Real-Time Maude}
Real-Time Maude~\cite{journ-rtm} extends the Maude language and
tool~\cite{maude-book} to support the formal modeling analysis of
real-time systems in rewriting logic. In Real-Time Maude, the data
types of the system are defined by  a \emph{membership equational
logic}~\cite{maude-book} 
theory $(\Sigma, E)$  with $\Sigma$ a signature%
\footnote{That is, $\Sigma$ is a set  of declarations of \emph{sorts}, \emph{subsorts}, and
  \emph{function symbols}.} 
and $E$ a set of {\em confluent and terminating
conditional equations}; the system's  \emph{instantaneous}
(i.e., zero-time) local transitions are defined by (possibly
conditional) \emph{labeled instantaneous rewrite rules}%
\footnote{$E = E'\cup A$, where $A$ is a set of axioms such as associativity and commutativity,  
so that deduction is performed \emph{modulo} $A$. 
%Operationally,  a term is reduced to its $E'$-normal form modulo $A$ before any rewrite rule is applied.
};
 and time elapse is modeled explicitly by \emph{tick rewrite rules} of the form 
\texttt{ crl\! [\(l\)]\! :\! \texttt{\char123}\(u\)\texttt{\char125}
  => \texttt{\char123}\(v\)\texttt{\char125} in time \(\tau\) if
  \(cond\)}, which  specifies a transition with duration $\tau$
from an instance of the term $\texttt{\char123}u\texttt{\char125}$ to the corresponding instance of the
term $\texttt{\char123}v\texttt{\char125}$. 


The Real-Time Maude syntax  is fairly intuitive.
 A function symbol $f$  is declared with the syntax \texttt{op }$f$ @:@ $s_1$ \ldots $s_n$ @->@ $s$, 
 where $s_1\:\ldots\:s_n$ are the sorts of  its arguments,  and $s$  is its (value) \emph{sort}. 
Equations are written with syntax 
 \quad@eq@ $u$ @=@ $v$,\quad or \quad@ceq@ $u$ @=@ $v$ \,@if@\, \emph{cond}\quad
for conditional equations. 
%The mathematical variables in such statements
% are declared with the keywords {\tt var} and {\tt vars}.
 We refer to~\cite{maude-book} for more details  on the syntax of
  Real-Time Maude. 
% We make extensive use of the fact that 
% an equation $f(t_1, \ldots, t_n) = t$ 
% with  the @owise@ (for ``otherwise'') attribute  can be applied to a subterm 
% $f(\ldots)$ only if no other equation with left-hand side
% $f(u_1, \ldots, u_n)$ can be applied.\footnote{A specification with 
% \texttt{owise} equations can be transformed to an equivalent system without
% such equations~\cite{maude-book}.}




A \emph{class} declaration
\quad
  @class@ \(C\) @|@ \(\attone\) @:@ \(\sonex\)@,@ \dots @,@ \(\attn\) @:@ \(\sn\) 
\quad %|
declares a class $C$ with attributes $att_1$ to $att_n$ of
sorts $s_1$ 
to $s_n$. An {\em object\/} of class $C$  is 
represented as a term
$@<@\: O : C \mid att_1: val_1, ... , att_n: val_n\:@>@$
 where $O$  is the
object's
\emph{identifier}, and where $val_1$ to 
$val_n$ are the current values of the attributes $att_1$ to
$att_n$.
The global 
 state has the form @{@$t$@}@, where $t$ 
 is a term of 
 sort @Configuration@ that     has 
the structure of a  \emph{multiset}  of objects and messages, with 
multiset union  denoted by a juxtaposition
operator  that is declared associative and commutative.
% so that rewriting is \emph{multiset rewriting} supported  in Real-Time Maude.
%
A \emph{subclass} inherits all the attributes and rules of its 
superclasses.

The dynamic behavior of concurrent
object systems is axiomatized by specifying each of its transition patterns by a rewrite rule. 
For example, the rule

\small
 \begin{alltt}
 rl [l] : m(O,w)  < O : C | a1 : x, a2 : O', a3 : z >   =>
                  < O : C | a1 : x + w, a2 : O', a3 : z >  dly(m'(O'),x) .
\end{alltt}
\normalsize

 \noindent  defines a parametrized family of transitions 
  in which a 
 message @m@, with parameters @O@ and @w@, is read and
 consumed by an object @O@ of class @C@. The transitions change 
 the attribute @a1@ of the  object @O@ and  send a new message
 @m'(O')@ \emph{with delay} @x@.  
 ``Irrelevant'' attributes (such as @a3@)
 need not be mentioned in a rule. 


A 
Real-Time Maude specification is \emph{executable}, and the tool 
provides a variety of formal analysis methods. 
The \emph{rewrite} command 
\quad@(trew @$t$@ in time <= @$\tau$@ .)@\quad 
simulates \emph{one} 
behavior of the system within time $\tau$, starting with a given initial state  $t$.
%
The \emph{search} command 

\begin{alltt}
(tsearch [\(n\)] \(t\) =>* \(pattern\) such that \(cond\) in time <= \(\tau\) .)
\end{alltt}

\noindent
uses a breadth-first strategy to find $n$ states reachable from the initial state $t$ within time $\tau$,
which match a \emph{pattern} and satisfy a \emph{condition}.
%
The Real-Time Maude's \emph{LTL model checker} 
   checks whether
each behavior from an initial state, possibly  up to a  time bound,
  satisfies a linear temporal logic 
  formula.
%The time-bounded model checking command has syntax 
%\begin{alltt}
%  (mc \(t\) |=t \(\mathit{formula}\) in time <= \(\tau\) .)
%\end{alltt}
%for initial state $t$ and temporal logic formula $\mathit{formula}$ .
 \emph{State propositions}  are 
operators of sort @Prop@. %  , and their semantics should be
% given by  equations of the form
%
% \small
% \begin{alltt}
%  ceq \texttt{\char123}\(\mathit{statePattern}\)\texttt{\char125} |= \(\mathit{prop}\) = \(b\) if \(cond\)
% \end{alltt} 
% \normalsize %|
%
% \noindent for $b$ a term of sort @Bool@, which defines the state proposition 
% $prop$ to hold in all
% states $@{@t@}@$ such that $@{@t@}@$ \verb+|=+ $\mathit{prop}$ evaluates
%  to @true@.
%
A temporal logic \emph{formula} is constructed by state
propositions and
temporal logic operators such as @True@, @~@ (negation),
@/\@, @\/@, @->@ (implication), @[]@ (``always''), @<>@
(``eventually''), @U@ (``until''), and @O@ (``next''). 
The command 
\quad
 @(mc@ \(\,t\) @|=u@ \(\,\varphi\)@ in time <= @\(\tau\) @.)@
\quad %|
  checks whether
the temporal logic formula $\varphi$ holds in all behaviors up to
duration $\tau$
 from the initial state $t$.
%and the command 
%\quad
% @(mc@ \(\,t\) @|=t@ \(\,\varphi\) @ in time <= @$\tau$@ .)@
%\quad
%checks the same but within time $\tau$.


