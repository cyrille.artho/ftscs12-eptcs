

\section{Multirate Synchronous Ensembles in Real-Time Maude}
\label{sec:framework}

We have defined a framework for formally modeling and analyzing
multirate ensembles in Real-Time Maude. Given a specification of
\emph{deterministic} 
single typed machines, their periods, input adaptors, a wiring
diagram, and one top-level nondeterministic environment,
our framework gives an executable Real-Time Maude model
of the synchronous composition of the ensemble. 
It is natural to assume that controller components are  deterministic;
however, if we have a nondeterministic component, our framework could
be modified 
so that local transitions are modeled by rewrite rules instead of equationally defined functions.
If the system has  ``local'' fast environments,
they should be dealt with by the corresponding fast machines.%
\footnote{An environment can  be viewed
  as a nondeterministic typed machine~\cite{pals-tcs}. Therefore, a
  faster machine's 
  environment  and the fast machine  itself form a nondeterministic
  2-machine ensemble.}  
%That is, the environment at
%the (slow) global level is only the environment of the high-level
%controllers.

This section gives a
brief overview of our framework and of how the user should specify
the ensemble; the entire definition of our framework is given in our
longer report~\cite{ftscs12-techrep}. 

\paragraph{Representing Multirate Ensembles in Real-Time Maude.}

The multirate  ensemble can be naturally specified in an object-oriented style,
where its machines and the ensemble can be modeled as objects.
A~typed machine is represented as an object instance of a subclass
of the base class \texttt{Component},
which has the common attributes @rate@, @period@, and @ports@.

\small
\begin{alltt}
class Component | rate : NzNat,   period : Time,   ports : Configuration .
\end{alltt}
\normalsize

\noindent
The @period@ denotes the period of  the typed machine, and 
 @rate@  denotes its rate in a multirate ensemble.
The \texttt{ports} attribute contains the input/output "ports"  of a typed machine,
represented as a multiset of \texttt{Port} objects, whose
\texttt{content} attribute contains the data content as 
a list of values of the supersort  \texttt{Data}:

\small
\begin{alltt}
class Port | content : List\char123Data\char125 .   class InPort .   class OutPort .
subclass InPort OutPort < Port .
\end{alltt}
\normalsize


For each typed machine, the user must define an appropriate subclass
of the class \texttt{Component}, 
the function \texttt{delta} defining its transition function, 
and the \texttt{adaptor} function for each input port:

\small
\begin{alltt}
op delta : Object -> Object .
op adaptor : ComponentId PortId NeList\char123Data\char125 -> NeList\char123Data\char125 .
\end{alltt}
\normalsize

\noindent
where the sort @NeList{Data}@ denotes a non-empty list of data.
%
%For example, we can specify the \texttt{Accumulator} component as follows,
%which keeps the sum of previously received input values and outputs
%the stored value in the previous round.
%
%\small
%\begin{alltt}
%class Accumulator | value : Nat .       --- a stored value
%subclass Accumulator < Component .
%ops in out : -> PortId [ctor] .         --- port names
%
%var C : ComponentId . vars N V : Nat . vars LI LO : List\char123Data\char125 .
%
%eq delta(
%   < C : Counter | ports : < in : InPort | content : N LI > 
%                           < out : OutPort : content : LO >,
%                   value : V >)
% = 
%   < C : Counter | ports : < in : InPort | content : LI > 
%                           < out : OutPort : content : LO V >,
%                   value : V + N > .
%\end{alltt}
%\normalsize
%
%\noindent
%Note that the \texttt{delta} function above consumes the first value \texttt{N}
%from the input data list \texttt{N LI} and appends the new output \texttt{V}
%to the existing output list \texttt{LO},
%so that it can be repeatedly applied for the decelerated case.
%%
%In the case of adaptors,
%if \texttt{acc1} is a controlled (fast) machine with rate $3$,
%and if \texttt{acc2} is a controller (slow) machine, 
%then their adaptor functions can be defined as follows:
%
%\small
%\begin{alltt}
%eq adaptor(acc1, in, N   ) = N bot bot .    --- 3 times faster
%eq adaptor(acc2, in, LI N) = N .            --- taking the last value
%\end{alltt}
%\normalsize
%
%\noindent




A multirate machine ensemble is modeled 
as an object instance of the class \texttt{Ensemble}.
We  support the definition of hierarchical ensembles by letting an
ensemble be a @Component@, 
which also contains the wiring diagram (\texttt{connections})
and the \texttt{machines} in the ensemble.
In this case, the \texttt{ports} attribute represents
the environment ports of the ensemble:

\small
\begin{alltt}
class Ensemble | machines : Configuration,   connections : Set\char123Connection\char125 .
subclass Ensemble < Component .
\end{alltt}
\normalsize %|


A wiring diagram  is modeled as a set of connections.
A  connection from an output port $P_1$ of a component $C_1$ to an
input port $P_2$ 
of a component $C_2$ is represented as a term
\texttt{\(C\sb{1}\).\(P\sb{1}\) --> \(C\sb{2}\).\(P\sb{2}\)}. 
Similarly, 
a connection between an environment port $P_1$ and a machine port $P_2$
of a subcomponent $C_2$ is represented as a term \texttt{\(P\) -->
  \(C\sb{2}\).\(P\sb{2}\)} (for an environment input) 
or a term \texttt{\(C\sb{2}\).\(P\sb{2}\) --> \(P\)} (for an environment output).

%For example,  in our case study,
%the airplane turning control system ensemble in Figure~\ref{fig:airplane}  
%is represented in Real-Time Maude by the term:
%
%\small
%\begin{alltt}
%< csystem : Ensemble | rate : 10, ports : ... 
%                       machines : < left : SubController | rate : 4, ... >
%                                  < right : SubController | rate : 4, ... >
%                                  < rudder : SubController | rate : 3, ... >
%                                  < main : MainController | rate : 1, ... >,
%                       connections : (input --> main . input) ; 
%                                     (main . output --> output) ;
%                                     (left . output --> main . inLW) ;
%                                     (main . outLW --> left . input) ;  ... >
%\end{alltt}
%\normalsize  %|

\paragraph{Defining Synchronous Compositions of Multirate Ensembles.}

Given these definitions of an ensemble, our framework defines its synchronous
composition as follows. 
The function \texttt{delta} of a multi-rate machine
ensemble~$\mathfrak{E}$  defines the transitions in the 
synchronous composition of $\mathfrak{E}$.


\small
\begin{alltt}
eq delta(< C : Ensemble | >)
 = transferResults(execute(transferInputs(< C : Ensemble | >))) .
\end{alltt}
\normalsize

\noindent
In the above equation, the function \texttt{transferInputs}  first
transfers to each input port 
a value in the corresponding environment output port or the feedback output port.
The \texttt{execute} function below  applies  the appropriate input adaptor to each sub-component,
and then performs the function \texttt{delta} of each component as
many times as its deceleration rate.
Finally, the new outputs in sub-components are transferred to 
the environment ports by the function \texttt{transferResults}.

\small
\begin{alltt}
eq execute(< C : Ensemble | machines : COMPS >)
 = < C : Ensemble | machines : executeSub(COMPS) > . 
eq executeSub(< C : Component | > COMPS)
 = k-delta(applyAdaptors(< C : Component | >)) executeSub(COMPS) .
eq executeSub(none) = none .
\end{alltt}
\normalsize

\noindent
where %For the $k$-step deceleration of a component,
 \texttt{k-delta}  applies \texttt{delta} 
as many times as the rate of a given typed machine:
%We assume that a delta function is defined to be repeatedly applied,
%as shown in the above example.

\small
\begin{alltt}
eq k-delta(< C : Component | rate : N >) = k-delta(N, < C : Component | >) .
eq k-delta(s N, OBJECT) = k-delta(N, delta(OBJECT)) .
eq k-delta(  0, OBJECT) = OBJECT .
\end{alltt}
\normalsize

\noindent
The rates and periods  should be consistent in an ensemble; 
that is, if some component has rate $k$ and period $p$, 
then any component with rate $k \cdot t$ in the same ensemble should have period $p / t$.

\paragraph{Formalizing the Synchronous Steps.}

When each typed machine is deterministic
and the system contains one (nondeterministic) top-level environment,
the dynamics of the entire system given by a multirate machine
ensemble is specified  
by the following  tick rewrite rule
that simulates each synchronous step of the composed system:

\small
\begin{alltt}
crl [step] : \char123< C\! : Component | period : T >\char125 
           => 
             \char123delta(envOutput(ENVOUTPUT, clearOutputs(< C\! : Component | >)))\char125 
            in time T
 if possibleEnvOutput => ENVOUTPUT .
\end{alltt}
\normalsize


In  the condition of the rule, \emph{any}  possible environment output
can be nondeterministically assigned to 
the variable \texttt{ENVOUTPUT}, since the 
constant @possibleEnvOutput@ can be rewritten to any possible
environment output 
by user-defined rewriting rules of the form 

\small
\begin{alltt}
rl possibleEnvOutput => (PortId\(\sb{1}\) = Value\(\sb{1}\), \(\ldots\), PortId\(\sb{n}\) = Value\(\sb{n}\)) .
\end{alltt}
\normalsize

\noindent
These  values are inserted into the appropriate component input port by the \texttt{envOutput} function,
after clearing the outputs generated in the previous round
by the @clearOutputs@ function. 
 
\footnotesize
\begin{alltt}
eq envOutput((P = DL,\;ENVASSIGNS),  < C\! : \!Component | ports\! : \!< P\! : \!InPort | > PORTS >)
 = envOutput(ENVASSIGNS,  < C\! : \!Component | ports\! : \!< P\! : \!InPort | content\! : \!DL > PORTS >) .
eq envOutput(empty, < C\! : \!Component | >) = < C\! : \!Component | > .
\end{alltt}
\normalsize

The \texttt{delta} function is finally applied to perform the transition of the component.
%with the given environment output.
Since an ensemble is an instance of the @Component@ class
and the @delta@ function is also given for the @Ensemble@ class,
this  tick rewrite rule can also be applied to a hierarchical multirate ensemble.
After each synchronous step, 
the input ports of the component contain the environment input given by 
the @possibleEnvOutput@,
and the output ports will have the resulting environment output generated by 
the @delta@ function.
The @period@ does not have any effect on the structure of
the transition system, 
but  can be useful to verify \emph{timed} properties, such as
time-bounded LTL properties
and metric CTL properties~\cite{lepri-wrla12}. 

%


