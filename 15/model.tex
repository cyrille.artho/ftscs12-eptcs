


\section{Modeling the Airplane Turning Control System}
\label{sec:airplane-model}

In this section we formally specify the airplane turning control
system in Section~\ref{sec:airplane-control}
using the ensemble framework in Real-Time Maude described in
Section~\ref{sec:framework}. 
The entire specification is available in our report~\cite{ftscs12-techrep}.
%
%\paragraph{Airplane Spec.}
%
The following parameters are
chosen to be representative of a small general aviation aircraft.
The speed of the aircraft is assumed to be $50\, m /s$, % = 180\, km / h
and the gravity constants is $g = 9.80555\, m / s^2$.

\small
\begin{alltt}
eq planeSize     = 4.0 .    eq weight        = 1000.0 .     eq wingSize  = 2.0 .
eq virtLiftConst = 0.6 .    eq horzLiftConst = 0.4 .        eq dragRatio = 0.05 .       
\end{alltt}
\normalsize

\paragraph{Subcontroller.}

The subcontrollers for the ailerons and the rudder are
modeled as object instances of the following class
\texttt{SubController}:

\small
\begin{alltt}
class SubController | curr-angle : Float,   goal-angle : Float,   diff-angle : Float .
subclass SubController < Component .  
\end{alltt}
\normalsize  %|


A subcontroller increases/decreases the @curr-angle@ toward the @goal-angle@
in each round, but the difference in a single (fast) round should be
less than or equal to 
the maximal angle @diff-angle@.
%
The transition function @delta@ of a subcontroller is then defined by
the following equation: 

\footnotesize
\begin{alltt}
ceq delta(< C : SubController | ports : < input : InPort | content : D LI >
                                        < output : OutPort | content : LO >,
                                curr-angle : CA, goal-angle : GA, diff-angle : DA >)
  = 
      < C : SubController | ports : < input : InPort | content : LI > 
                                    < output : OutPort | content : LO d(CA') >,
                            curr-angle : CA', goal-angle : GA' > 
  if CA' := angle(moveAngle(CA,GA,DA))
  /\char92 GA' := angle(if D == bot then GA else float(D) fi) .
\end{alltt}
\normalsize


\noindent where  \texttt{moveAngle(CA,\(\,\)GA,\(\,\)DA)}
equals  the angle that is increased or decreased from 
the current angle \texttt{CA} to the goal angle \texttt{GA} 
up to the maximum angle difference \texttt{DA}:

\small
\begin{alltt}
eq moveAngle(CA,\,GA,\,DA) = if abs(GA\;-\;CA) > DA then CA + DA * sign(GA\;-\;CA) else GA fi .
\end{alltt}
\normalsize
   
\noindent
The \texttt{angle} function keeps the angle value between $-180^\circ$
and $180^\circ$.  
The \texttt{delta} function updates the goal angle
to the input from the main controller, 
and  keeps the previous goal if it receives @bot@ (i.e., $\bot$).

\paragraph{Main Controller.}

The main controller for the aircraft turning control system
 is modeled as an  object instance of the following class
 \texttt{MainController}:

\small
\begin{alltt}
class MainController | velocity : Float,   goal-dir : Float,
                       curr-yaw : Float,   curr-rol : Float,    curr-dir : Float .
subclass MainController < Component .
\end{alltt}
\normalsize  %|

\noindent
The \texttt{velocity} attribute denotes  the speed of the aircraft. 
%The \texttt{period} attribute indicates the period of each round
%of the main controller.
The \texttt{curr-yaw}, \texttt{curr-roll}, and \texttt{curr-dir}
attributes model the position sensors of the aircraft by indicating 
 the current  yaw angle $\beta$, 
roll angle $\phi$, and  direction $\Psi$, 
respectively.
The \texttt{goal-dir} attribute denotes
 the goal direction given by the pilot.

For each round of the main controller,
the  attributes \texttt{curr-yaw}, \texttt{curr-roll}, and \texttt{curr-dir}
%for the current position status
are updated%
\footnote{
We currently use the simple Euler's method to compute such position values
given by the differential aeronautical equations  (\ref{eqn:dir}-\ref{eqn:yaw}),
but more precise methods can be easily applied.}
 using the angles of the wings in the input ports
that are transferred from the subcontrollers.
The \texttt{goal-dir} is also updated if a new goal direction arrives
to the input ports. 
Based on the new current position status and the goal direction,
the new angles of the wings are evaluated and sent back to the subcontrollers. 
%
The transition function @delta@ of the main controller is then defined as follows:


\footnotesize
\begin{alltt}
ceq delta(< C\! : \!MainController | 
              ports : < input\! : \!InPort | content\! : \!IN PI >    < output\! : \!OutPort | content\! : \!PO >
                      < inLW\! : \!InPort | content\! : \!d(LA) LI >   < outLW\! : \!OutPort | content\! : \!LO >
                      < inRW\! : \!InPort | content\! : \!d(RA) RI >   < outRW\! : \!OutPort | content\! : \!RO >
                      < inTW\! : \!InPort | content\! : \!d(TA) TI >   < outTW\! : \!OutPort | content\! : \!TO >,
              velocity\! : \!VEL, period\! : \!T, 
              curr-yaw\! : \!CY, curr-rol\! : \!CR, 
              curr-dir\! : \!CD, goal-dir\! : \!GD >)
  =    
      < C\! : \!MainController | 
          ports : < input\! : \!InPort | content\! : \!PI >  < output\! : \!OutPort | content\! : \!PO OUT >
                  < inLW\! : \!InPort | content\! : \!LI >   < outLW\! : \!OutPort | content\! : \!LO d(-\;RA') >
                  < inRW\! : \!InPort | content\! : \!RI >   < outRW\! : \!OutPort | content\! : \!RO d(RA') >
                  < inTW\! : \!InPort | content\! : \!TI >   < outTW\! : \!OutPort | content\! : \!TO d(TA') >,
          curr-yaw\! : \!CY', curr-rol\! : \!CR', 
          curr-dir\! : \!CD', goal-dir\! : \!GD' > 
   if CY' := angle( CY + dBeta(LA,RA,TA) * float(T) )
   /\char92 CR' := angle( CR + dPhi(LA,RA) * float(T) )
   /\char92 CD' := angle( CD + dPsi(CR,VEL) * float(T) )
   /\char92 GD' := angle( if IN == bot then GD else GD + float(IN) fi )
   /\char92 RA' := angle( horizWingAngle(CR', goalRollAngle(CR', CD', GD')) )
   /\char92 TA' := angle( tailWingAngle(CY') )
   /\char92 OUT := dir: CD' roll: CR' yaw: CY' goal: GD' .
\end{alltt}
\normalsize

\noindent
The first four lines in the condition compute new values for
\texttt{curr-yaw}, \texttt{curr-roll}, \texttt{curr-dir}, 
and \texttt{goal-dir}, based on values in the input ports. 
A non-$\bot$ value in the port @input@ is added to @goal-dir@.
The variables @RA'@ and @TA'@ denote new angles of the ailerons and the rudder,
computed by the control functions explained below.
Such new angles are queued in the corresponding output ports,
and will be transferred to the related subcontrollers at the next synchronous step
since they are feedback outputs.
The last line in the condition gives the output for the current step,
the new position information of the aircraft,
which will be transferred to its container ensemble at the end of the current synchronous step.


%\paragraph{Control Functions.}
%In our definition of the main controller,
The new angles of the ailerons and the rudder are computed by the
\emph{control functions}. 
The function \texttt{horizWingAngle}  computes the new angle for the
aileron in the right wing, 
based on the current roll angle and the goal roll angle. %given by the
                                %function \texttt{goalRollAngle}. 
The angle of the aileron in the left wing is always exactly opposite
to the one of the right wing. 
%
The function \texttt{goalRollAngle}  computes the desired roll angle $\phi$
to make a turn,
based on the current roll angle and the difference between the goal
direction and the current direction. 
Finally, in order to achieve a coordinated turn where the yaw angle is always $0$,
the function \texttt{tailWingAngle} computes
the new rudder angle based on the current yaw angle. 
%
We define all three control functions by simple linear equations as follows,
where @CR@ is a current roll angle and @CY@ is a current yaw angle:

\small
\begin{alltt}
eq goalRollAngle(CR,CD,GD) = sign(angle(GD\,-\,CD)) * min(abs(angle(GD\,-\,CD))\;*\;0.3, 20.0) .
eq horizWingAngle(CR,GR)   = sign(angle(GR\,-\,CR)) * min(abs(angle(GR\,-\,CR))\;*\;0.3, 45.0) .
eq tailWingAngle(CY)       = sign(angle(- CY)) * min(abs(angle(- CY))\;*\;0.8, 30.0) .
\end{alltt}
\normalsize

\noindent
That is, the goal roll angle is proportional to the difference
$\texttt{GD}\,-\,\texttt{CD}$ between the goal and current directions 
with the maximum $20^\circ$.
The horizontal wing (aileron) angles are also proportional to the
difference $\texttt{GR}\,-\,\texttt{CR}$ between the goal and current
roll angles 
with the maximum $45^\circ$.
Similarly, the rudder angle is proportional to the difference
$-\,\texttt{CY}$ between the goal and current yaw angles 
with the maximum $30^\circ$, where the goal yaw angle is always $0^\circ$.
  

\paragraph{Pilot Console.}

The pilot console,
the environment for the aircraft turning control system,
 is modeled as an  object instance of the following class \texttt{PilotConsole}:

\small
\begin{alltt}
class PilotConsole | scenario : List\char123Data\char125 .
subclass PilotConsole < Component .
\end{alltt}
\normalsize    %|


The attribute @scenario@ contains a list of goal angles that are
transmitted to the main controller. 
The transition function @delta@ of the pilot console
keeps sending goal angles in the @scenario@ to its output port
until no more data remains in the @scenario@.
In addition, the pilot console has an extra input port to receive
an outer environment input to generate \emph{nondeterministic} goal directions.
A non-$\bot$ value in the input port is added to
the output given by the @scenario@. 

\footnotesize
\begin{alltt}
ceq delta(< C : PilotConsole | ports : < input : InPort | content : IN LI >
                                       < output : OutPort | content : LO >,
                               scenario : d(F) SL >)
  = 
    < C : PilotConsole | ports : < input : InPort | content : LI >
                                 < output : OutPort | content : LO OUT >,
                         scenario : SL > .
 if OUT := d(if IN == bot then F else angle(F + float(IN)) fi) .

eq delta(< C : PilotConsole | ports : < input : InPort | content : IN LI >
                                      < output : OutPort | content : LO >,
                              scenario : nil >)
 = 
   < C : PilotConsole | ports : < input : InPort | content : LI >
                                < output : OutPort | content : LO bot >  > .
\end{alltt}
\normalsize %|

\noindent
If the @scenario@ is empty, i.e., @nil@, the outer
environment input is ignored, 
and the pilot console will keep sending $\bot$ to its output port.


\paragraph{Airplane System.}

The entire architecture of the airplane turning control system in
Figure~\ref{fig:airplane}, 
 including the environment (i.e., the pilot console),
is then represented as an ensemble 
object with subcomponents as follows (some parts of the specification
are replaced by `\texttt{\ldots}'): 

\footnotesize
\begin{alltt}
< airplane : \emph{Ensemble} | 
   rate : 1, 
   period : 600,
   ports :  < input : InPort | content : nil >  < output : OutPort | content : nil >,
   connections :
      input --> pilot\,.\,input ;  pilot\,.\,output --> csystem\,.\,input ;  csystem\,.\,output --> output,
   machines :
      \(\big(\)< pilot : \emph{PilotConsole} | rate : 1, period : 600,\ldots >
       < csystem : \emph{Ensemble} |
          rate :10, period : 60,
          ports : < input : InPort | content : nil >  
                  < output : OutPort | content : nil >,
          connections :
             input --> main\,.\,input ;  main\,.\,output --> output ; 
             left\,.\,output --> main\,.\,inLW ;  main\,.\,outLW --> left\,.\,input ; \ldots,
          machines : (< main : \emph{MainController} | rate : 1, period : 60,\ldots >
                      < left : \emph{SubController} | rate : 4, period : 15,\ldots >
                      < right : \emph{SubController} | rate : 4, period : 15,\ldots >
                      < rudder : \emph{SubController} | rate : 3, period : 20,\ldots >) >\(\big)\) >
\end{alltt}
\normalsize   %|

\noindent
The top-level  ensemble @airplane@ includes 
the pilot console @pilot@ and the ensemble @csystem@ for the
airplane turning control system.
The @airplane@ has one input port for the pilot console to generate
nondeterministic goals, 
and one output port to display the result.

In the ensemble @csystem@,
the input adaptors for the subcontrollers generate a~vector with extra $\bot$'s
and the adaptor for the main controller selects the last value of the input vector.

\small
\begin{alltt}
eq adaptor(left, input, D) = D bots(3) .   eq adaptor(rudder, input, D) = D bots(2) .
eq adaptor(right, input, D) = D bots(3) .  eq adaptor(main, P, LI D) = D .
\end{alltt}
\normalsize

\noindent
The function @bots(n)@ generates $n$ $\bot$-constants.
Similarly, in the top ensemble @airplane@, 
the input adaptor for the ensemble @csystem@
generates a vector with extra $\bot$'s,
and the adaptor for the pilot console selects the last value of the input vector.

\small
\begin{alltt}
eq adaptor(csystem, input, D) = D bots(9) 
eq adaptor(pilot, input, LI D) = D .
\end{alltt}
\normalsize

