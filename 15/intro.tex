
\section{Introduction}

Distributed cyber-physical systems (DCPS) are pervasive in areas such as aeronautics, ground transportation systems, medical systems, and so on; they include, in particular, the case of distributed hybrid systems, whose continuous dynamics is governed by differential equations.  DCPS design and verification is quite challenging, because to the usual complexity of a non-distributed CPS one has to add the additional complexities of asynchronous communication, network delays, and clock skews, which can easily lead a DCPS into inconsistent states.  In particular, any hopes of applying model checking
verification techniques in a direct manner to a DCPS look rather dim, due to the typically huge state space explosion
caused by the system's concurrency.

For these reasons, we and other colleagues at UIUC and Rockwell-Collins Corporation have been developing the 
\emph{physically asynchronous but logically synchronous}
PALS methodology \cite{icfem10,dasc09}, which can drastically reduce the system complexity of a DCPS so as to make it
amenable to model checking verification (for a comparison of PALS with related methodologies see \cite{steiner2011tta}). The PALS methodology applies to the frequently occurring case of
a DCPS whose implementation must be asynchronous due to physical constraints and for fault-tolerance reasons,
but whose \emph{logical} design requires that the system components should act together  in a virtually synchronous way.
For example, distributed control systems are typically of this nature.
The key idea of PALS is to reduce the design
and verification of a DCPS of this kind to the much simpler task%
\footnote{
For a simple avionics case study %considered 
in~\cite{pals-tcs}, the
number of system states  for their 
simplest possible distributed version with perfect clocks and no
network delays was 3,047,832,  but the PALS
pattern reduced the number of states %to be analyzed by model checking
to a mere 185.}
 of designing and verifying the
idealized synchronous system that should be realized in a distributed and asynchronous way.
  This is achieved
by a \emph{model transformation}  
$\mathcal{E} \;\mapsto\; \mathcal{A}(\mathcal{E},\Gamma)$
 that maps a
synchronous design  $\mathcal{E}$ to a distributed implementation  $\mathcal{A}(\mathcal{E},\Gamma)$ which is
\emph{correct-by construction} and that, as shown in \cite{icfem10,pals-tcs}, is \emph{bisimilar} to the
synchronous system $\mathcal{E}$.   This bisimilarity is the essential feature allowing the desired, drastic reduction
in system complexity and making model checking verification feasible: since bisimilar systems satisfy the exact same temporal
logic properties, we can verify that the asynchronous system $\mathcal{A}(\mathcal{E},\Gamma)$ satisfies a temporal logic
property $\varphi$ (which typically would be impossible to model check directly on  $\mathcal{A}(\mathcal{E},\Gamma)$)
by verifying the same property $\varphi$ on the vastly simpler synchronous system~$\mathcal{E}$.



The original PALS methodology presented in \cite{icfem10,dasc09}  assumes 
%of course 
a \emph{single logical period}, during  which
all components of the DCPS must communicate with each other and transition to their next states.   However,
a DCPS such as a distributed control system may have components that, for physical reasons, must operate with
different periods, even though those periods may all divide an overall longer period.  That is, many such
systems, although still having to be virtually synchronous for their correct behavior, are in fact \emph{multirate}
systems, with some components running at a faster rate than others.  An interesting challenge is how to extend 
%the
PALS %methodology
 to multirate DCPS.  This challenge has been given two different answers.  On the one hand,
an engineering solution for  Multirate PALS based on the AADL modeling  language has been proposed by
Al-Nayeem et al.\ in \cite{abdullah-multirate}.  On the other hand, three of us have
defined in \cite{bae-facs12}
a mathematical model of a multirate synchronous system $\mathcal{E}$, and have formally defined a
model transformation 
\[\mathcal{E} \;\mapsto\; \MA(\mathcal{E},T,\Gamma)\]
that generalizes to multirate systems the original single-rate PALS transformation defined in \cite{icfem10,pals-tcs}.
As before, we have proved in \cite{bae-facs12}
that  $ \MA(\mathcal{E},T,\Gamma)$ is a correct-by-construction implementation of  $\mathcal{E}$,
and that $\mathcal{E}$ and $\MA(\mathcal{E},T,\Gamma)$ are \emph{bisimilar}, making it possible
to verify temporal logic properties about $\MA(\mathcal{E},T,\Gamma)$ on the much simpler system
 $\mathcal{E}$.


 But how \emph{effective} is Multirate PALS in practice?  Can it be applied to formally verify important properties of a nontrivial multirate CPS such as a distributed hybrid system?  The main goal of this paper is to show that the answer is an emphatic \emph{yes}. We use Real-Time Maude \cite{journ-rtm} to formally specify in detail a multirate distributed hybrid system consisting of an airplane maneuvered by a pilot, who turns the airplane according to a specified angle $\alpha$ through a distributed control system with effectors located in the airplane's wings and rudder.  Our formal analysis revealed that the original design had control laws that were ineffective in achieving a smooth turning maneuver.  This led to a redesign of the system with new control laws which, as verified in Real-Time Maude by model checking, satisfies the desired correctness properties.  This shows that the Multirate PALS methodology is not only effective for formal DCPS verification, but, when used together with a tool like Real-Time Maude, can also be used effectively in the DCPS \emph{design} process, even before properties are verified.  To the best of our knowledge, this is the first time that the Multirate PALS methodology  has been applied to the model checking verification of a  DCPS.  In this sense, this paper complements our companion paper \cite{bae-facs12}, where the mathematical foundations of Multirate PALS were developed in detail, but where only a brief summary of some of the results presented here was given.
 
 This paper is organized as follows.
Section~\ref{sec:prelim}  explains  Multirate PALS and Real-Time Maude.
Section~\ref{sec:airplane-control} describes a simple model of an airplane turning control system 
whose continuous dynamics is governed by differential equations.
Section~\ref{sec:framework} presents a modeling framework for multirate ensembles in Real-Time Maude,
and Section~\ref{sec:airplane-model} then formally specifies the airplane turning control system using 
the ensemble  framework.
Section~\ref{sec:analysis} illustrates Real-Time Maude-based verification of the airplane turning control system.
%and shows how it is effective for the DCPS design process.
Finally, Section~\ref{sec:concl} gives some concluding remarks.















