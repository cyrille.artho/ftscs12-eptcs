
\section{The Airplane Turning Control System}
\label{sec:airplane-control}

This section presents a simple model of an avionics control system to turn an aircraft.
In general,
the direction of an aircraft is maneuvered by 
the ailerons and the rudder.
As shown in Figure~\ref{fig:airplane-str}, an aileron is a flap attached to the end of the left or the right wing,
and a rudder is a flap attached to the vertical tail 
(the aircraft figures in this section are borrowed from~\cite{collinson1996introduction}).

\begin{figure}[ht]
\centering
\includegraphics[width=0.5\textwidth] {airplane-str.pdf}    
\caption{The ailerons and the rudder of an aircraft.}  \label{fig:airplane-str}
\end{figure}


When an aircraft makes a turn, the aircraft rolls towards the desired direction of the turn,
so that the lift force caused by the two wings acts as the centripetal force and the aircraft moves in a circular motion.
The turning rate $d\psi$  can be given as a function of the aircraft's roll angle $\phi$:
%
\begin{equation}\label{eqn:dir}
	d\psi = (g / v) \,*\, \tan \phi
\end{equation}
%
where $\psi$ is the direction of the aircraft,  $g$ is the gravity constant, and $v$ is the velocity of the aircraft \cite{collinson1996introduction}.
The ailerons
are used to control the rolling angle $\phi$ of the aircraft by
generating different amounts of lift force in the left and the right
wings. Figure~\ref{fig:turn} describes such a banked turn using the
ailerons. 


\begin{figure}[ht]
\centering
\includegraphics[trim=0.3cm 0.4cm 0.3cm 0.4cm, clip=true, width=\textwidth]{turn.pdf}    
\caption{Forces acting in a turn of an aircraft with $\phi$ a roll angle and $\rho$ a roll rate.}  \label{fig:turn}
\end{figure}


\begin{wrapfigure}{r}{0.34\textwidth}
\centering
\includegraphics[trim=0.2cm 0.1cm 0.2cm 0.2cm, clip=true,width=0.30\textwidth]{yaw.pdf}    
%\includegraphics[width=0.34\textwidth] {yaw.pdf}    
\caption{Adverse yaw. \label{fig:yaw}}
\end{wrapfigure}


However, the rolling of the aircraft causes a difference in drag on
the left and the right wings, which produces a yawing moment in the
opposite direction to the roll, 
called adverse yaw.
This adverse yaw makes the aircraft sideslip in a wrong direction %during a turn
with the amount of the yaw angle $\beta$, as described in Figure~\ref{fig:yaw}.
This undesirable side effect is usually countered by using the aircraft's rudder,
which generates the side lift force on the vertical tail that opposes the adverse yaw.
To turn an aircraft safely and effectively, the roll angle $\phi$ of the aircraft should be increased for the desired direction
while the yaw angle $\beta$ stays at $0$.



Such a roll and yaw can be modeled by simple mathematical equations
under some simplifying assumptions, including:
\begin{inparaenum}[(i)]
	\item the aircraft's wings are flat with respect to the horizontal axis of the aircraft,
	\item the altitude of the aircraft does not change, which can
          be separately controlled by using the aircraft's elevator (a~flap attached to the horizontal tail of the aircraft),
	\item the aircraft maintains a constant speed by separately controlling the thrust power of the aircraft,
	 and
	\item there are no external influences such as  wind or  turbulence.
\end{inparaenum}
Then, the roll angle $\phi$ and the yaw angle $\beta$ can be modeled by the following equations~\cite{anderson2005introduction}:

\begin{align}
	d\phi^2 &= (\mathit{Lift\ Right} \,-\, \mathit{Lift\ Left}) \,/\, (\mathit{Weight} \,*\, \mathit{Length\ of\ Wing}) \label{eqn:roll}\\
	%
	d\beta^2 &= \mathit{Drag\ Ratio} \,*\, (\mathit{Lift\ Right} \,-\, \mathit{Lift\ Left}) \,/\, (\mathit{Weight} \,*\, \mathit{Length\ of\ Wing}) \nonumber\\
	&\ + \mathit{Lift\ Vertical} \,/\, (\mathit{Weight} \,*\, \mathit{Length\ of\ Aircraft})  \label{eqn:yaw}
\end{align}
%
The lift force from the left, the right, or the vertical wing is given by the following linear equation:
%
\begin{equation}
	\mathit{Lift} = \mathit{Lift\ constant} \;*\; \mathit{Angle} %\ with\ respect\ to\ free\ stream
\end{equation}
%
where, for $\mathit{Lift\ Right}$ and $\mathit{Lift\ Left}$, $\mathit{Angle}$ is the angle of the aileron,
and for $\mathit{Lift\ Vertical}$,  $\mathit{Angle}$ is the angle of the rudder.
The lift constant depends on the geometry of the corresponding wing,
and the drag ratio is given by the size and the shape of the entire aircraft.
%
%The drag ratio of an object has multiple parts, but with given assumptions,
%we only need to consider an \emph{induced drag}, which is related to 
%the amount of lift produced. In an aircraft turn, the wings produce different 
%amounts of lift and this produces different amounts of drag for each wing.
%The lack of balance creates a torque on the aircraft known as adverse yaw.
%The ratio of drag to lift is given by the equation:
%\[
%	\mathit{Induced\ Drag\ Constant} = (\mathit{Lift\ constant})^2 / (\pi e \mathit{AR})
%\]
%where $e$ is the wing efficiency and $\mathit{AR}$ is the wing aspect ratio.
%Both  $e$ and $\mathit{AR}$ are also determined by the size and the shape 
%of the aircraft. 
%


We model the airplane turning control system as a multirate ensemble 
with $4$ typed machines: the main controller, the left wing controller, 
the right wing controller, and the rudder controller.
The environment for the airplane turning control system is given by the pilot console,
which is  modeled as another typed machine
and is connected to the main controller on the outside of the control system.
 Each sub-controller moves the surface of the wing towards the goal angle specified 
 by the main controller, which sends the desired angles to the sub-controllers to make 
 a coordinated turn whose goal direction is specified  from the pilot console.
 The main controller also models position sensors that measure the roll, the yaw, and the direction
 by using the aeronautics equations above. 
 %
  In this case study,
 we assume that the main controller has period $60\,\mathrm{ms}$, 
the left and the right wing controllers have period $15\,\mathrm{ms}$ (rate~$4$),
and the rudder controller has period $20\,\mathrm{ms}$ (rate~$3$).
Our model  is a two-level hierarchical multirate ensemble,
since the airplane turning control system itself forms a single typed machine with period $60\,\mathrm{ms}$ (rate~$10$)
 and then is connected to the pilot console with period $600\,\mathrm{ms}$,
 as illustrated in Figure~\ref{fig:airplane}.

 
\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.9\textwidth]{airplane.pdf}
\caption{The architecture of our airplane turning control system.\label{fig:airplane}}
\end{center}
\end{figure}


Using the framework introduced in Section 4 for specifying and
executing multirate synchronous ensembles in Real-Time Maude, 
we  specify in Section 5 (and redefine in Section 6)  the multirate ensemble $\mathfrak{E}$ corresponding to 
the above airplane control system.
In Section 6 we then  exploit the bisimulation $\mathfrak{E} \,\simeq\, \MA(\mathfrak{E},T,\Gamma)$
(see \cite{bae-facs12}) to verify properties
about the asynchronous realization $\MA(\mathfrak{E},T,\Gamma)$ by model checking them on the much simpler system $\mathfrak{E}$.



