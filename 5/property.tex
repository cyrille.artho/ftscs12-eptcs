
\section{The Property Specification Language}\label{sec:specification}


We adopt the Interval Temporal Logic (ITL)~\cite{ITLMaszkowskiM83} as the property specification language. The reason why we adopt the interval-based logic instead of state-based logics like LTL or CTL is that most of the properties the domain engineers care about are related to some duration of time. For instance, the engineers would like to check if the system specified by {\modediagram} can stay in a specific state for a continuous period of time  instead of just reaching this state. Another typical scenario illustrated in Fig.~\ref{fig:propertyExample} is that, ``\textit{when the system control is in mode $m_4$, if a failure occurs, it should switch to mode $m_8$ in 100 ms}''. The standard LTL formula $\Box(failure \wedge m_4 \Rightarrow \Diamond m_8)$ can be used to specify that ``\textit{when the system is in mode $m_4$, and a failure occurs, it should switch to mode $m_8$}''. But the real-time feature ``\textit{in 100 ms}'' is lost. Though the extensions of LTL or CTL may also describe the interval properties to some extent, it is more natural for the domain engineers to use interval-based logic since the intuitive chop operator ($^\frown$) is available in ITL.

\begin{figure}[t]
  \centering
  \includegraphics[scale=0.6]{pic/specexample.eps}
  \caption{A Property about Failure}
  \label{fig:propertyExample}
\end{figure}

An interval logic formula can be interpreted over a time interval \cite{IntervalCalculus} or over a ``state interval'' (a sequence of states)~\cite{ITLMaszkowskiM83} . \hide{For example,  that a formula $\phi ^\frown \psi$ holds in a time interval $[b, e]$ signifies that there exists $m$ ($b \leq m \leq e$) such that $\phi$ holds in interval $[b, m]$ and $\psi$ holds in $[m, e]$.  } As explained  later in this section, our proposed specification language will be interpreted in the latter way \cite{ITLMaszkowskiM83} except for a small modification on the interpretation of  the chop operator ($^\frown$).


\subsection{Syntax}

\begin{figure}[t]
  \centering
    $
        \begin{array}{crl}
              \textit{Terms}  &   \theta     \ \triangleq  & r \mid v \mid l \mid f(\theta_1, \ldots, \theta_n) \\
              \textit{Formulas} &   \phi, \psi \ \triangleq & \ \tvalue \mid \fvalue \mid p(\theta_1, \ldots, \theta_n)  \mid \neg \phi \mid \phi \wedge \psi \mid \phi ^\frown \psi \\
        \end{array}
    $
  \caption{The Syntax of ITL}
  \label{fig:syntaxITL}
\end{figure}


The syntax of the specification language is defined in Fig~\ref{fig:syntaxITL}, where
\begin{itemize}
    \item The set of terms $\theta$ contains real-value constants $r$, temporal variables $v$\hide{(whose values may depend on some time intervals)}, a special variable $l$, and  functions  $f(\theta_1, \ldots, \theta_n)$ (with $f$ being an $n$-arity function symbol   and $\theta_1,\ldots,\theta_n$ being terms). \hide{(1) Constant symbol $r$ denotes a real value. (2) Temporal symbol $v$ is used to represent the variable whose value depends on the given interval. (3) Function symbol $f(\theta_1, \ldots, \theta_n)$ means a n-arity real value function.}
    \item Formulae can be boolean constants  ($\tvalue$, $\fvalue$), predicates ($p(\theta_1, \ldots, \theta_n)$ with $p$, an $n$-arity predicate symbol), classical logic formulae (constructed using $\neg$, $\wedge$, etc), or interval logic formulae (constructed using $\CHOP$). \hide{(1) Constant symbols $\tvalue$, $\fvalue$ denote the boolean constants $\True$ and $\False$, respectively. (2) Predicate symbol $p(\theta_1, \ldots, \theta_n)$ means a n-arity predicate. (3) Boolean connections: negation($\neg$) and conjunction ($\wedge$) (4) Modal connection $\CHOP$ is a binary modality which \emph{chop}s an interval into two consecutive sub-intervals.} If the formula $\phi \CHOP \psi$ holds for an interval $\ell$, it means that the interval $\ell$ can be ``chopped'' into two sub-intervals, where $\phi$ holds for the first sub-interval and $\psi$  holds for  the second one.
\end{itemize}

As a kind of temporal logic, ITL also provides the $\Box$ and $\Diamond$ operators. They are defined as the abbreviations of $\CHOP$.

\[
\Diamond \phi \triangleq \tvalue \CHOP (\phi \CHOP \tvalue), \text{ for some sub-interval }, \ \
\Box \phi     \triangleq \neg \Diamond (\neg \phi), \text{ for all sub-intervals }
\]

By the specification language proposed here, we can describe the properties the domain engineers may desire. For instance, the following property describes the scenario shown in Fig.~\ref{fig:propertyExample}.

\[
    \Box(
    m_4 \wedge (\neg \failure \CHOP \failure) \CHOP \tvalue \Rightarrow
    m_4 \wedge (\neg \failure \CHOP (\failure \wedge l \le 100)) \CHOP m_8 \CHOP \tvalue
    )
\]


%% 如果系统因为故障进入模式1，那么当系统从模式1转到模式2时，应当重置t_gi, t_hon, t_gon
%\textit{When system enters mode 1 because of failure, if it switches to mode 2 from mode 1, then it should reset $t_{gi}$, $t_{hon}$ and $t_{gon}$ in mode 2.}\\[2ex]
%We can capture the scenario precisely by the specification language in the following.
%\vspace*{-2mm}
%\[
%\begin{small}
%    \Box(failure \ne 0^{\frown} mode = 1^{\frown} mode = 2 \Rightarrow \tvalue^{\frown} (t_{gi} = 0 \wedge t_{hon} = 0 \wedge t_{gon} = 0))
%\end{small}
%\]


\begin{table}[t]
    \centering
    \begin{tabular}{l}
        $
        \begin{small}
            \begin{array}{l}
                \mathcal{I_T}(r, \Sigma) = r \\
                \mathcal{I_T}(l, \Sigma) = \begin{cases}
                                           \begin{array}{ll}
                                           \sigma_{n-1}(ts) - \sigma_{0}(ts) & \text{ if } \Sigma  = \sigma_0.\ldots.\sigma_{n{-}1} \\
                                           \infty                            & \text{ if } \mid \Sigma\mid = \infty \hide{\text{ is infinite }}
                                           \end{array}
                                           \end{cases} \\
                \mathcal{I_T}(v, \sigma_0 . \Sigma) = \sigma_0(v) \\
                \mathcal{I_T}(f(\theta_1, \ldots, \theta_n), \Sigma)  =  f\left(\mathcal{I_T}(\theta_1, \Sigma), \ldots, \mathcal{I_T}(\theta_n, \Sigma)\right) \\
            \end{array}
        \end{small}
        $ \\
        \\
        $
        \begin{small}
            \begin{array}{ccl}
                \mathcal{I_F}(p(\theta_1, \ldots, \theta_n), \Sigma) = \True & \text{ iff } &  p(\mathcal{I_T}(\theta_1, \Sigma), \ldots, \mathcal{I_T}(\theta_n, \Sigma)) \\
                 \mathcal{I_F}(\tvalue, \Sigma) = \True & \text{ iff } & \textit{always} \\
                \mathcal{I_F}(\fvalue, \Sigma) = \False & \text{ iff } & \textit{always} \\
                \mathcal{I_F}(\neg \phi, \Sigma) = \True & \text{ iff } & \mathcal{I_F}(\phi, \Sigma) = \False \\
                \mathcal{I_F}(\phi \wedge \psi, \Sigma) = \True & \text{ iff } &
                \mathcal{I_F}(\phi, \Sigma) = \True \text{ and } \mathcal{I_F}(\psi, \Sigma) = \True\\
                \mathcal{I_F}(\phi ^\frown \psi, \Sigma) = \True & \text{ iff } &
                \exists k  < \infty \cdot \ \Sigma = (\sigma_0 \ldots \sigma_k \cdot \Sigma') \wedge \\
                & & \hspace*{4mm} \mathcal{I_F}(\phi, \sigma_0\ldots\sigma_k) = \True \wedge \mathcal{I_F}(\psi, \Sigma') = \True \\
            \end{array}
        \end{small}
        $ \\
    \end{tabular}
  \caption{Interpretation of the Specification Language}\label{tbl:semanticsITL}
\end{table}


\subsection{Interpretation}

Terms/formulae in our property specification language are interpreted in the same way as in Maszkowski \cite{ITLMaszkowskiM83}, where
an interval is represented by  a finite or infinite sequence of states ($\Sigma = \sigma_0 \sigma_1 \ldots \sigma_{n-1} \ldots$), where $\sigma_i \in \State$. The interpretation is given by two functions (1) term interpretation :$\mathcal{I_T} \in  \Terms \times \Intv \mapsto \mathbb{R}$, and (2) formula interpretation function: $\mathcal{I_F} \in \Formulas \times \Intv \mapsto \{\True, \False\}$. Table~\ref{tbl:semanticsITL} defines these two functions, where $ts$  denotes the variable $\timestamp$. \hide{and a special temporal variable $l$ is introduced to denote the interval length. }The value of the variable $\timestamp$ increases with the elapse of the time. i.e.,  for any two states in the same interval $\sigma_i, \sigma_j$, if $i {<} j$, then $\sigma_i(ts) {<} \sigma_j(ts)$. Thus, we can compute the length of  time interval based on the difference of the two time stamps located in the first and last states respectively. The interpretation of a variable $v$ on $\Sigma$ is the evaluation of $v$ on the first state of $\Sigma$. Note that our chop operator requires that the first sub-interval of $\Sigma$ is restricted to be finite no matter whether the interval $\Sigma$ itself is finite or not.


