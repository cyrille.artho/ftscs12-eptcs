

\section{The {\modediagram} Notations}\label{sec:modechart}

Before developing the formal model of {\modediagram}, we will begin by giving its informal description. An  {\modediagram} model is composed by several modes, variables used in the mode, and mode transitions specifying the mode switch relations. A mode essentially refers to the state of the system which can be observed from outside. The mode body can be either a Control Flow Graph (CFG), which prescribes the computational tasks the system can perform in every period, or several other modes as sub-modes. If the mode has sub-modes, when the system is in this mode, it should be in one of its sub-modes. We say that the mode is a leaf mode if its mode body is a control flow graph. A leaf mode usually encapsulates the control algorithms involving complicated computations. The CFG in a leaf mode follows the standard notation, which contains assignment, conditional and loop. It also supports function units similarly to the ones in programming languages.

\subsection{The Syntax of {\modediagram}}


\begin{figure}[t]
    \centering
    \begin{tabular}{c}
    \begin{tabular}{cc}
    $
    \begin{small}
        \begin{array}{rcl}
          \kwSystem     & ::= & (\kwVar^{+}, \kwMode^{+}, \kwModule^{+})  \\
          \kwMode       & ::= & (\kwname, \kwperiod, \kwinitial, \\
                        &     & \hspace*{2mm} \kwBody, \kwTransition^{+})  \\
          \kwBody       & ::= & \kwMode^{+} \mid \kwCFG \\
          \kwTransition & ::= & (\kwsource,\kwguard, \kwpriority, \kwtarget) \\
          \kwModule     & ::= & (\kwname, V_I, V_O, CFG)
        \end{array}
    \end{small}
    $
    &
    $
    \begin{small}
        \begin{array}{rcl}
            \kwSExpr & ::= & \kwConst \mid \kwVar \mid f^{(n)}(\kwSExpr \ldots) \\
            \kwBTerm & ::= & \True \mid \False \mid p^{(n)}(\kwSExpr \ldots) \\
            \kwIExpr & ::= & (\after \mid \duration) ( \kwBTerm, \kwSExpr ) \\
            \kwGTerm & ::= & \kwIExpr \mid \kwBTerm \\
            \kwBExpr & ::= & \hspace*{1mm} \kwBTerm \mid \neg \kwBExpr \\
                     &     & \mid \kwBExpr \vee \kwBExpr \mid \kwBExpr \wedge \kwBExpr \\
            \kwGuard & ::= & \hspace*{1mm} \kwGTerm \mid \neg \kwGuard \\
                     &     & \mid \kwGuard \vee \kwGuard\mid \kwGuard \wedge \kwGuard \\
        \end{array}
    \end{small}
    $
    \\
    (a){\modediagram} & (b) Expressions and Guards
    \end{tabular} \\
    \\
    \begin{tabular}{c}
    $
            \begin{array}{rcl}
                 CFG & ::= &  \stmts \\
              \stmts & ::= &  \pStmt \mid \cStmt\\
              \pStmt & ::= &  \aStmt \mid \CALL ~ name \mid \SKIP ~ |\\
              \aStmt & ::= &  x := \kwSExpr \\
              \cStmt & ::= &  \stmts ;~\stmts \mid  \WHILE ~ \kwBExpr ~ \DO ~ \stmts \mid  \\
                     &     &  \IF ~ \kwBExpr ~  \THEN ~ \stmts ~ \ELSE ~ \stmts
            \end{array}
        $
    \\
    (c) CFG
    \end{tabular}
    \end{tabular}
    \caption{The Syntax of {\modediagram}}\label{fig:mcsyntax}
\end{figure}



We briefly list its syntactical elements in Fig.~\ref{fig:mcsyntax}(a). An {\modediagram} is composed of a list of modes ($\kwMode^+$) and modules ($\kwModule^{+}$), as well as a list of variables ($\kwVar^+$) used in those modes and modules.

Intuitively, a mode  refers to a certain state of the system which can be observed from outside. A mode has a name, a period, a body and a list of transitions. For simplicity, we assume all mode names are distinct in an MDM model. The mode period (an integer number) is used to trigger the periodic behavior of the  mode. The $\kwinitial$ denotes a mode is an initial mode or not. The mode body can be composed of either a control flow graph (CFG), prescribing the computational tasks the system can perform in the mode in every period, or a list of other modes as the immediate sub-modes of the current mode. If a mode has sub-modes, when the control lies in  this mode, the control should also be in one of the sub-modes. A leaf mode does not have sub-modes, so  its body contains a CFG. A mode is either a leaf mode, or it directly or indirectly has  leaf modes as its sub-modes. A mode is called top mode if it is not a sub-mode of any other mode. The CFG in a leaf mode is the standard control flow graph, which contains nodes and structures like assignment, module call, conditional and loop. It also supports  function units like the ones in conventional programming languages. The syntax of CFG is presented in Figure~\ref{fig:mcsyntax}(c).

A module encapsulates computational tasks as its CFG. $V_I$ specifies the set of variables used in the CFG, while $V_O$ is the set of variables modified in the CFG. A module can be invoked by some modes or other modules. As a specification for embedded systems, recursive module calls are forbidden.

A transition (from $\kwTransition^+$) specifying a mode switch from  one mode to another is represented as a quadruple, where the first element is the name of the source mode, the second specifies the transition condition,  the third  is the priority of the transition and the last element is the name of the target mode. The {\modediagram} supports mode switches at different levels in the mode-hierarchy. The transition condition (i.e. $\kwguard$) is defined in Fig.~\ref{fig:mcsyntax}(b). A state expression can be either a constant, a variable, or a real-value function on state expressions. A boolean term is either a boolean constant, or a predicate on state expressions. There are two kinds of interval expressions, $\mathsf{after}$ and $\mathsf{duration}$. These interval expressions are very convenient to model system behaviors related with past states. A guard term can be either an interval expression, or a boolean term. A guard is the boolean combination of guard terms. To ensure that the mode switches be deterministic, we require that the priority of a transition has to be different from the others in the same mode chain:
\[
    \forall m \in \kwMode \cdot \forall t_1, t_2 \in \outs{\supermodes{\MD}{m}} \cdot
    t_1 \ne t_2 \Rightarrow \priority{t_1} \ne \priority{t_2}
\]
The functions $\supermodes{\MD}{m}$ and $\outs{\mlist}$ will be defined later.

\subsubsection{Auxiliary Definitions}

Given an {\modediagram}
$
\begin{small}
\kwSystem ::= (\kwVar^{+}, \kwMode^{+}, \kwModule^{+})
\end{small}
$,
we introduce two auxiliary relations:\\
$Contains(\kwSystem) \subseteq {\kwMode}s \times {\kwMode}s$ for mode-subsume relation and\\
$Trans(\kwSystem) \subseteq {\kwMode}s \times \Int \times \kwGuard \times {\kwMode}s$ for mode-switch  relation.

Given a mode $m = (n, per, ini, b, tran)$ and a transition $t = (m, g, pri, m')$, we define these operations/predicates:
\[
    \begin{array}{llll}
        \period{m} = per   ~&~ \isInitial{m} = ini ~& ~\CFG{m} = b   ~&~                 \\
        \priority{t} = pri ~&~ \guard{t} = g       ~& ~\source{t}=m  ~&~ \target{t} = m'  \\
    \end{array}
\]
We also define the following auxiliary  functions:
\[
    \begin{array}{l}
        \supermodes{\MD}{m} \triangleq \langle m_1, m_2, \ldots, m_k \rangle, \text{ where }\\
        \hspace*{5mm} m_k = m \wedge m_1 \in {\TopModes}(\MD) \wedge \forall 1 {<} i {\le} k \cdot (m_{i-1}, m_i) \in \Contains(\MD) \\
        \hspace*{5mm} \text{and } m{\in}{\TopModes}(\MD) \triangleq  m {\in} {\kwMode}s(\MD) {\wedge} \neg\exists m'\hide{ {\in} {\kwMode}s}\cdot ( m', m ) { \in} \Contains(\MD) \\
        \\
       \upmodes{\MD}{m}{k} \triangleq \{ m_i \mid m_i \in \supermodes{\MD}{m} \wedge \mod(k, \frac{\period{\mathit{m_i}}}{\period{\mathit{m}}}) = 0 \} \\
       \\
       \submode{\MD}{m} \triangleq m', \text{ where } (m, m') \in \Contains(\MD) \wedge \isInitial{\mathit{m'}} \\
       \\
       \outs{\MD,\mlist} \triangleq \bigcup_{m \in \mlist}\{t \mid t \in \Trans(\MD) \wedge \source{\mathit{t}}=m\}
    \end{array}
\]
%
The function $\supermodes{\MD}{m}$  retrieves a sequence of modes from a top mode to $m$ using the $\Contains$ relation. The set ${\TopModes}(\MD)$ consists all the modes which are not sub-modes of any other mode. The function $\upmodes{\MD}{m}{k}$ returns those modes in $\supermodes{\MD}{m}$ whose periods are consistent with the period count $k$. An {\modediagram} requires that the period of a mode should be equal to or multiple to the period of its sub-modes. The function $\submode{\MD}{m}$ returns the initial sub-mode for a non-leaf node $m$, and the predicate $\isInitial{\mathit{m'}}$ means that the sub-mode $m'$ is the initial sub-mode in its hierarchy. The function $\outs{\mlist}$ returns all outgoing transitions from modes in $\mlist$.


\subsection{The Semantics}\label{subsec:semantics}

In order to precisely analyze the behaviors of {\modediagram}, for instance, model checking of {\modediagram} , we need its formal semantics. In this section, we present the operational semantics for {\modediagram}.

\begin{table}[t]
    \[
    \begin{small}
    \begin{array}{lcl}
      \sigmasq \models b                          & \Leftrightarrow & \sigma_n \models b \\
      \sigmasq \models \neg g                     & \Leftrightarrow & \neg(\sigmasq \models g)  \\
      \sigmasq \models g_1 \vee g_2               & \Leftrightarrow & \sigmasq \models g_1 \text{ or } \sigmasq \models g_2 \\
      \sigmasq \models g_1 \wedge g_2             & \Leftrightarrow & \sigmasq \models g_1 \text{ and } \sigmasq \models g_2 \\
      \sigmasq \models \mathsf{duration}(b, l)    & \Leftrightarrow & \sigma_n(l) = \nu \wedge  \exists i {<}n \cdot ( \sigma_i(ts) {+} \nu\leq \sigma_n(ts) \wedge \\
                                                                  &                   &
       \sigma_{i{+}1}(ts) {+} \nu \geq \sigma_n(ts) \wedge       \forall i {\leq} j {\leq} n \cdot \sigma_j(b) = \True)  \\
      \sigmasq \models \mathsf{after}(b, l)       & \Leftrightarrow & \sigma_n(l) = \nu \wedge       \exists i {<} n \cdot (   \sigma_i(ts) {+}\nu \leq \sigma_n(ts) \wedge\\
                                                                  &                   &
 \sigma_{i{+}1}(ts) {+} \nu \geq \sigma_n(ts)) \wedge      \sigma_i(b) = \True) % \\
    \end{array}
    \end{small}
    \]
    \caption{The Interpretation of Guards}\vspace*{-6mm}
    \label{tbl:evalie}
\end{table}

\subsubsection{Configuration}

The configuration in our operational semantics is represented as $( \MD, m, l, pc, k, \Sigma )$, where
\begin{itemize}
    \item $\MD$ is the \modediagram, and $m$ is the  mode  the system control currently lies in.
    \item $l \in \{\PBegin, \PExecute, \PEnd\}$ specifies the system is in the beginning, middle, or end of a period.
    \item $pc \in \mathcal{L}$, where $\mathcal{L} = \mathcal{N} \cup \{\CFGStart, \CFGExit, \bot\}$ is the program counter to execute the control flow graph. $\mathcal{N}$ is used to represent the nodes in control flow graphs and $\CFGStart$, $\CFGExit$ denote the start and exit locations of a control flow graph respectively. If the current mode is not equipped with any flow graph, we use the symbol $\bot$ as a placeholder.
    \item The fourth component $k$ records the count of periods for the current mode. If the system switches to another mode, it will be reset to $1$. The period count is used to distinguish whether a super-mode of the current mode is allowed to check its mode switch guard.
    \item $\Sigma$ is a list of states of the form $\Sigma' \cdot \sigma$, where $\sigma$ denotes the current state ($\sigma\in \State \triangleq \textit{Vars}{\rightarrow}\mathbb{R}$) and $\Sigma'$ represents a history of states.

\end{itemize}


\noindent{\bf Guards\quad}The evaluation of a transition guard may depend on the current state as well as some historical states. Table~\ref{tbl:evalie} shows how to interpret a guard in a given sequence of states. The symbol $ts$ is the abbreviation of the variable $\timestamp$. The guard $\mathsf{duration}(b,l)$ evaluates to $\True$ if the boolean expression $b$ has been  $\True$  during the time interval $l$ up to the current moment. The guard  $\mathsf{after}(b,l)$ evaluates to $\True$ if  the boolean expression $b$ was $\True$  the time interval $l$ ago. In this table, $b$ is a pure boolean expression
without interval expressions and $l$ is a state expression.


\subsubsection{Operational Rules}
\label{subsec:inferencerules}

\begin{table}[t]
    \centering
    \[
    \begin{small}
    \begin{array}{cc}
        \runa{enter} & \infrulesL{120}
                        {
                            \CFG{m} = \bot
                        }
                        {
                            ( \MD, m, \PBegin, \bot, k, \Sigma )
                            \overset{}{\longrightarrow}
                            ( \MD, m', \PBegin, pc', k, \Sigma )
                        }\\
                        & \hspace*{-8mm}\text{where }  m' = \submode{\MD}{m} \text{ and }
                                         pc' =
                                            \begin{cases}
                                                \bot ,     & \mbox{if } \CFG{m'} = \bot \\
                                                \CFGStart, & \mbox{if } \CFG{m'} \neq \bot
                                            \end{cases}  \\
        \runa{detect} &\hspace*{-8mm} \infrulesL{120}
                      {
                        \CFG{m} \ne \bot
                      }
                      {
                        ( \MD, m, \PBegin, pc, k, \Sigma \cdot \sigma )
                        \overset{}{\longrightarrow}
                        ( \MD, m, \PExecute, pc, k, \Sigma \cdot \sampling(\sigma) )
                      }\\
        \runa{execute} & \infrulesL{120}
                      {
                        \execute (\CFG{m}, pc, \sigma, \period{m}) = ( pc', \sigma' )
                      }
                      {
                        ( \MD, m, \PExecute, pc, k, \Sigma {\cdot}\sigma)
                        \overset{}{\longrightarrow}
                        ( \MD, m, \PEnd, pc', k, \Sigma' )
                      }\\
                      &
                        \begin{array}{cl}
                          \text{where } & \Sigma' = \Sigma \cdot \sigma'[ts \mapsto \sigma(ts) + \period{m}] \\
                        \end{array}   \\
        \runa{continue} & \infrulesL{120}
                      {
                        pc \neq \CFGExit
                      }
                      {
                        ( \MD, m, \PEnd, pc, k, \Sigma )
                        \overset{}{\longrightarrow}
                        ( \MD, m, \PExecute, pc, k, \Sigma )
                      }\\
        \runa{repeat} & \infrulesL{120}
                      {
                        \forall t \in \outs{\upmodes{\MD}{m}{k}} \cdot \Sigma \not\models \guard{t}
                      }
                      {
                        ( \MD, m, \PEnd, \CFGExit, k, \Sigma )
                        \overset{}{\longrightarrow}
                        ( \MD, m, \PBegin, \CFGStart, k{+}1, \Sigma )
                      }\\
        \runa{switch} &\hspace*{-8mm} \infrulesL{120}
                      {
                        \begin{array}{l}
                            \exists t \in \outs{\MD,\upmodes{\MD}{m}{k}} \cdot \Sigma \models \guard{t} \wedge \\
                          {\forall} t' {\in} \outs{\upmodes{\MD}{m}{k}} {-} \{t\} \cdot (\Sigma\not\models\guard{t'} \vee \priority{t'} {<} \priority{t})
                        \end{array}
                      }
                      {
                        ( \MD, m, \PEnd, \CFGExit, k, \Sigma )
                        \overset{}{\longrightarrow}
                        ( \MD, m', \PBegin, pc', 1, \Sigma )
                      }\\
                      & \text{where } m' = \target{t} \text{ and } pc' =
                                            \begin{cases}
                                                \bot,      & \mbox{if } \CFG{m'} = \bot \\
                                                \CFGStart, & \mbox{if } \CFG{m'} \neq \bot
                                            \end{cases} \\
                      &% \\
    \end{array}
    \end{small}
    \]
    \caption{Operational Semantic Rules for {\modediagram}}
    \label{tbl:inferenceRules}
\end{table}

The operational rules for {\modediagram} are given in Table~\ref{tbl:inferenceRules}. Here we adopt a big-step operational semantics for {\modediagram}, which means that we only observe the start and end points of a period in the current mode, while the state changes within a period are not recorded. This is reasonable since in practice  engineers usually monitor the states at the two ends of a period to decide if it works well. In the rules, we make use of an auxiliary function $\execute$ to represent the execution results for the mode in one period.
\[
\execute: \mathcal{CFG}(V) \times \mathcal{L} \times \State \times \mathbb{R}^+ \rightarrow \mathcal{L} \times \State
\]
It receives a flow graph, a program counter, an initial state and the time permitted to execute and returns the state and program counter after the given time is expired. Its detailed definition is left in the report \cite{modechart}. We now explain the operational rules:
\begin{itemize}
    \item[1.] \runa{enter}. When the system is at the beginning of a period, if the current mode $m$ has sub-modes, the system enters the initial sub-mode of $m$.

    \item[2.] \runa{detect}. When the system is at the beginning of a period, if the current mode $m$ is a leaf mode, the system updates its state by sampling from sensors. The function $\sampling$ represents the side-effect on variables during sensor detection. The period label $l$ is changed to be $\PExecute$, indicating that the system will then perform  computational tasks specified by the control flow graph of $m$.

    \item[3.] \runa{execute}. This rule describes the behaviors of executing CFG of the leaf mode $m$. \hide{The current state $\sigma$ is stored as the last element in the trace of states $\Sigma$. }The function $\execute$ is used to compute the new state $\sigma'$ from $\sigma$. The computation task may be finished in the current period and $pc' = \CFGExit$ holds or the task is not finished and the program counter points to some location in the control flow graph. The value of  the timestamp variable $ts$  in $\sigma'$ is equal to its value in state $\sigma$ plus the period of the mode $m$.

    \item[4.] \runa{continue}. This rule tells that when the computation task in leaf mode is not finished in a period, it will continue its task in the next period. In this case, the system is implicitly not allowed to switch to other modes from the current mode. When moving to  the next period, sensor detection is skipped.

    \item[5.] \runa{repeat}. This rule specifies the behavior of restarting the flow graph when the computation task is finished in a period. When it is at the end of a period and the system finishes executing the flow graph  ($pc = \CFGExit$), if there is no transition guard enabled, the system stays in the same mode and restarts the computation specified by the flow graph.

    \item[6.] \runa{switch}. This rule specifies the behavior of the mode transition. There exists a transition $t$, whose guard holds on the sequence of states $\Sigma$. And the priority of $t$ is higher than that of any other enabled transitions.

\end{itemize}
