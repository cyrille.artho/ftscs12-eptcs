
\section{{\modediagram} Verification by Statistical Model Checking}\label{sec:statistical}

As a modelling \& verification framework for periodic control systems, {\modediagram} supports the modeling of periodic behaviors, mode transition, and complex computations involving linear or non-linear mathematical formulae. Moreover, it also provides a property specification language to help the engineers capture requirements. In this section, we will show how to verify  that an {\modediagram} model satisfies  properties formalized in the specification language. There are two main obstacles to apply classic model checking techniques on {\modediagram}: (1) {\modediagram} models involve complex computations like non-linear mathematic formulae; (2) {\modediagram} models are open systems which need intensive interactions with the outside.

Our proposed approach relies on Statistical Model Checking(SMC)~\cite{SMCSenVA04,SMCYounes05,SMCTA2011, UppaalSMC}. SMC is a simulation-based technique that runs the system to generate traces, and then uses statistical theory to analyze the traces to obtain the verification estimation of the entire system. SMC usually deals with the following quantitative aspect of the system under verification~\cite{SMCYounes05}:
\begin{itemize}
    \item [] What is the probability that a random run of a system will satisfy the given property $\phi$?
\end{itemize}


Since the SMC technique verifies the target system with the probability estimation instead of the accurate analysis, it is very effective when being applied to open and non-linear systems. Because SMC depends on the generated traces of the system under verification, we shall  briefly describe how to simulate an {\modediagram} and then present an SMC algorithm for {\modediagram}.

\subsection{{\modediagram} Simulation}

 The {\modediagram} model captures a reactive system~\cite{ReactiveHarel}. The {\modediagram} model executes and interacts with its external environment in a \emph{control loop} in one period as follows: (1) Accept inputs via sensors from the environment. (2) Perform computational tasks. (3) Generate outputs to drive other components. The {\modediagram} simulation engine simulates the process of the control loop above.

Generally speaking, the simulation is implemented according to the inference rules defined in Table~\ref{tbl:inferenceRules}. However, the behaviors of an {\modediagram} model depends not only on the {\modediagram} itself, but also on the initial state and the external environment. When we simulate the {\modediagram} model, the initial values are randomly selected from a range specified by the control engineers from CAST. As a specification language, the type of variables defined in {\modediagram} can be real number. To implement the simulation, we use float variables instead, which may introduce some problems on precision. There are lots of techniques can be adopted to check if any loss of precision may cause problems\cite{floatHigham}. Because the simulation doesn't take care of the platform to deploy the system specified by the {\modediagram}, the time during simulation is not the real time, but the logic time. For each iteration in the \emph{control loop}, the time is increased by the length of period of the current mode.

To make the simulation be executable, we have to simulate the behaviors of the environment to make the {\modediagram} model to be closed with its environment. The environment simulator involving kinematic computations designed by the control engineers is combined with the {\modediagram} to simulate the physical environment the {\modediagram} model interacts with. In the beginning of each period, the simulator checks whether there are sub-modes in the current mode. If so, the simulator takes the initial sub-mode as the new current mode. When the current mode is a leaf mode, the simulator invokes the library simulating the physical environments and updates the internal state by getting the value detected from sensors. Then the simulator executes the control flow graph in the leaf mode. We assume that there is enough time to execute the CFG. The situation that tasks are allowed not to be finished in one period is not considered during simulation. In the end of each period, the guards of transitions are checked. The satisfactions of $\duration$ and $\after$ guards do not only depend on the current state, but also the past states. The simulator sets a counter for each $\duration$/$\after$ guard instead of recording the past states. As an {\modediagram} model is usually a non-terminating periodic system, the bound of periods is set during the process of simulation.

\subsection{SMC Algorithm}


\begin{figure}[t]
    \centering
    \begin{tabular}{rl}
    \textbf{input}  & $\MD$: the {\modediagram}, $\phi$: property, $B$: bound of periods \\
                    & $\delta$: confidence, $\epsilon$: approximation \\
    \textbf{output} & $p$: the probability that $\phi$ holds on an arbitrary run of $\MD$\\
    \textbf{begin}  & \\
      10 & $N := 4 * \frac{\log{\frac{1}{\delta}}}{\epsilon^2}$, $a := 0$ \\
      20 & \textbf{for} $i := 1$ \textbf{to} $N$ \textbf{do}\\
      30 & \hspace*{4mm} generate an initial state $s_0$ randomly \\
      40 & \hspace*{4mm} simulate the {\modediagram} from $s_0$ in $B$ periods to get the state trace $\Sigma$\\
      50 & \hspace*{4mm} \textbf{if} ($\mathcal{I_F}(\phi, \Sigma) = \True$) \textbf{then} $a := a + 1$\\
      60 & \textbf{end for} \\
      70 & \textbf{return} $\frac{a}{N}$ \\
      \textbf{end} &
    \end{tabular}
    \caption{Probability Estimation for \modediagram}\label{fig:smc}
\end{figure}
%\underline{}

We apply the methodology in ~\cite{SMCYounes05} to estimate the probability that a random run of an {\modediagram} will satisfy the given property $\phi$ with a certain precision and certain level of confidence. The statistical model checking algorithm for {\modediagram} is illustrated in Fig.~\ref{fig:smc}. Since the run of the {\modediagram} usually is infinite, the users can set the length of the sequence by the number of periods based on the concrete application. This algorithm firstly computes the number $N$ of runs  based on the formula $N := 4 * \log(1/ \delta) / \epsilon^2$ which involves the confidence interval $[p - \delta, p + \delta]$ with the confidence level $1 - \epsilon$. Then the algorithm generates the initial state (line 30) and gets a state trace $\Sigma$ by the inference rules defined in Table~\ref{tbl:inferenceRules} (line 40). The algorithm in line 50 decides whether $\phi$ holds on the constructed interval based on the interpretation for the specification language mentioned in Section~\ref{sec:specification}. If the interpretation is $\True$, the algorithm increases the number of traces on which property $\phi$ holds. Line 70 returns the probability for the satisfaction of $\phi$ on the {\modediagram}.

\subsection{Experiments}



We have implemented the {\modediagram} modeling and verification framework and applied it onto several real periodic control systems. The implementation framework of SMC is illustrated in Fig.~\ref{fig:implement}, where the simulator is used to simulate the {\modediagram} by the proposed operational semantics and the generated traces are for the statistical model checker. One of the real periodic control systems (termed as {\SA}) is for spacecraft control developed by Chinese Academy of Space Technology. Fig.~\ref{fig:mcoverall}(shown in Section 1) is a small portion of the {\modediagram} model for system {\SA}.

%Besides the modelling notation and the specification language, we provide some property patterns based on the specification language to facilitate the engineers to specify properties the system {\SA} should obey. Figure~\ref{fig:pattern} shows two examples of property patterns.

%\begin{figure}[t]
%    \begin{tabular}{cc}
%        \includegraphics[width=0.45\textwidth]{pic/p1.eps} & \includegraphics[width=0.45\textwidth]{pic/p2.eps} \\
%        $\pattern{1}{p}{t}$: no less than & $\pattern{2}{p}{t}$: no more than \\
%    \end{tabular}
%    \caption{Examples of Property Pattern}\label{fig:pattern}
%\end{figure}
%
%\[
%\pattern{1}{p}{t} \ = \
%                \Box \big ((\neg p \CHOP p \CHOP \neg p) \Rightarrow (\neg p \CHOP p \wedge \ell \ge t  \CHOP \neg p) \big)
%\]
%\[
%\pattern{2}{p}{t} \ = \
%                \Box \big((\neg p \CHOP p \CHOP \neg p) \Rightarrow (\neg p \CHOP p \wedge \ell \le t  \CHOP \neg p) \big)
%\]



\begin{figure}[t]
    \centering
    \includegraphics[scale=0.5]{pic/implement.eps}\\
    \caption{The Framework of Implementation}\label{fig:implement}
\end{figure}

We communicate with the engineers in CAST, summarize several properties the two models of spacecrafts should obey, and present these properties in our specification language. A total of 12 properties are developed by the engineers and these properties are verified on the systems {\SA}. We only highlight three properties  because the verification results on these three properties reveal two defects.

\begin{itemize}
\item
% 1. 系统将达到稳定状态
\textit{After 3000 seconds, the system will eventually reach the stable state forever}
\[
\ell \ge 3000 \Rightarrow tt ^\frown \Box(\sqrt{\omega_x^2 + \omega_y^2 + \omega_z^2} \leq 0.1 \wedge \sqrt{\dot{\omega_x}^2 + \dot{\omega_y}^2 + \dot{\omega_z}^2} \leq 0.01))
\]
where $\omega_x$, $\omega_x$ and $\omega_z$ are angles. $\dot{\omega_x}$, $\dot{\omega_x}$ and $\dot{\omega_z}$ are angle rates.

\item
% 2. 如果系统以模式2为初始模式，那么它最终将维持在模式5，6或8
\textit{The system starts from mode $m0$, and then it will finally switch to mode $m5$ or $m6$ or $m8$, and stay in one of these three modes forever}
\[
(\textsf{mode} = 0) ^{\frown} tt ^{\frown} \Box(\textsf{mode} = 5 \vee \textsf{mode = 6} \vee \textsf{mode} = 8)
\]

\item
% 3. 系统进入模式4之后，首先进入子模式0，然后转入子模式1，然后转入子模式2
\textit{Whenever the system switches to mode $m4$ and then leaves $m4$, during its stay in $m4$, it firstly stays in
sub-mode $G0$, and then it switches to sub-mode $G1$, and then $G2$.}
\[
    \begin{array}{c}
        \Box(\textsf{mode} \neq 4 ^{\frown} \textsf{mode} = 4 ^{\frown} \textsf{mode} \neq 4 \Rightarrow \textsf{mode} \neq 4 ^{\frown}\\
        \textsf{mode} = 4 \wedge (\textsf{gm} = 0 ^{\frown} \textsf{gm} = 1 ^{\frown} \textsf{gm} = 2)^{\frown} tt)
    \end{array}
\]
\end{itemize}

For the parameters of the statistical model checking algorithm, we set the half length of confident interval to be $1\%$ ($\delta = 1\%$) and the error rate to be $5\%$ ($\epsilon = 5\%$). Based on this algorithm, the total $7369$ traces for each control system are required to be generated to compute the probabilities during the verification process.

During the verification phase by the statistical model checking on {\modediagram}, two design defects in system {\SA} are uncovered by analyzing the verification results: (1)  A variable is not initialized properly. (2) A value from sensors is detected from the wrong hardware address. In the traditional developing process in CAST, these two defects may be revealed only after a prototype of the software is developed and then tested. Our approach can find such bugs in design phase and reduce the cost to fix defects.


