
\section{Introduction}

% The background: periodic control system
% The motivation: develop a convenient modeling notation
\hide{Periodic control systems are widely used in embedded computing area, for instance, spacecraft control system and automotive control system etc. Such systems are usually driven by a global or local clock, which triggers the behavior of the system in periods. Another feature of these systems is that they can be decomposed into several different modes from the system view.  Each mode actually represents an important state of the system. Further more, a mode can also be divided into several sub-modes. When a period ends, the system may switch from the current mode to another mode if the transformation condition holds. For instance, China Academy of Space Technology (CAST) spends great efforts in designing and developing embedded software for spacecrafts. The periodic control system is a key for the spacecrafts, which has the following features:}%

The control systems that are widely used in safety-critical embedded domains, such as spacecraft control and automotive control, usually reveal periodic behaviors. Such {\em periodic} control systems share some interesting features and characteristics:
\begin{figure*}[t]
    \centering
    \includegraphics[width=0.98\textwidth]{pic/overall.eps}\\
    \vspace*{-3mm}
    \caption{ {\modediagram}: An (Incomplete) Example }\label{fig:mcoverall}\vspace*{-3mm}
\end{figure*}

\begin{itemize}
    \item They are {\em mode-based}.  A periodic control system is usually composed of a set of modes, with each mode representing an important state of the system. Each mode either contains a set of sub-modes or performs controlled computation periodically.
    \item They are {\em computation-oriented}. In each mode,  a periodic control system may perform control algorithms involving complex computations. For instance, in certain mode, a spacecraft control system may need to process intensive data in order to determine its space location.

    \item They behave {\em periodically}.  A periodic control system is reactive and may run for a long time. The behavior of each mode is regulated by its own period. That is, most computations are performed within a  period and may be repeated in the next period if mode switch does not take place. A mode switch may only take place at the end of a period under certain conditions.

\end{itemize}

Despite the fact that periodic control systems have been widely used in areas such as spacecraft control, there is a lack of a concise and precise domain specific formal modeling language for such systems. In our joint project with China Academy of Space Technology (CAST), we have started with several existing modeling languages but they are either too complicated therefore require too big a learning curve for domain engineers, or  are too specific/general, therefore require non-trivial restrictions or extensions.  This motivates us to propose  a new formal but lightweight modeling language that matches exactly the need of the domain engineers,  the so-called  \underline{M}ode \underline{D}iagram \underline{M}odeling framework ({\modediagram}).

Although the proposed modeling notation {\modediagram} can be regarded as a variant of Statecharts \cite{statechart}, it has been specifically designed to cater for the domain-specific need in modeling periodic control systems. We shall now use an example  to illustrate informally the {\modediagram} framework, and leave the formal syntax and semantics to the next section. As shown in Fig~\ref{fig:mcoverall}, the key part of an {\modediagram} model is the collection of modes given in the mode level.  Each mode has a period, and the periods for different modes can be different. A mode can be nested and the transitions between modes or sub-modes may take place. A transition is enabled  if the associated guard is satisfied. In {\modediagram}, the transition guards may involve complex temporal expressions. For example, in the transition from mode {\sf G2} to mode {\sf m6}, in addition to the condition $\textsf{SK12=10}$, it also requires that the condition $\textsf{gm=2}$ has held for 40s, as captured by the $\mathsf{duration}$ predicate.

An {\modediagram} model is presented hierarchically. A mode that does not contain any sub-mode (termed a {\em leaf} mode) contains a control flow graph (CFG) encapsulating specific control algorithms or computation tasks. The details of CFGs are given in the CFG level. The CFGs may refer to modules (similar to procedures in conventional languages) details of which are given in the module level.

To support formal reasoning about {\modediagram} models, we also provide a property specification language inspired by an interval-like calculus~\cite{IntervalCalculus}, which facilitates the capture of temporal properties  system engineers may be interested in. Two example properties are listed in Fig~\ref{fig:mcoverall}. The property {\sf P1} says that ``whenever the system enters the {\sf m4} mode, it should stay there for at least 600s''. The formal details of the specification language is left to a later section.

To reason about whether an {\modediagram} model satisfies  desired properties specified by system engineers using the property specification language, we employ statistical model checking techniques~\cite{SMCYounes05,SMCYounesS02}. Since {\modediagram} may involve complex non-linear computation in its control flow graph, complete verification is undecidable. Apart from incompleteness, statistical model checking can verify  hybrid systems efficiently \cite{StatHeterogeneous}. Our experimental results on real-life cases have demonstrated that statistical model checking can help uncover  potential defects of {\modediagram} models.

In summary, we have made the following contributions in this paper:

\begin{itemize}\setlength{\itemsep}{0pt}
    \item  We propose a novel visual formal modeling notation {\modediagram}  as a concise yet precise modeling language for  periodic control systems. Such a notation is inspired from the industrial experiments of software engineers.
    \item  We present a formal semantics for {\modediagram} and a property specification language to facilitate the verification process.
    \item We develop a new statistical model checking algorithm   to verify   {\modediagram} models against various temporal properties. Some real-life case studies have been carried out to demonstrate the effectiveness of the proposed framework.  Furthermore,  the design defects of a real spacecraft control system are discovered by our approach.
\end{itemize}

The rest of this paper is organized as follows. Section~\ref{sec:modechart} presents the formal syntax and semantics of {\modediagram}. Section~\ref{sec:specification} introduces our interval-based property specification language and its semantics. The statistical model checking algorithm for {\modediagram} is developed in Section~\ref{sec:statistical}, followed by related work and concluding remarks.
	
