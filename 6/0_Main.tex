% =====================================================================%
%                       Making an article                             %
%=====================================================================%
\documentclass[submission]{eptcs}[10pt]
\providecommand{\event}{FTSCS 2012}

%=====================================================================%
%                        Packages import                              %
%=====================================================================%
\usepackage{graphicx,amssymb,amstext,amsmath,amsthm,hyperref,indentfirst}
\usepackage{array,amsfonts, amsmath, amssymb}
\usepackage{xspace}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{ulem}
\usepackage{paralist}
\usepackage{caption}
\usepackage{subcaption}
%\usepackage{multirow}
\usepackage{multicol}
\usepackage{wrapfig}
%\usepackage{subfig}
\usepackage{algorithm}
%\usepackage{algorithmic}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
%\usepackage[table]{xcolor}
\usepackage{tikz}
%\usetikzlibrary{arrows}
%\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]

%=====================================================================%
% Options to make stuff look pretty                                   %
%=====================================================================%
\hypersetup{
  pdfborder={0 0 0},
  colorlinks=true,
  linkcolor=blue,
  citecolor=blue,
  urlcolor=blue,
  % pdftitle={
  %   Exact Property Directed Backward Reachability Analysis,
  %   Introducing Backward Quantifier Elimination.
  % }
  pdftitle={
    Generating Property-Directed Potential Invariants By Backward Analysis
  }
}

\newcommand\reff{\textcolor{red}{[reffff]}}
%\renewcommand{\baselinestretch}{0.97}

%==================================================================%
%                         llncs configuration                      %
%==================================================================%
\title{
  Generating Property-Directed Potential Invariants By Backward Analysis
}
\author{
  Adrien Champion
  \institute{Onera, The French Aerospace Lab\\Toulouse, France}
  \institute{Rockwell Collins France\\Blagnac, France}
  \email{adrien.champion@onera.fr}
  \and
  R\'emi Delmas
  \institute{Onera, The French Aerospace Lab\\Toulouse, France}
  \email{remi.delmas@onera.fr}
  \and
  Michael Dierkes
  \institute{Rockwell Collins France\\Blagnac, France}
  \email{mdierkes@rockwellcollins.com}
}
\def\titlerunning{Property-Directed Lemmas By Backward Analysis}
\def\authorrunning{A. Champion, R. Delmas, M. Dierkes}

%==================================================================%
%                    Let's start the document                      %
%==================================================================%
\begin{document}
\normalem

%==================================================================%
%                              Title                               %
%==================================================================%
\maketitle

%==================================================================%
%                             Abstract                             %
%==================================================================%
\begin{abstract}
  This paper addresses the issue of lemma generation in a $k$-induction-based
  formal analysis of transition systems, in the linear real/integer arithmetic fragment.
  A backward analysis, powered by quantifier elimination, is used to output
  preimages of the negation of the proof objective, viewed as unauthorized states, or \emph{gray states}.
  Two heuristics are proposed to take advantage of this source of information.
  First, a thorough exploration of the possible partitionings of the gray state space
  discovers new relations between state variables, representing potential invariants.
  Second, an inexact exploration regroups and over-approximates disjoint areas
  of the gray state space, also to discover new relations between state variables.
  $k$-induction is used to isolate the invariants and check if they strengthen
  the proof objective.
  These heuristics can be used on the first preimage of the backward exploration, and each time a new one
  is output, refining the information on the gray states.
  In our context of critical avionics embedded systems,
  we show that our approach is able to outperform other academic or commercial
  tools on examples of interest in our application field.
  The method is introduced and motivated through two main examples, one of which was provided by
  Rockwell Collins, in a collaborative formal verification framework.
\end{abstract}

%==================================================================%
%                               Sections                           %
%==================================================================%
\section{Introduction}
\input{Introduction}

\section{Fault Tolerant Avionics Architectures}
\input{Systems}

\section{Related Work and Tools}
\input{Context}

\section{Notations}
\input{Notations}

\section{Proofs by Temporal Induction}
\input{kind}

\section{Approach Overview: Backward Exploration and Hull Computation}
\input{Approach}

\section{Generating Potential Lemmas Through Hull Computation}
\input{Hullqe}

\section{Applications}
\input{Applications}

\section{Framework and Implementation}
\input{Framework}

\section{Conclusion}
\input{Conclusion}

%==================================================================%
%                             Appendices                           %
%==================================================================%
%% \appendix
%% \section{Reconfiguration Logic}
%% \subsection{Double Counter}
%% \label{app:counter}
%% \begin{verbatim}
%%  1: node top(a,b,c: bool) returns (o1, o2, ok: bool);
%%  2: var
%%  3:   x, y, pre_x, pre_y: int;
%%  4:   n1, n2: int;
%%  5: let
%%  6:   n1    = 10;
%%  7:   n2    = 6;
%%  8:   x     = if (b or c) then 0 else (if (a and pre_x < n1) then pre_x + 1 else pre_x);
%%  9:   y     = if   (c)    then 0 else (if (a and pre_y < n2) then pre_y + 1 else pre_y);
%% 10:   o1    = x = n1;
%% 11:   o2    = y = n2;
%% 12:   ok    = o1 => o2;
%% 13:   pre_x = 0 -> pre(x);
%% 14:   pre_y = 0 -> pre(y);
%% 15:   prove(ok);                 (* main proof objective *)
%% 16:   prove(0 <= x and x <= 10); (* range lemma *)
%% 17:   prove(0 <= y and y <=  6); (* range lemma *)
%% 18: tel
%% \end{verbatim}
%% \subsection{Reconfiguration}
%% \label{app:reconf}
%% \begin{verbatim}
%% node conf1(x:bool; n:int) returns (y: bool);
%% var
%% pre_cpt: int;
%% cpt: int;
%% let
%% pre_cpt = 0 -> pre(cpt);
%% cpt = if x then (if pre_cpt < n then pre_cpt+1 else pre_cpt) else 0;
%% y = cpt >= n;
%% assert(0 <= pre_cpt and pre_cpt <= n);
%% tel

%% node latch(x: bool) returns (y: bool);
%% var
%% pre_y: bool;
%% let
%% pre_y = x -> pre(y);
%% y = x or pre_y;
%% tel


%% node range_monitor(
%% 	 value, default_value, min, max:int; 
%% 	 n:int
%% ) returns (
%%   	 out_of_range, corrupt: bool; 
%%   	 ranged_value:int
%% );
%% let
%% out_of_range = value > max or value < min;
%% corrupt = latch(conf1(out_of_range, n));
%% ranged_value = if corrupt then default_value
%%                else (if value > max then max
%%                                     else (if value < min then min
%%                                                          else value));
%% tel

%% node priority(
%% 	 command, safe_command_value: int; 
%% 	 command_failure:bool;
%% 	 other_in_command: bool;
%% 	 n: int
%% ) returns (
%%     safe_command: int; 
%% 	in_command: bool
%% );
%% let
%% in_command = not command_failure and conf1(not other_in_command, n);
%% safe_command = if in_command then command else safe_command_value;
%% tel


%% node system(
%% 	 sensor_value1, sensor_value2, sensor_value3: int
%% ) returns (
%%   	command: int; 
%% 	safe_command1, safe_command2, safe_command3: int; 
%% 	in_command1, in_command2, in_command3: bool;
%% 	ok: bool
%% );

%% var
%% max, min, default_value, safe_value: int;
%% n1, n2, n3: int;
%% m1, m2, m3, m4: int;
%% ranged_sensor1, ranged_sensor2, ranged_sensor3:int;
%% out_of_range1, out_of_range2, out_of_range3: bool;
%% corrupt1, corrupt2, corrupt3: bool;
%% no_command: bool;

%% let
%% max = 90;
%% min = -90;
%% default_value = 10;
%% safe_value = 0;
%% n1 = 10;
%% n2 = n1+2;
%% n3 = n1+5;
%% m1 = 20;
%% m2 = m1+2;
%% m3 = m1+5;
%% m4 = m3+12;
%% (out_of_range1, corrupt1, ranged_sensor1) =
%%   range_monitor(sensor_value1, default_value, min, max, n1);
%% (out_of_range2, corrupt2, ranged_sensor2) =
%%   range_monitor(sensor_value2, default_value, min, max, n2);
%% (out_of_range3, corrupt3, ranged_sensor3) =
%%   range_monitor(sensor_value3, default_value, min, max, n3);

%% (safe_command1,  in_command1) =
%%   priority(ranged_sensor1, safe_value, corrupt1, false, m1);
%% (safe_command2,  in_command2) =
%%   priority(ranged_sensor2, safe_value, corrupt2, in_command1, m2);
%% (safe_command3,  in_command3) =
%%   priority(ranged_sensor3, safe_value, corrupt3, in_command1 or in_command2, m3);

%% command = safe_command1 +  safe_command2 +  safe_command3;
%% no_command = not(in_command1 or in_command2 or in_command3);
%% ok = not conf1(no_command, m4);
%% assert(not corrupt1 or not corrupt2 or not corrupt3);
%% --%PROPERTY ok;
%% --%MAIN;
%% tel
%% \end{verbatim}


%% \section{Rockwell Collins' Triplex Voter}
%% \label{app:voter}
%% \begin{verbatim}
%% node top(input1, input2, input3: real) returns (output: real);
%% var
%%   equalized1, equalized2, equalized3: real;
%%   equalization1, equalization2, equalization3 : real;
%%   satCentering, centering : real;
%%   df1, df2, df3, st1, st2, st3, c1, c2, c3, d1, d2, d3 : bool;
%%   check: bool;
%% let
%%   assert (input1 <  0.2); assert (input1 > -0.2);
%%   assert (input2 <  0.2); assert (input2 > -0.2);
%%   assert (input3 <  0.2); assert (input3 > -0.2);

%%   equalized1 = input1 - equalization1;
%%   df1 = equalized1 - output;
%%   st1 = if (df1 > 0.5) then 0.5 else (if (df1 < -0.5) then -0.5 else df1);
%%   equalization1 = 0.0 -> pre (equalization1) + (pre (st1) - pre (satCentering)) * 0.05;
 
%%   equalized2 = input2 - equalization2;
%%   df2 = equalized2 - output;
%%   st2 = if (df2 > 0.5) then 0.5 else (if (df2 < -0.5) then -0.5 else df2);
%%   equalization2 = 0.0 -> pre (equalization2) + (pre (st2) - pre (satCentering)) * 0.05;

%%   equalized3 = input3 - equalization3;
%%   df3 = equalized3 - output;
%%   st3 = if (df3 > 0.5) then 0.5 else (if (df3 < -0.5) then -0.5 else df3);
%%   equalization3 = 0.0 -> pre (equalization3) + (pre (st3) - pre (satCentering)) * 0.05;

%%   c1 = equalized1 > equalized2; c2 = equalized2 > equalized3; c3 = equalized3 > equalized1;
%%   output = if (c1 = c2) then equalized2 else (if (c2 = c3) then equalized3 else equalized1);
%%   d1 = equalization1 > equalization2;
%%   d2 = equalization2 > equalization3;
%%   d3 = equalization3 > equalization1;

%%   centering = if       (d1 = d2) then equalization2
%%               else (if (d2 = d3) then equalization3 
%%               else                    equalization1);
%%   satCentering = if       (centering >  0.25) then  0.25
%%                  else (if (centering < -0.25) then -0.25
%%                  else                               centering);
%%   check = ( equalization1 <=  2.0 * 0.2) and ( equalization1 >= -2.0 * 0.2) and
%%           ( equalization2 <=  2.0 * 0.2) and ( equalization2 >= -2.0 * 0.2) and
%%           ( equalization3 <=  2.0 * 0.2) and ( equalization3 >= -2.0 * 0.2);

%%   prove(check);
%% tel
%% \end{verbatim}

%=================================================================%
%                            Bibliography                         %
%=================================================================%
\bibliographystyle{eptcs}
\bibliography{Biblio}


%=================================================================%
%                      End of the Document                        %
%=================================================================%
\end{document}
