\label{systems}

We consider embedded reactive software functions which contribute to
the safe operation of assemblies of hardware sensors, networked
computers, actuators, moving surfaces, {\em etc.} called
\emph{functional chains}. A functional chain can for instance be in
charge of "controlling the aircraft pitch angle", and must meet both
qualitative and quantitative safety requirements depending on the
effects of its failure. Effects are ranked from MIN (minor effect) to
CAT (catastrophic effect, with casualties). For instance, the failure of a pitch
control function is ranked CAT, and the function shall be robust to at least a double
failure and have an average failure rate of at most $\mathit{10^{-9}}$
per flight hour. In order to meet these requirements, engineers
must introduce hardware and software redundancy and implement several fault detection and
reconfiguration mechanisms in software. 

\begin{figure}[t]
\begin{center}
\scriptsize
\begin{tikzpicture}[scale=0.95]
\node[draw] (PITCH) at (-2,0) {PITCH};
\node[draw] (SPEED) at (-2,1) {SPEED};
\node[draw] (ORDER) at (-2,2) {ORDER};

\node[draw] (SMON3) at (-0.5,0) {SMON};
\node[draw] (SMON2) at (-0.5,1) {SMON};
\node[draw] (SMON1) at (-0.5,2) {SMON};

\node[draw] (VOTE13) at (1,0) {VOTE};
\node[draw] (VOTE12) at (1,1) {VOTE};
\node[draw] (VOTE11) at (1,2) {VOTE};

\node[draw] (LAW3) at (2.3,0) {LAW};
\node[draw] (LAW2) at (2.3,1) {LAW};
\node[draw] (LAW1) at (2.3,2) {LAW};

\node[draw] (VOTE23) at (3.6,0) {VOTE};
\node[draw] (VOTE22) at (3.6,1) {VOTE};
\node[draw] (VOTE21) at (3.6,2) {VOTE};

\node[draw] (AMON3) at (5,0) {AMON};
\node[draw] (AMON2) at (5,1) {AMON};
\node[draw] (AMON1) at (5,2) {AMON};

\node[draw] (RCF3) at (6.5,0) {RCF};
\node[draw] (RCF2) at (6.5,1) {RCF};
\node[draw] (RCF1) at (6.5,2) {RCF};

\node[draw] (ACT) at (8,1) {ACT};

\draw[->, >=latex] (PITCH) -- (SMON1);
\draw[->, >=latex] (SPEED) -- (SMON1);
\draw[->, >=latex] (ORDER) -- (SMON1);
\draw[->, >=latex] (SMON1) -- (VOTE11);
\draw[->, >=latex] (SMON1) -- (VOTE12);
\draw[->, >=latex] (SMON1) -- (VOTE13);
\draw[->, >=latex] (VOTE11) -- (LAW1);
\draw[->, >=latex] (LAW1) -- (VOTE21);
\draw[->, >=latex] (LAW1) -- (VOTE22);
\draw[->, >=latex] (LAW1) -- (VOTE23);
\draw[->, >=latex] (VOTE21) -- (AMON1);
\draw[->, >=latex] (ACT) -- (AMON1);
\draw[->, >=latex] (AMON1) -- (RCF1);
\draw[->, >=latex] (RCF1) -- (ACT);
\draw[->, >=latex] (RCF1) -- (RCF2);
\draw[->, >=latex] (RCF1) to[out=-50, in=50] (RCF3);

\draw[->, >=latex] (PITCH) -- (SMON2);
\draw[->, >=latex] (SPEED) -- (SMON2);
\draw[->, >=latex] (ORDER) -- (SMON2);
\draw[->, >=latex] (SMON2) -- (VOTE11);
\draw[->, >=latex] (SMON2) -- (VOTE12);
\draw[->, >=latex] (SMON2) -- (VOTE13);
\draw[->, >=latex] (VOTE12) -- (LAW2);
\draw[->, >=latex] (LAW2) -- (VOTE21);
\draw[->, >=latex] (LAW2) -- (VOTE22);
\draw[->, >=latex] (LAW2) -- (VOTE23);
\draw[->, >=latex] (VOTE22) -- (AMON2);
\draw[->, >=latex] (AMON2) -- (RCF2);
\draw[->, >=latex] (RCF2) -- (ACT);
\draw[->, >=latex] (ACT) to[out=-160,in=-20] (AMON2);
\draw[->, >=latex] (RCF2) -- (RCF3);

\draw[->, >=latex] (PITCH) -- (SMON3);
\draw[->, >=latex] (SPEED) -- (SMON3);
\draw[->, >=latex] (ORDER) -- (SMON3);
\draw[->, >=latex] (SMON3) -- (VOTE11);
\draw[->, >=latex] (SMON3) -- (VOTE12);
\draw[->, >=latex] (SMON3) -- (VOTE13);
\draw[->, >=latex] (VOTE13) -- (LAW3);
\draw[->, >=latex] (LAW3) -- (VOTE21);
\draw[->, >=latex] (LAW3) -- (VOTE22);
\draw[->, >=latex] (LAW3) -- (VOTE23);
\draw[->, >=latex] (VOTE23) -- (AMON3);
\draw[->, >=latex] (ACT) -- (AMON3);
\draw[->, >=latex] (AMON3) -- (RCF3);
\draw[->, >=latex] (RCF3) -- (ACT);
\end{tikzpicture}
\normalsize
\caption{Shuffled, triple channel architecture}
\label{fig:archi}
\end{center}
\vspace{-15pt}
\end{figure}

A frequently encountered architectural design pattern, triplication
with shuffle, is depicted in Fig.~\ref{fig:archi}.  It allows to recover from single failures and to detect double failures. Data sources, data
processing hardware and functions are triplicated to obtain three
\emph{channels}. The actuator is not replicated. Data is locally monitored
right after acquisition/production, but is also broadcast across
channels to be checked using triplex voting functions, in order to detect
complex error situations. Last, in each channel, depending on the
fault state of the channel and the observable behavior of other
channels, the \emph{reconfiguration logic} decides whether the channel in question
must take control of the actuator or on the contrary mute itself. Being \emph{healthy}
for a channel means that no fault has occurred for a sufficient amount
of consecutive time steps to become \emph{confirmed}.

Previous work by our team addresses the formal verification of
control laws numerical stability~\cite{RouxGaroche2011}, yet
ensuring proper behavior of \emph{voting functions} and
\emph{reconfiguration logic} as introduced here is equally important,
for these building blocks and design patterns are ubiquitous in fault
tolerant avionics software.  For the voting logic, we focus on BIBO
properties, ``bounded input implies bounded output'', the verification
of which is detailed in Section~\ref{applications:voter}. For
reconfiguration logic, which makes an extensive use of integer timers
and discrete logic, we focus on bounded liveness properties such as
``assuming at most two sensor, network or CPU faults, the actuator
must never remain idle for more than N consecutive time units'', the
verification of which is addressed in Section~\ref{applications:reconf}.
