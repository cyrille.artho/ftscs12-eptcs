\label{intro}
The recent DO-178C and its formal methods supplement DO-333 published by
RTCA\footnote{\href{http://www.rtca.org/}{http://www.rtca.org/}}
acknowledge the use of formal methods for the verification and
validation of safety critical flight control software and allow
their use in development processes. Successful
examples of industrial scale formal methods applications exist, such
as the verification by Astr\'ee \cite{BlanchetEtAl-PLDI03} of the run-time safety of the Airbus A380 flight control software C code.
However, the verification of general functional properties at model level, {\em i.e.}  on
Lustre~\cite{DBLP:conf/popl/CaspiPHP87} or MATLAB Simulink{\textcopyright} programs, from which the
embedded code is generated, still requires expert human intervention to succeed on
common avionics software design patterns,
preventing industrial designers from using formal verification on a larger scale.
Formal verification at model level is important, since it helps raising the confidence in the correctness of the
design at early stages of the development process.
Also, the formal properties and lemmas discovered at model level can be forwarded to the generated code, in order
 to facilitate the final design verification and its acceptance by certification authorities~\cite{dierkes2012}.
Our work addresses some of the issues encountered when attempting formal verification of properties of synchronous data flow models written in Lustre.
We propose a property-directed lemma generation approach, together with a prototype implementation. The proposed approach aims at reducing the amount of human
intervention usually needed to achieve {\em $k$-induction} proofs, possibly using {\em abstract interpretation} technique in cooperation.
Briefly outlined, the approach consists first in an abstract interpretation pass to discover coarse bounds on the numerical state variables of the
system; a $k$-induction engine and our lemma generation techniques are then ran in parallel
to search for potential invariants in order to strengthen the property.
We insist on the fact that the primary goal of the proposed method is discovering
missing information needed to prove properties the verification of which is either very expensive or impossible with currently available
methods and tools, rather than improving the performance of the verification of properties which are already relatively easily provable.

The paper is structured as follows: Section~\ref{systems} describes
the embedded software architectures targeted by our work. Related work and tools
are discussed in Section~\ref{related} before notations and vocabulary are
given in Section~\ref{notations}.
A description of the underlying $k$-induction engine assumed in this paper follows in Section~\ref{sec:kind}.
We introduce and motivate our approach in Section~\ref{approach} and detail the
lemma generation techniques in Section~\ref{hullqe}. The proposed approach is then
illustrated on a reconfiguration logic example and on Rockwell Collins industrial triplex sensor
voter in Section~\ref{applications}. Implementation is
briefly discussed in Section~\ref{framework}, before concluding in Section~\ref{conclusion}.

