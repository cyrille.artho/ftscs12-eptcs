\label{applications}

In this section we discuss the results of the proposed approach on two
real world examples: a reconfiguration logic and the triplex voter of
Rockwell Collins.

\subsection{Reconfiguration Logic}
\label{applications:reconf}

Distributed reconfiguration logic as presented in
Section~\ref{systems} would be best described as a distributed
priority mechanism. In each redundant channel, the reconfiguration
logic comes last and monitors the warning flags raised by the
monitoring logic implemented earlier in the data flow. Integer timers and latches are used to confirm warnings over a number of
consecutive time steps and trigger a reconfiguration. The duration of
the various confirmations can vary from a few steps to hundreds or
thousands of steps and are tuned by system designers to be not overly
sensitive to transient perturbations, which would unnecessarily trigger
reconfigurations of otherwise healthy channels, while being fast
enough to ensure safety. Assuming at most two
sensors, network or CPU faults, the following generic property is expected
to hold for the reconfiguration mechanism: ``No unhealthy channel shall be in
control for more than $N$ steps''. This property can be decomposed and
instantiated per channel. However, a property such as ``No more than one channel shall be in command at any time'', or ``The actuator must never stay idle for
more than $m_4$ steps'' are more challenging because they cover all
three channels simultaneously and drag many state
variables in their cone of influence. For instance, the formal verification of the second
property is done by
assembling a model of the distributed system and by using the
synchronous observer technique as shown in Figure~\ref{fig:reconf-observer}. The observer uses a timer and is coded
so that its output becomes true as soon as the absence of control of
the actuator has been confirmed for the requested amount of $m_4$
consecutive steps. The proof objective on the system/observer composition is
to show that the output of this observer can never be true.

\begin{figure}[t]
\begin{center}
\scriptsize
\begin{tikzpicture}[scale=0.95]
\node[draw] (W1) at (-3.0, 2.0) {MON};
\node[draw] (W2) at (-3.0, 1.0) {MON};
\node[draw] (W3) at (-3.0, 0.0) {MON};
\node[draw] (RCF1) at (-1.5, 2.0) {RCF};
\node[draw] (RCF2) at ( 0.0, 1.0) {RCF};
\node[draw] (RCF3) at ( 1.5, 0.0) {RCF};
\node[draw] (PLUS) at ( 3.0, 1.0) {+};
\node[draw] (ACT) at  ( 4.5, 1.0) {ACT};
\node[draw] (OBS) at  ( 4.2, -1.0) {Observer};

\draw[->, >=latex] (W1) -- (RCF1);
\draw[->, >=latex] (W2) -- (RCF2);
\draw[->, >=latex] (W3) -- (RCF3);

\draw[->, >=latex] (RCF1) to[out=0, in=90] (PLUS.north);
\draw[->, >=latex] (RCF1) to[out=0, in=90] (RCF2.north);
\draw[->, >=latex] (RCF1) to[out=0, in=90] (RCF3.north);

\draw[->, >=latex] (RCF2) to[out=0, in=180] (PLUS.west);
\draw[->, >=latex] (RCF2) to[out=0, in=90] (RCF3.north west);
\draw[->, >=latex] (RCF3) to[out=0, in=-90] (PLUS.south);

\draw[->, >=latex] (PLUS) -- (ACT);

\draw[->, >=latex] (PLUS.east) to[out=0, in=90] (OBS.north);
\draw[->, >=latex] (W1.east) to[out=-20, in=180] (OBS.west);
\draw[->, >=latex] (W2.east) to[out=-15, in=180] (OBS.west);
\draw[->, >=latex] (W3.east) to[out=-10, in=180] (OBS.west);
\draw[->, >=latex] (OBS.east) -- (5.5,-1.0)node[above]{OK};
\end{tikzpicture}
\end{center}
\normalsize
\caption{Reconfiguration subsystem with observer.}
\label{fig:reconf-observer}
\vspace{-15pt}
\end{figure}

The timer logic found in this system is similar to that of the toy example developed in
Section~\ref{approach:counter}, and instantiated several times, indeed a channel becoming corrupt triggers several timers with different
bounds, running into each other or in parallel.
Let us now see how  hullification performs on this system.
The first preimage does not contain enough
information, since hullification generates no potential lemma which either strengthens
the PO or is $k$-inductive by itself.
The union of the first and second preimages however allows hullification to generate
about $200$  potential invariants.
Once they are negated, $k$-induction invalidates most of them
and indicates the PO was found ($1$-)inductive conjoined with about $50$ lemmas after about $30$ seconds of computation.


After the minimization phase described in Section~\ref{approach}, it turns out
that only three lemmas are required.
If we call $timer_i$ the integer variable used to count the time
channel $i$ is not in command for $1\leq i\leq 3$, and $timer_o$
the timer used by the observer, the lemmas are:
$\neg(timer_o - timer_i\;\geq m_4 - m_i -1)$
where $1\leq i\leq 3$.
These lemmas are found no matter the values of the $m_i$
for $1\leq i\leq 4$.
We insist on the interest of hullification here.
Merging polyhedra in some single arbitrary order is too coarse and the resulting
hull cannot strengthen the PO, whereas the thorough exploration generates useful lemmas.

The reconfiguration logic was also analyzed using NBac, Scade Design
Verifier and Tinelli's Kind.  NBac did not succeed in proving the
property after 1 hour of computation. Both the Scade Design Verifier
and Kind kept on incrementing the induction depth without finding a proof after 30 minutes of run time.

The invariant generation of Kind was also run on this system, and
yielded a number of small theorems, but obviously not property
directed and unfortunately not sufficient to strengthen the PO and
prove it.

In conclusion, the proposed combination of backward analysis, hullification and $k$-induction allows us to complete a
proof in a few seconds on a widely used avionics design pattern, where other
state of the art tools fail. In addition, we see two very interesting points worth
highlighting about hullification:
\begin{inparaenum}[\itshape (i)]
  \item The PO is made ($1$-)inductive, implying the proof can
        easily and quickly be re-run and checked by any existing induction tool;
  \item the time needed to complete the proof does not depend
        on the numerical values of the system --about thirty
        seconds on a decent machine in practice for this
        system\footnote{Using our prototype implementation in Scala.}.
\end{inparaenum}
This is very important for critical embedded systems manufacturers
as point {\itshape (i)} means that the proofs are trustworthy, both for the industrials
themselves and the certification organisms.
On the other hand, point {\itshape (ii)} implies that strengthening lemmas can be very quickly generated
for similar design patterns with altered numerical values, easing the
integration of formal verification in the development process.
Indeed, it avoids the need for an expert to manually transpose the lemmas on the
new system, as can be the case for complicated and resource/time consuming
proofs.


\subsection{The Triplex Voter}

\label{applications:voter}


Let us now turn to the Rockwell Collins triplex sensor voter, an
industrial example of voting logic as introduced in
Section~\ref{systems}, implementing
redundancy management for three sensor input values. 
This voter does not compute an average value, but uses the $middleValue(x,y,z)$ function, 
which returns the input value, bounded by the minimum and the maximum input values
({\em i.e.} $z$ if $y < z < x$).
Other voter algorithms which use a (possibly weighted) average value are more sensitive 
to one of the input values being out of the normal bounds.
The values considered for voting are {\em equalized} by subtracting
equalization values from the inputs.
The following recursive equations describe the behaviour of the voter with $X \in \{A, B, C\}$:
\[
\begin{array}{rcl}
EqualizationX_0 & = & 0.0\\
EqualizedX_t & = & InputX_t - EqualizationX_t\\
EqualizationX_{t+1}	& = & 0.9 * EqualizationX_t + \\
& & \hspace{-12ex}0.05 * (InputX_t + ((EqualizationX_t - VoterOutput_t) - Centering_t))\\
Centering_t & = & middleValue(EqualizationA_t, EqualizationB_t,\\
& &\hspace{14.5ex} EqualizationC_t)\\
VoterOutput_t & = & middleValue(EqualizedA_t, EqualizedB_t, EqualizedC_t)\\
\end{array}
\]
The role of the equalization values is to compensate offset errors of 
the sensors, assuming that the middle value gives the most accurate 
measurement. 

We are interested in proving Bounded-Input Bounded-Output (BIBO) stability 
of the voter, which is a fundamental requirement for 
filtering and signal processing systems, ensuring that the system output cannot 
grow indefinitely as long as the system input stays within a certain range. In 
general, it is necessary to identify and prove auxiliary system invariants in 
order to prove BIBO stability.

So, we want to prove the stability of the system, {\em i.e.} we want to
prove that the voter output is bounded as long as the input values differ 
by at most the maximal authorized deviation $MaxDev$ from the true value 
of the measured physical quantity represented by the variable $TrueValue$.
In our analysis, we fixed the maximal sensor deviation to $0.2$, a value 
that domain experts gave us as typical value in practical applications.
It is staightforward to prove that the system is stable if the equalization 
values are bounded. 

When applied to Rockwell Collins \textbf{tri}plex sensor voter, our prototype implementation
manages to prove the PO in less than $10$ seconds by discovering that
$-0.9\le \sum^3_{i=1} Equalization_i\le 0.9$ is a strengthening lemma, using ICH calculation.
Again, the time taken to complete the proof does not depend on the
system numerical constants, and the strengthened PO is ($1$-)inductive.
We insist on the importance of these characteristics for both industrials and
certification organisms: the proof is trustworthy and can be redone easily for
similar, slightly altered designs.

The stability of the system without fault detection nor reset was already 
proven in~\cite{dierkes11}, but the necessary lemmas had to be found by hand
after the Scade Design Verifier, Kind as well as Astrée (which was run
on C-Code generated from the Lustre source) failed at automatically
verifying the BIBO property. 
