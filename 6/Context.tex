\label{related}

In this section we review the state of the art of verification tools
relevant in our application domain, \emph{i.e.}, of currently available
tools and techniques allowing to address synchronous data flow models
written in Lustre. We distinguish two main families of verification
approaches.

First, approaches based on abstract interpretation
(AI~\cite{DBLP:conf/popl/CousotC77}). The tool
NBac~\cite{DBLP:journals/fmsd/Jeannet03} for instance allows to
analyze properties of Lustre models by using a combination of forward
and backward fixpoint computation using AI. AI tends to need expert
tuning for the choice of abstract domains, partitioning, {\em etc.} to
behave correctly on the systems we consider. NBac proposes a heuristic
selection of AI parameter tuning, which dynamically refines domains
and partitionings to try to obtain a better precision without falling
in a combinatorial blowup.

Second, the family of $k$-induction~\cite{DBLP:conf/fmcad/SheeranSS00}
based approaches, with the commercial tool Scade Design
Verifier\footnote{\href{http://www.esterel-technologies.com/products/scade-suite/add-on-modules/design-verifier}{http://www.esterel-technologies.com/products/scade-suite/add-on-modules/design-verifier}},
or the academic tool Kind~\cite{DBLP:journals/corr/abs-1111-0372}. Kind is the most
recently introduced tool, and wraps the $k$-induction core in an
automatic counter example guided abstraction refinement loop whereas
the Scade Design Verifier does not.  $k$-induction is an exact
technique, in which little or no abstraction is performed (the
concrete semantics of the program is analyzed). Experiments show that
it does not scale up out of the box on the systems
encountered in our application field.  Proving proof obligations
on such systems often requires to unroll the system's transition relation to the
reoccurrence diameter of the model which can be very large in practice
(hundred or thousands of transitions).  For such proof
obligations, which are either $k$-inductive for a $k$ too large to be
reached in practice, or even non-inductive at all, numerical lemmas
are needed to help better characterize the reachable state space and
facilitate the inductive step of the
reasoning.

In order to address this common issue with $k$-induction, automatic
lemma generation techniques have been studied. Two main approaches can
be distinguished.  First, property agnostic approaches, such
as~\cite{KahGT-NFM-11}, in which template formulas are instantiated in
a brute force manner on combinations of the system state variables to
obtain a set of potential invariants. They are then analyzed alongside the
PO using the main $k$-induction engine.  Second, property directed approaches, such
as~\cite{DBLP:conf/ictac/BradleyM06,DBLP:conf/vmcai/Bradley11}, in
which the negation of root states of counterexamples are used as
strengthening lemmas, with or without generalization, or are used to
guide template instantiation. 
Also worth mentioning, interpolation~\cite{DBLP:conf/tacas/McMillan08}
yields very interesting results in lemma generation but unfortunately to our
knowledge no interpolation tool analyzing Lustre code exists.

We consider a lemma generation pass successful when the generated
potential invariants allow to prove the original proof objective with a
$k$-induction run with a small $k$. Once the right lemmas are found,
the proof can be easily re-run and checked by third party
$k$-induction tools, an important criterion for industrials and
certification organisms.
As we will see in the rest of the paper, the lemma generation approach proposed
in this paper takes inspiration from all the aforementioned techniques 
%to compensate one's weakness with another's strength
: while somehow brute force in its exploration of
the gray state space partitionings, our approach discovers relevant lemmas thanks
to its property-directed nature.


%% \subsection{Notations}
%% Logic, integers, rationals, linear, synchronous systems, strengthening lemmas,
%% dnf, polyhedra.
