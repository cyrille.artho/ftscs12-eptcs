\label{hullqe}

We now detail two heuristics which use the preimages output by the backward analysis.
The first one follows the example from Section~\ref{approach:counter} and consists
in a thorough, exact exploration of the partitionings of the gray state space.
After explaining the basic algorithm in Section~\ref{hullqe:algorithm}, optimizations are
developed in Section~\ref{hullqe:optimization}. A small example illustrates
the method in Section~\ref{hullqe:example}.
The second heuristic over-approximates areas of the gray state space in the spirit of the
discussion in Section~\ref{approach:duplex}, and is discussed
in Section~\ref{hullqe:ich}.
Both aim at discovering new relations between the state variables which once negated
become potential invariants.
Figure~\ref{fig:global} provides a high level view of the different components and the way
they interact internally and with the exterior.

\subsection{Hullification Algorithm}
\label{hullqe:algorithm}

The algorithm presented in this section, called \emph{hullification}, calculates all
the convex hulls that can be
created by iterating the convex hull calculation on a given set of polyhedra, called
the {\em source} polyhedra.
In this algorithm we will calculate ECH as opposed to ICH to avoid both losing precision
in the process and the potential combinatorial blow up -- ICH are used in a different
approach in Section~\ref{hullqe:ich}.
The difficulty here is to not miss any of the ECH that can be possibly
calculated from the source polyhedra. Indeed, back to the example on Figure~\ref{fig:echInt}
the merging order $(1)$
misses the ECH of $s_2$ and $s_5$ (represented as a dotted line), and consequently the potential relational 
lemma $y\leq-x+4$, which could have strengthened the PO.


Imperative and slightly object-oriented pseudo-code is provided on
Algorithm~\ref{alg:hull}.
The purpose of $generatorSetMemory$ is related to optimizations,
discussed in Section~\ref{hullqe:optimization}.
Please note that for the sake of clarity, the function called on line~$20$ is
detailed separately on Algorithm~\ref{alg:update}.
The hullification algorithm iterates on a set of pairs called the $generatorSet$:
the first component of each of these pairs is a convex hull called the {\em pivot}.
The second one is a set of convex hulls the pivot will be tried to be exactly merged with,
called the pivot {\em seeds}.
Note that since the ECHs are calculated by merging polyhedra two by two,
our hullification algorithm cannot find convex hulls that require to merge more than
two polyhedra at the same time to be exact.\\
The $generatorSet$ is initialized such that for any couple $(i,j)$ such that $0\leq i\leq n$
and $i< j\leq n$, $p_i$ is a pivot and $p_j$ is one of its seeds, line~$3$.
A $newgeneratorSet$ is initialized with the same
pivots as the $generatorSet$ but without any seeds (line~$8$).
At each iteration (line~$6$), a first loop enumerates the pairs of pivot and seeds
of the generation set (line~$9$).
Embedded in the first one, a second loop iterates on the seeds (line~$11$) and tries
to calculate the ECH of the pivot and the seed (line~$14$) as described in
Section~\ref{notations}.
If the exact merge was successful, the new ECH is added to the seeds of the pivots of
the $newGeneratorSet$ (line~$20$, detailled below) and as a new pivot with no seeds.
Once the elements of the $generatorSet$ have all been inspected and if new
ECH(s) have been found, a new iteration begins with the $newGeneratorSet$.
When no new convex hulls are discovered during an iteration, the algorithm returns all the ECHs found so far (line~$27$).

\begin{algorithm}[t!]
\scriptsize
\caption{Hullification Algorithm:\\$hullification(\{p_i|0\leq i\leq n\})$.}
\label{alg:hull}
\begin{algorithmic}[1]
\State $generatorSetMemory=\{\{p_i\}|0\leq i\leq n\}$
\State $sourceMap=\{p_i\rightarrow \{p_i\}|0\leq i\leq n\}$
\State $generatorSet=\{(p_i,S_i)|0\leq i\leq n\land S_i=\{p_k|i<k\leq n\}\}$
\State $generatorSetMemory = generatorSetMemory \cup \{\{p_i,p_j\}|0\leq i\leq n,\; i< j\leq n\}$
\State $fixedPoint=\mathbf{false}$
\While {$(\neg\text{fixedPoint})$}
  \State $fixedPoint=\mathbf{true}$
  \State $newGeneratorSet=\{(p_i,\{\})|\exists S,(p_i,S)\in generatorSet\}$
  \ForAll {$((pivot,seeds)\in generatorSet)$}
    \State $sourcePivot=sourceMap.get(pivot)$
    \ForAll {$(seed\in seeds)$}
      \State $sourceSeed=sourceMap.get(seed)$
      \State $source=sourcePivot\cup sourceSeed$
      \State $hull=computeHull(pivot,seed)$
      \State $newGeneratorSet.update(pivot,newGeneratorSet.get(pivot) - seed)$
      \If {$(hull\not=\mathbf{false})$}
        \State $fixedPoint=\mathbf{false}$
        \State $sourceMap.add(hull\rightarrow source)$
        \State $newGeneratorSet=$
        \State $\quad\quad updateGenSet(hull,source,pivot,seed,newGeneratorSet)$
      \EndIf
    \EndFor
  \EndFor
  \State $generatorSet=newGeneratorSet$
  \State // Communication.
\EndWhile
\State $\mathbf{return}\; \{p_i|\exists S,(p_i,S)\in generatorSet\}$
\end{algorithmic}
\end{algorithm}

\begin{algorithm}[t!]
\scriptsize
\caption{Updating the $newGeneratorSet$:\\$updateGenSet(hull,source,newGeneratorSet)$.}
\label{alg:update}
\begin{algorithmic}[1]
\State $result=\{\}$
\ForAll {$((pivotAux,seedsAux)\in newGeneratorSet)$}
  \State $sourceAux = sourceMap.get(pivotAux)$
  \State $shallAdd = (sourceAux\cup source)\not\in generatorSetMemory\;\&\&$
  \State $\quad\quad\quad\quad\quad(sourceAux \not\subset source)$
  \If {$(shallAdd)$}
    \State $result.update((pivotAux,seedsAux\cup\{hull\}))$
    \State $generatorSetMemory.add(sourceAux\cup source)$
  \Else
    \State $result.add((pivot,seeds))$
  \EndIf
\EndFor
\State $result.add((hull,\{\}))$
\State $\mathbf{return}\;result$
\end{algorithmic}
\end{algorithm}

\subsection{Optimizing Hullification}
\label{hullqe:optimization}

The hullification algorithm is highly combinatorial, and this section
presents optimizations that improve its scalability.

%% The convex hull, whether exact or not, of a set of polyhedra being unique, we will
%% sometimes refer to an ECH by its source, {\em e.g.} "$\{s_1,\dots,s_n\}$" for "the ECH
%% calculated by exactly merging $s_1$ and $\dots$ and $s_n$", implicitly meaning that
%% at least one order allowing to merge all those polyhedra while staying exact exists.
%% Also, we will say that "we consider the source $S$" for "we consider merging the ECH of source
%% $S_1$ and the ECH of source $S_2$ for some $S_1$ and $S_2$ such that $S_1\oplus S_2=S$,
%% $S_1\not=\emptyset$, $S_2\not=\emptyset$ and $S_1$ and $S_2$ are ECHs".
%% Exactly which polyhedra are in $S_1$ or in $S_2$ is not relevant since the
%% result will be the same, either the same ECH or a failure.

In the hullification algorithm, the number of merge attemps increases
dramatically depending on the number of elements added in the
$generatorSet$ at each iteration. With hullification as is, in many
cases, elements of this set can be redundant, in the sense that the
new hulls derived from them, if any, would be the same even though the
elements are different. The key idea to reducing redundancy is to
keep a link between any ECH calculated and the source polyhedra merged
to create it, thereafter called the ECH source, and use this
information to skip redundant ECH calculation attempts.

Consider for example Figure~\ref{fig:sqr}.  If we already tried to
merge the ECH of source $\{s_1,s_2,s_3\}$ with the one of source
$\{s_4,s_5\}$ then it is not necessary to consider trying to merge say
the ECH of source $\{s_3,s_4\}$ with the one of source
$\{s_1,s_2,s_5\}$.  The result would be the same, {\em i.e.} the same
ECH or a failure to merge the convex hulls exactly (the same ECH here).
Note that since
we are generating all the existing ECHs from the source polyhedra,
this case happens every time an ECH can be calculated by merging its
source in strictly more than one order, that is to say \textbf{very}
often. More generally, we do not want to attempt merges of different
hulls deriving from the same set of source polyhedra.

\begin{wrapfigure}{R}{.45\textwidth}
  \vspace{-10pt}
  \begin{center}
  \begin{subfigure}[b]{.2\textwidth}
    \begin{center}
      \begin{tikzpicture}[scale=.8]
        % Lines.
        \draw (0,0) -- (2,0);
        \draw (0,0) -- (0,2);
        \draw (2,0) -- (2,2);
        \draw (0,2) -- (2,2);
        \draw (0,0) -- (2,2);
        \draw (0,2) -- (2,0);
        \draw (1,0) -- (1,1);
        % Area names.
        \draw (0.75,0.4) node {$s_1$};
        \draw (1.25,0.4) node {$s_2$};
        \draw (1.75,1)   node {$s_3$};
        \draw (1,1.75)   node {$s_4$};
        \draw (0.25,1)   node {$s_5$};
      \end{tikzpicture}
    \end{center}
    \caption{Square example}\label{fig:sqr}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{.2\textwidth}
    \begin{center}
      \begin{tikzpicture}[scale=.8]
        % Lines.
        \draw (0,0) -- (2,2);
        \draw (0,0) -- (4,0);
        \draw (2,0) -- (2,1);
        \draw (2,2) -- (4,0);
        \draw (0,1) -- (4,1);
        \draw (0,2) -- (4,2);
        \draw (0,2) -- (0,1);
        \draw (4,2) -- (4,1);
        % Area names.
        \draw (0.75,1.5) node {$s_1$};
        \draw (3.25,1.5) node {$s_2$};
        \draw (2,1.4)    node {$s_3$};
        \draw (1.25,0.5) node {$s_4$};
        \draw (2.75,0.5) node {$s_5$};
      \end{tikzpicture}
    \end{center}
    \caption{Hat example}\label{fig:hat}
  \end{subfigure}
  \vspace{-10pt}
  \end{center}
  \caption{Hullification redundancy issues}
  \label{fig:polys}
  \vspace{-8pt}
\end{wrapfigure}

Another source of redundancy is that, when a seed is added to a pivot during the $generatorSet$ update,
it represents a potential merge of the union of the pivot source and the seed source.
Even if this merge has not yet been considered, a potential merge of the same source might
have already been added to the $generatorSet$ through a different seed added to a different
pivot. In this case we do not want the seed to be added.
So, in order to prevent redundant elements from being added to the $generatorSet$,
we introduce a memory called $generatorSetMemory$, and
control how new hulls are added to the $newGeneratorSet$.
For a new hull to be added to a pivot as a seed,
$source(pivot)\cup source(hull)\not\in generatorSetMemory$ must hold
(Algorithm~\ref{alg:update} line~$4$); if the hull is indeed added to
the seeds of the pivot, then $generatorSetMemory += source(pivot)\cup source(hull)$
(Algorithm~\ref{alg:update} line~$8$).  Informally, this memory contains the
sources of all the potential merges added to the generator set. This
ensures that the merge of a source will never be considered more than
once, and that
the merges we did not consider were not reachable by successive pair-wise ECH calculation.

Also, we forbid adding a seed to a pivot's seeds if the source of
the latter is a subset of the former, since the result would
necessarily be
the seed itself (we call this $(1)$).
Another improvement deals with \emph{when} hullification interacts with the the rest
of the framework. Since our goal is to generate potential invariants,
we do not need to wait for the hullification algorithm to terminate to
communicate the potential invariants already found so far.  They are
therefore communicated, typically to $k$-induction, after each {\em
  big iteration} of the algorithm (loop on Algorithm~\ref{alg:hull}
line~$25$).  This has the added benefit of launching $k$-induction on
smaller potential invariant sets.  

There is a drawback in comparing hulls using their sources:
assume that two of the input (source) polyhedra $p_i$ and $p_j$ are such that
$p_i\Rightarrow p_j$.
Then the exact merge of $p_i$ and $p_j$ succeeds and yields the hull of source
$\{p_i,p_j\}$, which is really $p_j$.
As a consequence, the pivots of the $generatorSet$ are redundant, as are their seeds
and in the end the merge attempts.
To avoid this, we first check the set of input polyhedra and discard redundant ones.

Last but not least, merges are also memorized in between calls to the
algorithm so that we do not call the merge algorithm when considering two
polyhedra we already merged during a previous call. Since
hullification is called on the ever-growing disjunction of all
preimages found so far, each new disjunction contains the previous
one and this represents a significant improvement.

In the next subsection we illustrate hullification on a small example before introducing
another potential invariant generation algorithm in Section~\ref{hullqe:ich}.
Hullification will be illustrated on a reconfiguration logic system in Section~\ref{applications:reconf}

\subsection{Hullification Example}
\label{hullqe:example}

Let us now unroll the algorithm on a simple example depicted on Figure~\ref{fig:hat}.
For the sake of concision a source $\{s_1,s_2,\cdots,s_n\}$ will be written $12\cdots n$.
We write generator sets in the following fashion: $\{(pivot,[seeds])\}$.\\
With this convention, the initial $generatorSet$ is $\{(1,[2,3,4,5]),(2,[3,4,5]),(3,[4,5]),(4,[5]),(5,[])\}$.
The $newGeneratorSet$ for the first {\em big step} iteration trace is as follows:

\begin{center}
\begin{tabular}{l l l l l l l l}
  $1,[]$ & $2,[]$ & $3,[]$ & $4,[]$ & $5,[]$ & & & \\\hline
  $1,[]$ & $2,[13]$ & $3,[]$ & $4,[13]$ & $5,[13]$ & $13,[]$ & & \\\hline
  $1,[]$ & $2,[13]$ & $3,[]$ & $4,[13,23]$ & $5,[13,23]$ & $13,[]$ & $23,[]$ & \\\hline
  $1,[45]$ & $2,[13,45]$ & $3,[45]$ & $4,[13,23]$ & $5,[13,23]$ & $13,[45]$ & $23,[45]$ & $45,[]$ \\
\end{tabular}
\end{center}

At first $newGeneratorSet$ is the same as $generatorSet$ without seeds (first line of the trace).
We first consider $1$ as a pivot. The merge of $1$ and $3$ works while the other ones fail, leading to the
second line of the trace.
Note that $13$ is not added to $1$ nor $3$ since $1\subseteq 13$ and $3\subseteq 13$ by $(1)$.
With this pivot we add three sources to the $generatorSetMemory$: $213$, $413$ and $513$ $(2)$.
The next pivot is $2$ which is merged with $3$ while the merges with the other
seeds fail.
After the $newGeneratorSet$ update we obtain the third line of the trace.
Note that $23$ is not added to the seeds of $1$ since source $213$ has already been added to the
$generatorSetMemory$ at $(2)$ so $23\cup 1\in generatorSetMemory$.
Similarily, it is not added to the seeds of $13$ either.
Next pivot $3$ cannot be merged with any of its seeds.
Pivot $4$ can be merged with $5$ producing the fourth line of the generator trace.
A new big step iteration begins during which $2$ will be merged with $13$ and $3$ with $45$
while all the other merges will fail. At the beginning of the third big step iteration the
$generatorSet$ is \[\{(1,[345]),(2,[345]),(3,[]),(4,[123]),(5,[123]),\]\[(13,[345]),(23,[345]),(45,[123]),(123,[345]),(345,[])\}.\]
No new hull is found and the algorithm detects that a fixed point has been reached.

%% To illustrate this, assume we discovered a new ECH of source $\{s_1,s_2,s_3\}$ --see
%% Figure~\ref{fig:hat} for visual support.
%% Assume also that the source $\{s_1,s_2,s_3,s_4,s_5\}$ has not yet been considered
%% ({\em i.e.} is not in the memory set),
%% but that the ECHs of source $\{s_4,s_5\}$ and $\{s_3,s_4,s_5\}$ have been previously
%% found, {\em i.e.} are in the generator set.
%% In that case we want to avoid considering merging both $\{s_1,s_2,s_3\}$ with $\{s_4,s_5\}$
%% and $\{s_1,s_2,s_3\}$ with $\{s_3,s_4,s_5\}$ since the result would be the same, either
%% failure or the same ECH --failure in the case of Figure~\ref{fig:hat}.
%% Checking if the intersection of the source of the newly discovered ECH and the source
%% of a pivot is empty before updating the pivot seeds is not satisfactory.
%% Indeed, suppose now --this is not the case on Figure~\ref{fig:hat}-- that $\{s_4\}$ and
%% $\{s_5\}$ cannot be merged exactly, but that $\{s_3,s_4\}$ and $\{s_5\}$ can.
%% As in the previous example, we discovered a new ECH of source $\{s_1,s_2,s_3\}$,
%% and the source $\{s_1,s_2,s_3,s_4,s_5\}$ has not yet been considered.
%% Then {\em a priori} the algorithm should consider $\{s_1,s_2,s_3\}$ and
%% $\{s_3,s_4,s_5\}$ during the next iteration, since $\{s_4,s_5\}$ can not be
%% in the generator set (it is not exact).
%% The intersection check previously mentioned would forbid it.
%% On the other hand, it might be the case that the ECH $\{s_1,s_2,s_3,s_4,s_5\}$ --assuming it
%% exists at all-- can be constructed by merging $\{s_1,s_2,s_3\}$ with $\{s_i\}$ and
%% $\{s_j\}$ where $(i,j)\in\{(4,5),(5,4)\}$ successively, but again it might not be.

%% Augmenting the generator set in such a way that no redundant merge can be considered
%% at any point asks for topological considerations that elude
%% the scope of this paper, if possible at all.
%% Until such research is conducted in this particular context, we propose to use the
%% memory set as follows:
%% when considering two hulls of sources $S_1$ and $S_2$, the algorithm first
%% checks if the memory set contains their union.
%% If it does then by uniqueness of the convex hull either it exists and it already is
%% in our generator set, or it does not.
%% In both cases actually performing the ECH calculation is a waste of time.
%% This solution is not optimal since it can generate redundant pivot/seed combinations
%% but avoids the expensive calls entailed by ECH calculation and the generation of
%% redundant hulls which could raise significantly the algorithm average complexity.
%% It is also very rewarding to start the hullification on the results of the previous
%% one augmented by the new polyhedra from the new preimage.
%% It prevents from recalculating all the hulls that have already been found and allows
%% to use the memory set from one hullification to the other.

\subsection{Another Way to Generate Potential Invariants: ICHs}
\label{hullqe:ich}

As mentioned before in Section~\ref{hullqe:algorithm}, ECH calculation cannot do much
for real state variables.
We therefore propose a second approach based on Inexact Convex Hull (ICH) calculation
modulo intersection as mentioned in Section~\ref{approach:duplex},
simply called ICH calculation in the rest of this paper.
That is, two polyhedra will be inexactly merged if and only if their intersection
is not empty.
This regroups areas of the gray state space that are not disjoint and over-approximates them
to make new numerical relations appear.
An efficient way to check for intersection is to check the satisfiability of the
conjunction of the constraints describing the two polyhedra using an SMT solver.
Note that this technique is also of interest in the integer case.

For a given set of polyhedra more ICHs than ECHs can be created, in practice often
a lot more. The hullification algorithm using ICHs thus tends to choke.
We propose the following algorithm, only briefly described for the sake of concision.\\
Select a pivot in the input polyhedra set and try to find an ICH with the other ones.
If an ICH with another polyhedra (source) exists, both the pivot and the source are
discarded, and the ICH becomes the new pivot.
Once all the merges have been tried, the pivot is put aside and a new pivot is selected
in the remaining polyhedra set.
When the algorithm runs out of polyhedra, it starts again on the polyhedra put aside
if at least one new hull was found.
If not, a fixed point has been reached and the algorithm stops.\\
Although the intermediary ICHs computed in this algorithm depends on the order in which
the pivots are selected and merged with the other polyhedra, its result does not.
Indeed, the fact that two polyhedra have a non-empty intersection will stay true even
if one or both of them are merged with other polyhedra.
This result, as depicted in Section~\ref{approach:duplex}, is an over-approximation
of disjoint areas of the gray state space.

In practice, both the ECH based hullification and the ICH calculation heuristics
run in parallel, and the sets of potential invariants they output are merged
before being sent to the $k$-induction.
This allows us to combine the precision of ECHs with the over-approximation effect
of ICHs.
A high level view of our approach is available on Figure~\ref{fig:global}.
The next section will present two examples taken from a functional chain as
presented in Section~\ref{systems} each illustrating the ideas introduced in this
section: a reconfiguration logic system and a voting logic system.

\begin{figure}
  \begin{centering}
  \scriptsize
  \begin{tikzpicture}[scale=.8,auto,node distance=2cm,>=latex]
    \draw[draw=black,fill=blue!20] (0,2) -- (0,6) -- (10,6) -- (10,2) -- cycle;
    \node (HQ) at (0.7,5.7) {HullQe};
    \node[anchor=west] (Key) at (0.65,1)
    {\begin{tabular}{c c r}
    $F_i(s,s')=$ & $PO(s)\land invs(s)\land T(s,s')\land invs(s')\land\neg PO(s')$ & if $i=1$\\
                 & $PO(s)\land invs(s)\land T(s,s')\land invs(s')\land G_{i-1}(s')$ & if $i>1$\\
    \end{tabular}};
    \draw[draw=black] (1.45,6) -- (1.45,5.4) --(0,5.4);
    \node[draw=black,fill=blue!10] (BQE) at (1,4)  {HullQe};
    \node[draw=black,fill=blue!10] (QE)  at (4,4)  {QE};
    \node[draw=black,fill=blue!10] (ICH) at (5,5.5)  {ICH};
    \node[draw=black,fill=blue!10] (ECH) at (5,2.5)  {ECH};
    \node[draw=black,fill=blue!10,text width=40pt,text centered] (Fla) at (8,4)  {Atom extraction and negation};
    \node[draw=black,fill=blue!10,text width=70pt,text centered] (Kin) at (13,4) {$k$-induction};
    \node[draw=black,fill=blue!10] (Dis) at (13,5.5) {Discarded};
    \node[draw=black,fill=blue!10] (PO)  at (13,2.5) {Contains PO};
    \node[draw=black,fill=blue!10] (Do)  at (15,2.5) {Done};

    \draw[->, >=latex] (QE) -- node[anchor=south] {$G_i$} (BQE);
    \draw[->, >=latex] (BQE.north) ..controls +(up:10mm) and +(up:10mm).. node {$F_i(s,s')$} (QE.north);
    \draw[->, >=latex] (QE) -- node[anchor=west,pos=.1] {$\bigvee G_i$} (ICH);
    \draw[->, >=latex] (QE) -- node[anchor=east] {$\bigvee G_i$} (ECH);
    \draw[->, >=latex] (ICH) -- node {inexact hulls} (Fla);
    \draw[->, >=latex] (ECH) -- node[anchor=west,pos=.01] {exact hulls} (Fla);
    \draw[->, >=latex] (Fla) -- node {$atoms$} (Kin);
    \draw[->, >=latex] (Kin.0) ..controls +(1,0) and +(1,1).. node[anchor=west,text centered] {$U$ (retried on next iteration)} (Kin.45);
    \draw[->, >=latex] (Kin) -- node {$F$} (Dis);
    \draw[->, >=latex] (Kin) -- node[name=v] {$V$} (PO);
    \draw[->, >=latex] (PO) -- (Do);
    \draw[->, >=latex] (v) ..controls +(left:3mm) and +(down:33mm).. node[pos=0.97,anchor=west] {invariants} (BQE.south);
    %\draw[->, >=latex] (Kin.south) ..controls +(down:13mm) and +(down:16mm).. (BQE.south);
  \end{tikzpicture}  \caption{High level sequential description}
  \label{fig:global}
  \end{centering}
\end{figure}
