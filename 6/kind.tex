\label{sec:kind}
The Stuff framework provides an SMT-based $k$-induction
module. Performing a $k$-induction analysis of a potential state invariant $P$
on a transition system $\langle I, T \rangle$ consists in checking the
satisfiability of the $\mathit{Base_k}(I, T, P)$ and
$\mathit{Step_k}(T,P)$ formulas, defined in \eqref{eq:kind}, for
increasing values of $k$, starting from a user specified $k>1$.

\begin{equation}
  \label{eq:kind}
  \begin{split}
    \mathit{Base_k}(I, T, P) & \equiv  
    \overbrace{I(s_0)}^{\text{Initial state}} \land
    \overbrace{\bigwedge_{i \in [0,k-2]}{T(s_i,s_{i+1})}}^{\text{trace of k-1 transitions}} \land
    \overbrace{\bigvee_{i \in [0,k-1]} \lnot P(s_i)}^{\text{$P$ falsified on some state}} \\
    \mathit{Step_k}(T,P) & \equiv
    \underbrace{\bigwedge_{i \in [0,k-1]} T(s_i,s_{i+1})}_{\text{trace of $k$ transitions}} \land
    \underbrace{\bigwedge_{i \in [0,k-1]} P(s_i)}_{\text{$P$ satisfied on first $k$ states}} \land
    \underbrace{\lnot P(s_k)}_{\text{$P$ falsified by last state}}
  \end{split}
\end{equation}

The base and step instances are analysed, until either a base model
has been found, in which case the proof objective is falsified, a user
specified upper bound for $k$ has been reached for base and step, in
which case the status of the proof objective is still undefined, or a
$k$ value has been discovered so that both formulas are unsatisfiable,
which proves the validity of the objective.


In addition, this $k$-induction engine allows, for any $n$, to partition a given set
of proof objectives $P = \{P_j\}$, viewed as a conjunction
$P = \bigwedge_j{P_j}$, in three maximal subsets $F_n$, $U_n$ and $V_n$, such
that: 

\begin{itemize}
\item elements $P \in F_n$ are such that
  $\mathit{Base_n(I,T,P)}$ is satisfiable: they are \emph{Falsified};
\item elements $P \in U_n$ are such that $\mathit{Base_n(I,T,P)}$ is
  unsatisfiable and $\mathit{Step_n(T,P)}$ is satisfiable: they are
  \emph{Undefined} because neither falsifiable nor $n$-inductive;
\item elements of $V_n$ are such that
  $\mathit{Base_n(I,T,\bigwedge_{P \in V_n}{P})}$ is unsatisfiable and
  $\mathit{Step_n(T,\bigwedge_{P \in V_n}{P})}$ is unsatisfiable: they
  are mutually $n$-inductive, \emph{i.e.} \emph{Valid} on the transition
  system.
\end{itemize}
% The partitionning mechanism operates as follows, for any $n$ and any
% given set of proof objectives $P$.  First, the conjunction of elements
% of $P$ is analysed in the base case. As long as $\mathit{Base}_n(I, T,
% \bigwedge_{p \in P}{p})$ is satisfiable, simulation is used to
% identify which conjuncts of $\bigwedge_{p \in P}{p}$ are falsified by
% each model. Falsified conjuncts are removed from $P$ and placed in
% $F_n$ and the base case is analysed again with the remaining
% objectives. If all objectives get falsified while iterating the base
% case at depth $n$, the analysis terminates, returning $F_n$ and empty
% sets for $U_n$ and $V_n$. If the base instance becomes unsatisfiable
% before all proof objectives have been removed from $P$, the analysis
% moves on to the step case with the remaning proof obligations. As long
% as $\mathit{Step_n}(T,\bigwedge_{p \in P}{p}) $ is satisfiable,
% simulation is used to identify which conjuncts of $\bigwedge_{p \in
%   P}{p}$ are falsified by each model. Falsified conjuncts are removed
% from $P$ and placed in $U_n$, and the step analysis is repeated with
% the remaining objectives. If all objectives get falsified while
% iterating the step case, the analysis terminates, returning sets $F_n$
% and $U_n$ and an empty set for $V_n$. If the step instance becomes
% unsatisfiable before all elements of $P$ have been invalidated, the
% remaining objectives are placed in the set $V_n$, and the analysis
% terminates, returning the sets $F_n$, $U_n$ and $V_n$.  To continue
% the analysis at depth $n+1$, $S$ must be initialised with the set
% $U_n$.
