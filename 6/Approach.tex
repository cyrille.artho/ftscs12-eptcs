\label{approach}
Our lemma generation heuristic builds on a backward property-directed
reachability analysis.
We use Quantifier Elimination (QE~\cite{DBLP:conf/lpar/Monniaux08,Nipkow10,bjorner10}) to compute
successive preimages of the negation of the PO, in the spirit
of~\cite{DBLP:conf/cav/MouraRS03,DBLP:conf/atva/DammDHJPPSWW07}. In
our approach, the states characterized by the preimages are generated
in a way such that
\begin{inparaenum}[\itshape (i)]
\item they satisfy the PO and
\item from them, it is possible to reach a state violating the PO if certain transitions are taken.
\end{inparaenum}
Such states will be referred to as \emph{gray states}.
This can be achieved by calculating the preimages as follows:
\begin{equation}
\label{eq:preimage}
\begin{split}
\mathit{preimage_1} &= \mathit{QE}(s',\mathit{PO}(s) \land T(s,s') \land \neg\mathit{PO}(s'))\\
\mathit{preimage_i} &= \mathit{QE}(s',\mathit{PO}(s) \land T(s,s') \land \mathit{preimage_{i-1}}[s'/s]) \quad( \text{for}\;i>1 )
\end{split}
\end{equation}
where $QE(\vec{v},F)$ returns a quantifier-free formula equisatisfiable to $\exists\vec{v},\;F$ and such that $FV(QE(v,F))=FV(F)\setminus v$.
The preimages themselves are assumed to be in DNF, by using
\cite{DBLP:conf/lpar/Monniaux08} as a QE engine for instance.

From these preimages we extract information using two search
heuristics introduced and motivated in the rest of this section and
detailled in Section~\ref{hullqe}. These heuristics run in parallel,
alongside the backward analysis computing the next preimage and a
$k$-induction engine.  The backward analysis is not run to a fixed
point before proceeding further, it is rather meant to probe the gray
state space around the negation of the PO, and feeding the potential lemma generation with the preimages
as soon as they are produced.

To extract information out of the preimages, at any point of the backward exploration, their disjunction is considered: it represents
the gray states found so far as a union of polyhedra. The main idea underlying the potential lemma generation
is to explore the ways in which
those polyhedra can be grouped using convex hull calculation, thus discovering linear relations over state variables
representing boundaries between convex regions of the gray state space.  
Since these convex boundaries enclose unauthorized states, they are negated before
being sent to our $k$-induction engine to check their validity and try to stenghten the PO.

The PO is successfully strengthened by a set of lemmas when the
set $V_k$  of valid POs, produced by the $k$-induction analysis detailled in Section~\ref{sec:kind},
contains the main PO at the end of a run.
If the original PO is not strengthened by the potential invariants extracted from the
currently available preimages, a new preimage is calculated, bringing more information.
Yet, when the PO is strengthened and proved valid, it can be the case that not all elements
of the valid subset $V_k$ are needed to
entail the original PO. A minimization pass inspects them one by one,
discarding $l\not=PO$ from $V_k$ if
$\bigwedge(V\setminus l)$ remains
$k$-inductive, to obtain a relatively small and readable set of lemmas.

Note that, in the backward exploration, the choice of which variables to eliminate by QE and which to keep is important.
Eliminating the next state variables
and keeping the current state variables is not satisfactory in the general case, as on large scale systems, many state variables might not be relevant for the PO under
investigation, and might hinder the performance of the convex hull calculation or $k$-induction.
Therefore, the only state variables that are \textbf{not} eliminated are the ones found
in the cone of influence of the PO, in their {\em current state} version. In particular, the system inputs are eliminated since they do not provide more information
from a backward analysis point of view.

\begin{wrapfigure}{r}{.55\textwidth}
  \vspace{-10pt}
  %\begin{center}
  \begin{subfigure}[b]{.2\textwidth}
    \begin{center}
      \scriptsize
      \begin{tikzpicture}[scale=.5]
        % Axes.
        \draw [->] (0,0) -- coordinate (x axis mid) (4.5,0) node[right] {$x$};
        \draw [->] (0,0) -- coordinate (y axis mid) (0,3.5) node[left] {$y$};
        % Ticks.
        \foreach \x in {0,...,4}
        \draw (\x,1pt) -- (\x,-3pt)
        node[anchor=north] {\x};
        \foreach \y in {0,...,3}
        \draw (1pt,\y) -- (-3pt,\y) 
        node[anchor=east] {\y};
        % Lines.
        \draw (0,0) -- (1,1);
        \draw (0,0) -- (1,0);
        \draw (1,0) -- (1,1);
        \draw (2,0) -- (2,1);
        \draw (2,1) -- (3,1);
        \draw (3,1) -- (3,0);
        \draw (2,0) -- (3,0);
        \draw (3,2) -- (3,3);
        \filldraw[black] (0,0) circle (2pt);
        \filldraw[black] (1,0) circle (2pt);
        \filldraw[black] (2,0) circle (2pt);
        \filldraw[black] (3,0) circle (2pt);
        \filldraw[black] (4,0) circle (2pt);
        \filldraw[black] (1,1) circle (2pt);
        \filldraw[black] (2,1) circle (2pt);
        \filldraw[black] (3,1) circle (2pt);
        \filldraw[black] (2,2) circle (2pt);
        \filldraw[black] (3,2) circle (2pt);
        \filldraw[black] (3,3) circle (2pt);
        % New borders.
        \draw[dashed] (1,1) -- (3,3);
        \draw[dashed] (3,3) -- (4,0);
        \draw[dashed] (1,0) -- (2,0);
        \draw[dashed] (3,0) -- (4,0);
        \draw[dotted] (3,1) -- (4,0);
        % Polyhedra names.
        \draw (0.75,0.37) node {$s_1$};
        \draw (2.5,0.5)   node {$s_2$};
        \draw (1.5,2.25)  node {$s_3$};
        \draw (3.5,3.25)  node {$s_4$};
        \draw (4.5,0.25)  node {$s_5$};
      \end{tikzpicture}
    \end{center}
    \caption{ECH on integers}\label{fig:echInt}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{.3\textwidth}
    \begin{center}
      \scriptsize
      \begin{tikzpicture}[scale=.8]
        % Axes.
        \draw [->] (0,0) -- coordinate (x axis mid) (4.5,0) node[right] {$x$};
        \draw [->] (0,0) -- coordinate (y axis mid) (0,2.5) node[left] {$y$};
        % Ticks.
        \foreach \x in {0,...,4}
        \draw (\x,1pt) -- (\x,-3pt)
        node[anchor=north] {\x};
        \foreach \y in {0,...,2}
        \draw (1pt,\y) -- (-3pt,\y) 
        node[anchor=east] {\y};
        % Lines.
        \draw (0,0) -- (0,2);
        \draw (0,0) -- (3.85,0);
        \draw (0,2) -- (1.85,2);
        \draw (3.85,0) -- (1.85,2);
        \draw (2,2) -- (4,0);
        % Area names.
        \draw (1,1)   node {$s_1$};
        \draw (3.5,1) node {$s_2$};
      \end{tikzpicture}
    \end{center}
    \caption{ECH on Reals}\label{fig:echReal}
  \end{subfigure}
  %\end{center}
  %\vspace{-10pt}
  \caption{New relations with hulls}
  \label{fig:polys}
  \vspace{-10pt}
\end{wrapfigure}

Before going into the details of the potential lemma generation
algorithm, let us illustrate how computing ICHs and ECHs can actually
make new numerical relations appear, using the
examples given in Figure~\ref{fig:echInt} and
Figure~\ref{fig:echReal}.

In Figure~\ref{fig:echInt}, the gray state space of a system with two integer state variables is represented.
States are represented as dots, polyhedron $s_1$ contains three
states, polyhedra $s_3$ and $s_5$ only contain one state {\em etc}.

Computing exact convex hulls over these base polyhedra in the LIA fragment yields (at least) two new borders,
{\em i.e.} potential relational invariants, pictured as dashed lines.
An example of merging order is to merge $s_1$ with $s_2$, $s_3$ with $s_4$, $\{s_1,s_2\}$
with $\{s_3,s_4\}$, and $\{s_1,s_2,s_3,s_4\}$ with $s_5$ $(1)$.

On a system with real valued state variables however, as shown in Figure~\ref{fig:echReal}, the only case in which we will {\em discover} a new border
by computing exact convex hulls is when one is the limit of another, as illustrated on Figure~\ref{fig:echReal}.
Here $s_1$ is made of $0\leq x$, $0\leq y\leq2$ and $y+x-4<0$;
$s_2$ is made of $0\leq y\leq2$ and $y+x-4=0$, so the resulting hull will be
$0\leq x$, $0\leq y\leq2$ and $y+x-4\leq0$. The information learned this way has little chance
of strengthening the PO.

As will be seen in the next sections, when trying to discover new relations, ECH-based techniques work best
for integer valued systems, while ICH can be beneficial for both real or integer valued systems.

\subsection{A First Example}
\label{approach:counter}

\begin{wrapfigure}{r}{.5\textwidth}
  \vspace{-20pt}
  \scriptsize
  \begin{center}
    \begin{tikzpicture}[scale=.5]
      % Axes.
      \draw [->] (0,0) -- coordinate (x axis mid) (11,0) node[right] {$x$};
      \draw [->] (0,0) -- coordinate (y axis mid) (0,7) node[left] {$y$};
      % Ticks.
      \foreach \x in {0,...,10}
      \draw (\x,1pt) -- (\x,-3pt)
      node[anchor=north] {\x};
      \foreach \y in {0,...,6}
      \draw (1pt,\y) -- (-3pt,\y) 
      node[anchor=east] {\y};
      % Hullification.
      %\draw {(8,0) -- (9,0) -- (9,5) -- (8,4)} [fill = gray,gray];
      % Bounds.
      \draw (0,6) -- (10,6);
      \draw (10,0) -- (10,6);
      % Lemma.
      \draw [thick] (3.5,-0.5) -- (10.5,6.5);
      % Preimage one.
      \draw [dashed] (9,0) -- (9,5);
      \draw (9.6,2) node {$(1)$};
      % Preimage two.
      \draw [dashed] (8,0) -- (8,4);
      \draw (7.4,2) node {$(2)$};
    \end{tikzpicture}
  \end{center}
  \vspace{-10pt}
  \caption{ECH calculation on the double counter}
  \label{fig:dblc}
%  \vspace{-10pt}
\end{wrapfigure}

We consider a simple example called the double counter\footnote{Code available
at~\href{http://www.onera.fr/staff-en/adrien-champion/}{http://www.onera.fr/staff-en/adrien-champion/}.} with two integer state
variables $x$ and $y$ and three boolean inputs $a$, $b$ and $c$. Variables $x$ and $y$ are
initialized to $0$, and are both incremented by one when $a$ is true or keep their
current value when $a$ is false. The variable $x$ is reset if $b\lor c$ is true,
and saturates at $n_x$. The variable $y$ is reset when $c$ is true and saturates at $n_y$,
hence $y$ cannot be reset without resetting $x$, and $n_x>n_y$.
The proof objective is $x=n_x\Rightarrow y=n_y$. 
Here is a possible transition relation for such a system:
\begin{center}
\small
\begin{tabular}{r c l l l l}
  $ T(s,s') =$ &         & $\big(\text{if}\;(b\lor c)$ & $\text{then}\;x'=0\;\text{else if}\;(a\land x<n_x)$ & $\text{then}\;x'=x+1$ & $\text{else}\;x'=x\big)$\\
               & $\land$ & $\big(\text{if}\;(c)$ & $\text{then}\;y'=0\;\text{else if}\;(a\land y<n_y)$ & $\text{then}\;y'=y+1$ & $\text{else}\;y'=y\big)$.\\
\end{tabular}
\end{center}

Let us see now how the proposed approach performs on this system
when fixing $n_x = 10$ and $n_y = 6$ for instance.
First, using the abstract interpretation tool presented in~\cite{DBLP:journals/entcs/RouxDG10},
bounds on $x$ and $y$ are easily discovered: $0\leq x\leq n_x = 10$ and
$0\leq y\leq n_y = 6$, yet the PO cannot be proved with AI without further manual intervention.
So, using these range properties once $k$-induction has confirmed them, we start the
backward property-directed analysis, which outputs a first preimage:
$x = 9\;\land\;0 \leq y < 5\;(1)$.
Unsurprisingly, it is too weak to conclude, {\em i.e.} its negation is not $k$-inductive for a small $k$.
The next preimage is $x = 8\;\land\;0 \leq y < 4\;\lor x=9\;\land\;0\leq y<5\;(2)$ which
does not allow to conclude either for the same reason.
Instead of iterating until a fixed point is found, consider the graph on Figure~\ref{fig:dblc}.
It shows the two first preimages as dashed lines which seem to suggest a relation between $x$ and
$y$, pictured as a bold line.
This relation can be made explicit by calculating the convex hull of the disjunction of the first
two preimages -- since this particular system can stutter, it is the same as $(2)$.
This yields $8 \leq x \leq 9 \;\land\; 0 \leq y < x - 4$.
Note that this convex hull is an ECH, since both $x$ and $y$ are integers.
The four inequalities are negated -- they characterize gray states --
and are sent to the $k$-induction engine.
Potential invariants $\neg 8\leq x$, $\neg x\leq 9$ and $\neg 0 \leq y$ are falsified,
and the PO in conjunction with lemma $\neg y < x-4$ is found to be 1-inductive.

In fact, this PO could also be proved correct by $k$-induction given the bounds found by AI only,
by unrolling the transition relation to the reoccurrence diameter of the system.
In practice, even on such a simple system it is not possible for large
values of $n_x$ and $n_y$ (hundreds or thousands of transitions).
The performance of our technique on the other hand is not sensitive to the actual value of numerical constants: it will always
derive the strengthening lemma from the first two preimages.
Obviously, the time needed to compute the preimages is not impacted by changing the constants values either.\\
For more complex systems with preimages made of more than two polyhedra, simply merging
them in arbitrary order using convex hull calculation is not robust since
the resulting convex hulls would depend on the merging order, and
interesting polyhedra could be missed.
This idea of an exhaustive enumeration of the intermediary ECHs that can appear
when merging a set of polyhedra is explored in Section~\ref{hullqe:algorithm}.

\subsection{A Second Example}
\label{approach:duplex}

Let us now consider briefly a two input, real valued voting logic system
derived from the Rockwell Collins triplex voter.
We will not discuss the system itself since the triplex
voter is detailled in Section~\ref{applications:voter}. It simply allows us to represent
graphically the state space in a plan.
The PO here is that two of the state variables, $Equalization_1$ and $Equalization_2$, range
between $-0.4$ and $0.4$.
Figure~\ref{fig:duplex} depicts the corresponding square.
On Figure~\ref{fig:duplex1} we can see the first preimage calculated by our backward reachability
analysis as black triangles, and the strengthening lemmas found by hand in~\cite{dierkes11} transposed
to the two input system as a gray octagon.
Calculating ECH on this first preimage does not allow to conclude.

\begin{figure}[t]
  %\vspace{-10pt}
  \begin{center}
  \begin{subfigure}[b]{.4\textwidth}
    \begin{center}
      \includegraphics[width=\textwidth]{duplex_2}
    \end{center}
    \vspace{-20pt}
    \caption{Two inputs voter, first preimage}\label{fig:duplex1}
  \end{subfigure}
  \begin{subfigure}[b]{.4\textwidth}
    \begin{center}
      \includegraphics[width=\textwidth]{duplex_3}
    \end{center}
    \vspace{-20pt}
    \caption{Two inputs voter with ICH}\label{fig:duplex2}
  \end{subfigure}
  \end{center}
  \vspace{-10pt}
  \caption{Simple voting logic.}
  \label{fig:duplex}
  \vspace{-10pt}
\end{figure}

A more relevant approach would be to calculate ICH. Yet, since the ICH of all the preimage polyhedra
is the $[-0.4,0.4]^2$ square, we need to be more
subtle and introduce a criterion for ICH to be actually computed between two polyhedra: they have
to intersect. Intersection can be checked by a simple satisfiability test performed using a SMT solver.
This check allows us to identify overlapping areas of the gray state space and to over-approximate them, while not merging disjoint areas in the gray state space explored so far.
This approximation obtained through ICH resembles widening techniques used in abstract
interpretation~\cite{DBLP:conf/popl/CousotC77} in the sense that it allows to {\em jump} forward in the
analysis iterations, yet it differs in the sense that, contrary to widening, it does not ensure
termination. The only goal here is to generate potential invariants for the PO, and Figure~\ref{fig:duplex} shows that the
ICH yields exactly the dual, in the $[-0.4,0.4]^2$ square, of the octagon invariant found by hand
in~\cite{dierkes11}. %only serve the explanation and are \textbf{not} used anywhere in the proof.
This second idea of using ICHs to perform overapproximations will be discussed in Section~\ref{hullqe:ich}.
