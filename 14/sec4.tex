




\section{Case Study: Driver Fatigue}\label{casestudy:sec}

Enhancing the driver experience through increasing autonomy has been of interest for well over a decade now. Reduction in drive stress, freeing up limited attentional resources and improving road safety have been the major goals of this effort. However, autonomy brings with it a variety of other challenges that potentially risk road safety \cite{StM96}. This could be due to sensor limitations, system design faults, error inducing design, or inadequate driver training; these certainly are some of the lessons learned from the introduction of autonomy in the aviation domain.

Of interest here is driver fatigue, which results in drowsiness and hypovigilance, particularly after prolonged periods of driving and monotonous roadside experience through motorways \cite{ThB03}. This has been confirmed by a number of studies \cite{Mil97,LDDW98}. This is a major cause of accidents across the world, with around 10-35\% of all road accidents in the USA and several European countries estimated to be fatigue and sleep related~\cite{RoSPA01}. While manual strategies adopted by drivers to cope with this problem are recognised \cite{GSO11}, fatigue serves to be one of the main factors for increasing autonomy in vehicles \cite{OpS11}. 

%Our specific case study has been derived from the requirements from a military scenario combined with empirical models from the literature. The are in the context of fatigue and issues such as hazard or target identification mainly involving UGVs and UAVs. The control models of the UGVs and UAVs are similar and have many characteristics  of ACC, especially when there are many units involved in the operation. We have used  different behaviour models and safety properties have been studied. Here we present the details of one such case study that illustrates our approach.

We view each  vehicle as a safety critical system
where one should avoid accidents as well as complete the mission. 
Even if the vehicle is unoccupied, a human will be involved in its control.
The vehicles are travelling in a convoy and it is important to maintain
safe stopping distance. Otherwise, unexpected environment
events (like explosions) can cause the driver or human controller to accelerate
and run into the vehicle in front of the convoy.
The case study studies the effect of
driver ability and input modes on the desired vehicle
separation  for safety given a specific route.

The example we present here 
focusses on using different actual empirical results
that are incorporated into our behavioural
models. For instance, the parameters for
hazard perception are chosen from \cite{CMC10,Che10}.
Similarly, the parameters for the control mode are chosen
from \cite{BMP11,CMC10}
while the
desired separation and safe stopping distance  are chosen from \cite{CMC10}.
The function that calculates the reaction time from
factors such as fatigue is derived from \cite{GrM11,BMP11,CBM12}.
Other factors such as speed and route are generated (either manually or
non-deterministically) as part of the scenario exploration experiment.

The use of sub-models requires
us to experiment with hypothetical integration to achieve
a single behavioural model.
We have to consider hypothetical integration
due to lack of suitable models that cover all aspects covered
in the sub-models.  This separation
of actual models and hypothetical integration
documents the assumptions under which our analysis is valid. It also points
to areas where more precise cognitive models are required.

\subsection{Structure of the ACC System Model}
\label{system-model-casestudy:sec}

The system model has the key aspects of the ACC mainly the control
system to maintain speed of the various vehicles, the
separation between them and a notion of safe stopping distance to prevent
accidents.
The environment model has the terrain and hazards that are non-deterministically
generated.
The behaviour model we consider is that of fatigue which is
calculated from the duration the driver has been in the vehicle and
the complexity of the terrain.
The driver's cognitive ability is influenced by fatigue and hazard perception.
This
when combined with the mode to issue the commands determines the reaction
time. The current speed of the vehicle and the reaction time of the driver
will determine the safe stopping distance.

Adaptive cruise control (ACC) is one of the mechanisms introduced to provide safe distance control from lead vehicle in front: once engaged, the vehicle operates in a typical cruise controlled fashion with the added feature of sensing the vehicle in front to adapt speed (if it slows down or speeds up) ensuring a minimal safe distance at all times. Figure~\ref{accalgorithm:fig} below shows an algorithm for a simple implementation of ACC. 
%
\begin{figure}[htb]
\begin{verbatim}
void function ACC(dist_veh_in_front)
{ 
   loop{
        input current_dist_veh_in_front; 
        IF dist_veh_in_front == current_dist_veh_in_front
         CALL Maintain_host_veh_speed;
        END IF
        IF dist_veh_in_front > current_dist_veh_in_front
         CALL Decelerate_host_veh_speed;
        END IF
        IF dist_veh_in_front < current_dist_veh_in_front
         CALL Accelerate_host_veh_speed;
        END IF
   }
}
\end{verbatim}
\caption{Adaptive Cruise Control (ACC) algorithm in pseudocode}
\label{accalgorithm:fig}
\end{figure}
%
The \verb|ACC| algorithm uses the vehicle's on-board sensors to read in the gap from the vehicle in front. This is preset by the driver and passed onto the algorithm as a parameter. Once it enters the loop, it strives to maintain this distance by continuing to maintain the vehicle speed if the gap to the vehicle in front is the same, decelerating if the distance gets narrower than desired or accelerating if the gap is wider.  

Studies have demonstrated that ACC has the potential of causing delayed driver reaction~\cite{VSG11}, and awkward handover and mode confusion with up to a third of drivers having forgotten at some stage whether ACC was engaged or otherwise~\cite{Lar12}. This has serious road safety risks and raises a question whether the design of such mechanisms would ultimately serve to be detrimental to the intended goal. In addition to the time on task effects, road conditions and terrain also significantly affect driver experience, and contribute to fatigue \cite{OrR07};
difficult terrains require more frequent driver interventions \cite{SAY04} in semi-autonomous vehicles.

The above points to a clear need to analyse the relationship between driver control and vehicles offering semi-autonomous features such as ACC. The case study proposed to demonstrate the framework presented in this paper revolves around how driver behaviour and perception is affected
by fatigue which itself is influenced by factors such as
journey time. The behaviour model is influenced by parameters including over time on task, fatigue and perception of ACC status. The system model is parameterised to represent a vehicle equipped with ACC, ranging over speed, acceleration, distance to vehicle in front, and ACC status.

\subsection{Structure of Behaviour Models}
\label{behaviour-model-casestudy:sec}

The sub-models associated with the behaviour model are now described.
The fatigue is calculated using the variables that are
identified by  Oppenheim and Shinar \cite{OpS11}. 
Although they do not present empirical results they summarise
results from other papers.   These results are not specific to the
military situation. They are more from generic  driving conditions. However,
as  that is the best data available we use it. 
Similarly, the role of terrain is identified as an important factor
\cite{CBM12,RPI08}. The role of terrain is explained only in qualitative
terms and neither present any concrete model of how the terrain actually affects
fatigue.
Therefore, there is no obvious way to create a unified
behaviour model from the empirical results.


Thus we explicitly define an integrator function that will combine the variables
and values indicated in prior work \cite{OpS11,CBM12,RPI08}.
As the behaviour of this function is not available,
we encode various  candidate functions. 
All candidate functions are automatically evaluated in the verification
process. Technically,
this is achieved using non-deterministic choice in CBMC.
 So
if our analysis indicates that safe stopping distance is
always maintained, we can conclude that safety is independent of
the candidate functions.


The reaction
time calculation is from Baber et al. \cite{BMP11}.
The reaction time depends on the mode of communication
between the driver and the vehicle control system as well as the derived
value for the fatigue factor.
Baber et al. \cite{BMP11}  present empirical results
for mode of communication and reaction time. 
Here again we rely on a non-deterministic choice
of integrator functions to combine the model from Baber et al.
\cite{BMP11} and the calculated fatigue value.
For the case study the interaction mode is a factor where research
shows that in certain circumstances the use of speech is better than using text
while the use of joystick or gamepad to control the system is often
better than issuing text based commands.

For instance, the empirical results from Baber et al. \cite{BMP11} are encoded
to calculate the reaction time is shown in
Figure~\ref{encodingHumanFactors:fig}.
They function is  \verb|setReactionTime| which calculates a qualitative
reaction time based on the input mode and then invokes
\verb|changeReactionTime| with the fatigue level to calculate a quantitative
value.

\begin{figure}[htb]
\begin{verbatim}
void setReactionTime(mode iM, fatigueLevel df)
/* mode is the input mode used by the driver and
   fatigueLevel is a indication of the driver's  tiredness
   we use symbolic values rather than concrete values for the reactionTime
   
*/
{
  switch (iM) {
    case GamePad: reactionTime = okay;
    case Speech: reactionTime = slow;
    case MultiModal: reactionTime = fast;
  };
  changeReactionTime(df);
}

void changeReactionTime(fatigueLevel df)
/* this function alters the reaction time based on the
   level of tiredness. These factors can either be chosen from
   empirical results  or can be  set non-deterministically
*/
{
   switch (df) {
     case Exhausted : reactionTime = reactionTime * eFactor;
     case Tired     : reactionTime = reactionTime * tFactor;
     case Normal    : reactionTime = reactionTime * nFactor;
    };
}
\end{verbatim}
\caption{Encoding of Human Factors.}
\label{encodingHumanFactors:fig}
\end{figure}




In general we represent the parameters in the various models as
global variables and represent the actual calculation
of the the values as functions. For instance,
\verb|timeDriven| and \verb|terrain| are global integer values.
As there are discrete levels of fatigue
we use enums to represent the possible values
the global variable \verb|driverFatigue| can take.
The function \verb|setDriverFatigue| assigns a value to the variable
\verb|driverFatigue| based on the behaviour model.

The hazard perception is based directly on Chen \cite{Che10}. As we are using
only a single model there is no need to define an integrator for hazard
perception.
This ability includes the size of the hazard.
In general larger the hazard the easier it is to perceive.
We use the variable \verb|hazardPerception| to denote the
user's ability to perceive hazards. This can be derived
from \verb|driverFatigue|. It is also possible
for  one to use CBMC's non-determinism to generate a value.
An example is  the following statement
\begin{quote}
\verb|hazardPerception = hpFunction(nondet_int());|
\end{quote}
where the function
\verb|hpFunction| converts an integer
to a suitable hazard perception value. 
The function \verb|hpFunction| is a part of $f_{BI}$ in the formal model.


\begin{figure}[htb]
\begin{verbatim}
v1 = model1(p1); /* p1 represents the parameters used by
                     the first model */
v2 = model2(p2); /* p2 represents the parameters used by
                     the second model */
opChoice = nondet_uint() % choices;
return choiceFunction(opChoice,v1,v2);
\end{verbatim}
\caption{Non-Deterministic Integrator Function}
\label{nondetI:fig}
\end{figure}

The structure of a non-deterministic integrator function is
shown in Figure~\ref{nondetI:fig}.
The variable
\verb|opChoice| represents the function to combine the values from the
different models and its value is calculated non-deterministically. 
The actual integration is performed by the
function \verb|choiceFunction| which applies the chosen function to
the values from the sub-models to return the actual value for the
joint model and
represents a specific instance of $f_{BS}$ (i.e., the function that calculates the state associated with the user's behaviour).

\subsection{Structure of Environmental Models} 
\label{environment-model-casestudy:sec}

The environment model allows for variability in scenario to help analysis,
including road conditions, terrain or obstacles
that result in manual driver interventions) and journey time.
The travel scenario is expressed as a sequence (i.e., an array)
of  route points where each
route point has the relevant information (and represented as a struct).
A simple example of the structure of the route is shown in
Figure~\ref{route:fig}.

\begin{figure}[htb]
\begin{verbatim}
struct routePoints{
int obstacle; /* represents difficulty to overcome */
int distance; /* time to travel is calculated using the speed */
int terrain; /* represents offRoad, onRoad */
int curvature;
};

struct routePoints route[5];  /* there are 5 sections to travel */
\end{verbatim}
\caption{Example of Route.}
\label{route:fig}
\end{figure}




%%In our specific case studies we do not consider issues such as driving
%%conditions and visibility.

The cause and effect relations, informed by the literature above, are explored to analyse for relative safety thresholds that arise from the interaction of the three models. Potential safety violations in terms of dangerous levels of fatigue and increased likelihood of mode confusion are studied.

\subsection{Verification Process}
\label{verification-casestudy:sec}

In the context of our framework (shown in Figure~\ref{schemeModel:fig})
the ACC is part of the control loop associated with the system model.
The user's commands are, normally, filtered via the control loop except
when the user can override the ACC and issue commands directly to the system.
This type of behaviour is typically seen when the user is fatigued or 
encounters a hazardous situation.

The behaviour of the system (i.e., the vehicle and the human operator)
was analysed for under various conditions (e.g., route choices, travelling
speed).
We were able to show that a small separation between vehicles
was safe if the driver
was not too tired and the interaction mode involved game pads.
We were also able to show that even a large separation was unsafe
if the driver was tired and the interaction mode involved only speech.
The safety conditions are written as assertions such as 

\begin{verbatim}
assert(isOkay(driverFatigue,hazardPerception,safeStoppingDistance));
\end{verbatim}
where
\verb|isOkay| determines if the current separation is
safe given the parameters from the behaviour  model.
These are specific to the application being analysed.

In the function \verb|main| we invoke the function
associated with the control loop (shown in Figure~\ref{cloop:fig})
 with the various
scenarios.
By varying the variables that are assigned non-deterministic
values, one can explore the safety of different scenarios within a given
model.
We also use CBMC to calculate values where failures occur.
For instance, the following program fragment
determines  if there are any
values of \verb|driverFatigue| that can lead to  accidents.

\begin{verbatim}
int driverFatigue = nondet_uint()%numFatigueValues;
assert(isOkay(driverFatigue,hazardPerception,safeStoppingDistance));
\end{verbatim}

CBMC will calculate a value for \verb|driverFatigue| 
 that will violate the assertion. If CBMC cannot find such a violation,
we are sure that the composite behaviour represented by the models
is safe.

